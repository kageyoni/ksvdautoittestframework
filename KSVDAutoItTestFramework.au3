#include <ScreenCapture.au3>
#include <File.au3>
#include <WinAPIFiles.au3>
#include <Date.au3>

; ---- ��������� ������� -----
#include "UserFunctions\FncMouseClick.au3" ; ���� ������ �� �����������
#include "UserFunctions\FncRunApp.au3" ; ������ ���������
#include "UserFunctions\FncCreateResultsFolder.au3" ; �������� ���������� ����������� ���������� ������
#include "UserFunctions\FncScreenCapture.au3" ; ������ ���������
#include "UserFunctions\FncTestEnding.au3" ; �������� �����, ���������������� �� ������ ����������� �����
#include "UserFunctions\FncSendData.au3" ; ���� ������ � ����
#include "UserFunctions\FncCloseWindow.au3" ; �������� ����
#include "UserFunctions\FncGetScriptName.au3" ; ����������� ����� �������, ��� �������� ���������� �����������
#include "UserFunctions\FncCheckAndActivateWindow.au3" ; �������� ���������� ����
#include "UserFunctions\FncControlClick.au3" ; ���� ������ �� ��������
#include "UserFunctions\FncGetTestConfigFileName.au3" ; ��������� ���� �� ������� �������
#include "UserFunctions\FncLicenseKeyGenerate.au3" ; ��������� ��������
#include "UserFunctions\FncKSVDLogParser.au3" ; ������ ����� KSVD
#include "UserFunctions\FncResetResources.au3" ; ����� ���������� (����������� �������, �������� � �.�.)
#include "UserFunctions\FncDateTimeChange.au3" ; ������ � �����/��������
#include "UserFunctions\FncCopyToClipboard.au3" ; ����������� ������ � ����� ������
#include "UserFunctions\FncSavePrintScreen.au3" ; �������� ���� ��������� ����������� PrintScreen
#include "UserFunctions\FncLocalUpdateV666Generator.au3" ; ��������� ���������� ������ 666
#include "UserFunctions\FncCheckKSVDInstallation.au3" ; ��������, ����������� �� �� KSVD � �������
#include "UserFunctions\FncGetLocalIP.au3" ; ��������� ���������� IP ������
#include "UserFunctions\FncAddItemsToArrayAddedElementsInKSVD.au3"
#include "UserFunctions\FncRemoveAddedElements.au3"
#include "UserFunctions\FncParseKsvdVersion.au3"
#include "UserFunctions\FncCloseArmAdmin.au3"
#include "UserFunctions\FncCloseWindowByHandle.au3"
#include "UserFunctions\FncClickTo.au3"
#include "UserFunctions\FncCheckNetworkFileExists.au3"

; ---- ��������� ����������� �������� -----
; Add
#include "StandartActions\ActionAddCamera.au3"
#include "StandartActions\ActionAddArchiveGUI.au3"
#include "StandartActions\ActionAddCameraToArchive.au3"
#include "StandartActions\ActionAddRtspRetranslatorGUI.au3"
#include "StandartActions\ActionAddRtspRetranslatorToCamera.au3"
#include "StandartActions\ActionAddRegionByScada.au3"
#include "StandartActions\ActionAddRegionByScadaScripts.au3"
#include "StandartActions\ActionAddObjectToRegionByScada.au3"
#include "StandartActions\ActionAddObjectToRegionByScadaScripts.au3"
#include "StandartActions\ActionAddCameraToObjectByScada.au3"
#include "StandartActions\ActionAddViewPanelByScada.au3"
#include "StandartActions\ActionAddViewPanelByScadaScripts.au3"
#include "StandartActions\ActionAddPlanToRegionObject.au3"
#include "StandartActions\ActionAddUser.au3"
#include "StandartActions\ActionAddTileServer.au3"
#include "StandartActions\ActionAddUpdateSourse.au3"
; Run
#include "StandartActions\ActionRunArmAdmin.au3"
#include "StandartActions\ActionRunArmMonitor.au3"
#include "StandartActions\ActionRunArmExportFromArchive.au3"
#include "StandartActions\ActionRunArmOperator.au3"
#include "StandartActions\ActionRunArmSettings.au3"
; Close
#include "StandartActions\ActionCloseArmOperator.au3"
#include "StandartActions\ActionCloseArmExportFromArchive.au3"
; Remove
#include "StandartActions\ActionRemoveKSVD.au3"
#include "StandartActions\ActionRemoveUpdateSource.au3"
#include "StandartActions\ActionRemoveRtspRetranslatorInGui.au3"
#include "StandartActions\ActionRemoveCameraInGUI.au3"
#include "StandartActions\ActionRemoveArchiveInGui.au3"
#include "StandartActions\ActionRemoveRegion.au3"
#include "StandartActions\ActionRemoveRegionByScadaScripts.au3"
#include "StandartActions\ActionRemoveRegionByScada.au3"
#include "StandartActions\ActionRemoveUserInGui.au3"
#include "StandartActions\ActionRemoveAllViewPanels.au3"
#include "StandartActions\ActionRemoveTileServer.au3"
; Find
#include "StandartActions\ActionFindRegionInScada.au3"
#include "StandartActions\ActionFindRegionObjectInScada.au3"
#include "StandartActions\ActionFindArchiveInGui.au3"
#include "StandartActions\ActionFindRegionObjectInArmOperator.au3"
#include "StandartActions\ActionFindViewPanelInArmOperator.au3"
#include "StandartActions\ActionFindRtspRetranslatorInGui.au3"
#include "StandartActions\ActionFindCameraInGUI.au3"
#include "StandartActions\ActionFindTileServerInGui.au3"
#include "StandartActions\ActionFindUserInGui.au3"
#include "StandartActions\ActionFindPlayCameraShowPanelArmOperator.au3"
; Other
#include "StandartActions\ActionLogInArmAdmin.au3"
#include "StandartActions\ActionEnterLicenseKeyInArmAdmin.au3"
#include "StandartActions\ActionClearFilterFieldInArmAdmin.au3"
#include "StandartActions\ActionChangeArmSettings.au3"
#include "StandartActions\ActionWindowsInstallKSVD.au3"
#include "StandartActions\ActionHideShowArmOperatorWindow.au3"
#include "StandartActions\ActionCheckKsvdVersion.au3"
#include "StandartActions\ActionPlayLiveAndArchiveVideoArmOperator.au3"
#include "StandartActions\ActionOpenViewPanelInArmOperator.au3"
#include "StandartActions\ActionPlayFileInVlc.au3"
#include "StandartActions\ActionPlayStreamInVlc.au3"





Opt("MouseCoordMode",2) ; 2 - �������� � ���������� ����� ��������� ���� (��� ��, ��� ���� ���������). � AutoIt Window Info ������������� Coord Mode = Client
;��� ���������� ����������� � ��������� �� ���������� FullHD

; ---- ���� �� ���������������� ������ ----
Local $sKSVDAutoItTestFrameworkDir = @ScriptDir & "\KSVDAutoItTestFramework\"
Local $sSettingsDir = $sKSVDAutoItTestFrameworkDir & "Settings\"
Local $sCommonConfig = $sKSVDAutoItTestFrameworkDir & "config.ini"; ���� �� ����������� �������
Local $sKsvdAppsParams = $sSettingsDir & "ksvdAppsParams.ini"; ���� �� ������� ���������� ���������� KSVD
Local $sTestConfigPath = FncGetTestConfigFileName(@ScriptFullPath) ; ���� �� ������� �����
Local $sAddedObjectsConfig = $sSettingsDir & "addedObjects.ini" ;���� �� ������� �� ������� ����������� ��������
Local $sUsersConfig = $sSettingsDir & "users.ini"; ���� �� ������� � ��������������
Local $sCamerasConfig = $sSettingsDir & "cameras.ini"; ���� �� ������� � ��������
Local $sArchivesConfig = $sSettingsDir & "archives.ini"; ���� �� ������� � �����������
Local $sRTSPConfig = $sSettingsDir & "rtspRetranslators.ini"; ���� �� ������� � RTSP ���������������
Local $sRegionsConfig = $sSettingsDir & "regions.ini"; ���� �� ������� � ���������
Local $sUpdateSources = $sSettingsDir & "updateSources.ini"; ���� �� ������� � ������� ����������
Local $sRegionsObjectsConfig = $sSettingsDir & "regionsObjects.ini"; ���� �� ������� � ��������� ��������
Local $sViewPanelsConfig = $sSettingsDir & "viewPanels.ini"; ���� �� ������� � �����������
Local $sTileServersConfig = $sSettingsDir & "tileServers.ini"; ���� �� ������� � ��������� ���������
Local $sKSVDAutoItTestFrameworkVersionFile = $sKSVDAutoItTestFrameworkDir & "version.ini"; ���� �� �������, ����������� ������ ����������
Local $sScriptsVersionFile = @ScriptDir & "\version.ini"; ���� �� �������, ����������� ������ ��������

; ---- ������ ����������� ������� ---
Local $sKSVDSetupFolder = IniRead ( $sCommonConfig, "InstallInformation", "KSVDSetupFolder", "$sKSVDSetupFolder" ) ;���� �� �����, ���������� �� ����������� ��� ��������� KSVD
Local $sInstallationVersion = IniRead( $sCommonConfig, "InstallInformation", "installationVersion", "$sInstallationVersion") ;������ KSVD � ������� ����������� ������
Local $sInstallPath = IniRead ( $sCommonConfig, "InstallInformation", "installPath", "$sInstallPath" ) ;���������� ��������� KSVD
Local $sArmAdmin = IniRead ( $sKsvdAppsParams, "KsvdAppsParams", "armAdminExe", "$sArmAdmin") ;��� ����� ������� ��� ��������������
Local $sArmAdminWindowName = IniRead ( $sKsvdAppsParams, "KsvdAppsParams", "armAdminWindowName", "$sArmAdminWindowName") ;�������� ���� ��� ��������������
Local $sArmExportFromArchiveWindowName = IniRead ( $sKsvdAppsParams, "KsvdAppsParams", "armExportFromArchiveName", "$sArmExportFromArchiveWindowName") ;�������� ���� ��� �������� �������
Local $sArmOperatorWindowName = IniRead ( $sKsvdAppsParams, "KsvdAppsParams", "armOperatorWindowName", "$sArmOperatorWindowName") ;�������� ���� ��� ���������
Local $sArmSettings = IniRead ( $sKsvdAppsParams, "KsvdAppsParams", "armSettingsExe", "$sArmSettings") ;��� ����� ������� ��� ���������
$sArmSettings = $sInstallPath & "\" & $sArmSettings
Local $sArmSettingsWindowName = IniRead ( $sKsvdAppsParams, "KsvdAppsParams", "armSettingsWindowName", "$sArmSettingsWindowName") ;�������� ���� ��� ���������
Local $sArmMonitorWindowName = IniRead ( $sKsvdAppsParams, "KsvdAppsParams", "armMonitorWindowName", "$sArmMonitorWindowName") ;�������� ���� ��� �����������
Local $sKeyFileName = IniRead ( $sCommonConfig, "InstallInformation", "keyFileName", "$sKeyFileName") ;��� ����� ������������� �����
Local $sArchiveDir = IniRead ( $sCommonConfig, "BasicInformation", "archiveDir", "$sArchiveDir") ;���������� � ������� ��������

; ---- ������������� ��������� ���������� ----
Local $sTestName = FncGetScriptName(@ScriptName)
Local $sResultsFolderPath = @ScriptDir & "\Results\" & $sTestName ; �������� ���������� � ������������ �����
Local $sResourcesDir = $sKSVDAutoItTestFrameworkDir & "Resources\"
Local $sLogFile = $sResultsFolderPath & "\" & $sTestName & "_log.txt" ; ���� �� ����� �����
Local $sDebugLogFile = $sResultsFolderPath & "\Debug\" & $sTestName & "_debug_log.txt" ; ���� �� �������� (�����������) �����
Local $sDebugScreenPath = $sResultsFolderPath & "\Debug\" ;���� �� ������� ��� �������
Local $sKeyScreenPath = $sResultsFolderPath & "\" ;���������� "��������" �������
Local $iScreenNumber = 1 ;��������� ����������
Local $TestFailed = "���� " & $sTestName & " ��������"
Local $TestComplete = "���� " & $sTestName & " ������� ��������"
Local $bExitIfAuthFail = True ;����������� �� ���������� �����, ���� ����������� ���� �� ��������
Local $sAction = ""
Local $bTestEnds = False
Local $aAddedElementsInKSVD[22][22]
Local $iAddedCount = -1
Local $bArmOperator28CameraViewerIsOpened = False
Local $sLicenseKey
Local $sArmOperatorLoginSuccess
Local $hArmOperatorWindowHandle
Local $hArmMonitorWindowHandle

; ---- ��������� ������ ���������� � �������� ---
Local $sKSVDAutoItTestFrameworkVersion = IniRead( $sKSVDAutoItTestFrameworkVersionFile, "KSVDAutoItTestFramework", "version", "$sKSVDAutoItTestFrameworkVersion")
Local $sScriptsVersion = IniRead( $sScriptsVersionFile, "ScriptsVersion", "version", "$sScriptsVersion")

FncCreateResultsFolder($sResultsFolderPath) ; ������ ���������� ��� ����������� ���������� �����

_FileWriteLog($sLogFile, "������ ��������: " & $sScriptsVersion)
_FileWriteLog($sLogFile, "������ KSVDAutoItTestFramework: " & $sKSVDAutoItTestFrameworkVersion)

Local $sVersion = FncParseKsvdVersion($sInstallationVersion)