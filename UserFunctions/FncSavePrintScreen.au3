Func FncSavePrintScreen($sScreenshotName)

    Local $sWinVer = @OSVersion
    Local $sScreensDir = @UserProfileDir & "\Pictures\Screenshots\"
    Local $aFileList = _FileListToArray ( $sScreensDir, Default, $FLTA_FILES, True)

    If FileExists($sScreensDir) Then
        _FileWriteLog($sDebugLogFile, "���������� " & $sScreensDir & " ����������")
    Else
        _FileWriteLog($sDebugLogFile, "�������� �������� �������� ���������� " & $sScreensDir)
        DirCreate($sScreensDir)
    EndIf

    If $aFileList = 0 Then
        _FileWriteLog($sDebugLogFile, "���������� " & $sScreensDir & " �� �������� ������")
    Else
        ;---- ������� ������ � ���������� ----
        For $i = 1 To $aFileList[0]
            FileDelete ( $aFileList[$i] )
        next
    EndIf

    If $sWinVer = "WIN_7" then
        _FileWriteLog($sDebugLogFile, "������� ��: " & $sWinVer & ". �������� �������� ������ ��������� ���� ������� ����� ���������� �������")
        Local $sSnippingTool = @WindowsDir & "\System32\SnippingTool.exe"
        Local $sSnippingToolWindowName
        Local $sOsLanguage = @OSLang
        _FileWriteLog($sDebugLogFile, "������ ���������� �������")
		_WinAPI_Wow64EnableWow64FsRedirection ( False )
        FncRunApp($sSnippingTool)
		_WinAPI_Wow64EnableWow64FsRedirection ( True )

        If $sOsLanguage = 1033 Then
            _FileWriteLog($sDebugLogFile, "������� ���� ��: ����������. ��� �����: " & $sOsLanguage)
            $sSnippingToolWindowName = "Snipping Tool"
        Else
            _FileWriteLog($sDebugLogFile, "������� ���� ��: �������. ��� �����: " & $sOsLanguage)
            $sSnippingToolWindowName = "�������"
        EndIf

        FncCheckAndActivateWindow($sSnippingToolWindowName)

        $sAction = "����� �������� ���������"
        FncMouseClick("left",95,18,1)

        $sAction = "������� ������ UP"
        FncSendData("{UP}")

        $sAction = "������� ������ ENTER"
        FncSendData("{ENTER}")

		sleep(1000)

        $sAction = "������� ������ CTRLDOWN"
        FncSendData("{CTRLDOWN}")
        $sAction = "������� ������ s"
        FncSendData("{s}")
        $sAction = "������� ������ CTRLUP"
        FncSendData("{CTRLUP}")

        sleep(1000)

        $sAction = "���� ����� �����"
        FncSendData($sKeyScreenPath & $sScreenshotName & ".jpg")

        $sAction = "������� ������ ENTER"
        FncSendData("{ENTER}")

		$sAction = "������� ������ ESC"
        FncSendData("{ESC}")

        FncCloseWindow($sSnippingToolWindowName)

    Else
        _FileWriteLog($sDebugLogFile, "������� ��: " & $sWinVer & ". �������� �������� ������ ��������� ����� WIN+PRINTSCREEN")

        $sAction = "������� ������ left Windows"
	    FncSendData("{LWINDOWN}")

        $sAction = "������� ������ PRINTSCREEN"
	    FncSendData("{PRINTSCREEN}")

        $sAction = "������� ������ left Windows"
	    FncSendData("{LWINUP}")

		$aFileList = _FileListToArray ( $sScreensDir, Default, $FLTA_FILES, True)

        sleep(5000)

        for $i = 0 to 10
            $aFileList = _FileListToArray ( $sScreensDir, Default, $FLTA_FILES, True)
            If $aFileList = 0 And $i < 10 then
                _FileWriteLog($sDebugLogFile, "������� �" & $i & ". ���������� " & $sScreensDir & " �� �������� ������")
                sleep(2000)
            ElseIf $aFileList = 0 And $i = 10 then
                FncTestEnding(False, "�� ������ �������� � ���������� " & $sScreensDir)
            Else
                _FileWriteLog($sDebugLogFile, "�������� ������")
                FileMove( $aFileList[1], $sKeyScreenPath & $sScreenshotName & ".jpg", $FC_OVERWRITE )
                ExitLoop
            EndIf
        next
    EndIf
EndFunc