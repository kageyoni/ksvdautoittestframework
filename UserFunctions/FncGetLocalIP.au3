Func FncGetLocalIP()

    Local $sIP1 = @IPAddress1
    Local $sIP2 = @IPAddress2
    Local $sLocalIP

    If $sIP1 = "127.0.0.1" Then
        _FileWriteLog($sDebugLogFile, "������ ������� ��������� ������ ��������: "& $sIP1)
        If $sIP2 = "0.0.0.0" Then
            FncTestEnding(False, "������ ������� ��������� ������ ��������: 0.0.0.0")
        Else
            $sLocalIP = $sIP2
        EndIf
    ElseIf $sIP1 = "0.0.0.0" Then
        FncTestEnding(False, "��� �������� ������� �����������. ������ ip ������ ������ �������� 0.0.0.0")
    Else
        _FileWriteLog($sDebugLogFile, "���������� ��������� IP �����: " & $sIP1)
        $sLocalIP = $sIP1
    EndIf

    Return $sLocalIP
EndFunc