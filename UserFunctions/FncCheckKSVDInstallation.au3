Func FncCheckKSVDInstallation()

    Local $sInstallPath = IniRead ( $sCommonConfig, "InstallInformation", "installPath", "$sInstallPath" )
    Local $bKSVDInstalledInner

    If FileExists($sInstallPath & "\unins000.exe") Then
        _FileWriteLog($sDebugLogFile, "KSVD ���������� � �������")
        $bKSVDInstalledInner = True
    Else
        _FileWriteLog($sDebugLogFile, "KSVD �� ���������� � �������. �� ������ ����: " & $sInstallPath & "\unins000.exe")
        $bKSVDInstalledInner = False
    EndIf

    Return $bKSVDInstalledInner

Endfunc