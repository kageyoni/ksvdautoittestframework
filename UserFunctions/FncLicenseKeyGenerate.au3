Func FncLicenseKeyGenerate($sInstallationId)

   Local $sLicenseExe = "License.exe"
   Local $sFuncName = "FncLicenseKeyGenerate. "
   Local $sWindowsLicenseGeneratorFolder = $sKSVDSetupFolder & "\supp\windows"
   $sKeyFileName = "key." & $sInstallationId & ".ini"
   Local $sKeyFileFullPath = $sWindowsLicenseGeneratorFolder & "\" & $sKeyFileName

   _FileWriteLog($sLogFile, "---------------------------------------")
   _FileWriteLog($sLogFile, "������ �������� ��������� ������������� �����")
   _FileWriteLog($sDebugLogFile, "---------------------------------------")
   _FileWriteLog($sDebugLogFile, "������ �������� ��������� ������������� �����")
   _FileWriteLog($sDebugLogFile, $sFuncName & "���������� ���������� ������������ ������: " & $sWindowsLicenseGeneratorFolder)

   ;---------------------------------------------------------------------------------
   ;----- �������� ������������� ����� ���������� ����� ����� -----------------------
   ;---------------------------------------------------------------------------------

   If FileExists($sKeyFileFullPath) Then
      _FileWriteLog($sDebugLogFile, $sFuncName & "������ ����� ��������� ���� " & $sKeyFileFullPath)
      Local $iSetAttrib = FileSetAttrib($sKeyFileFullPath, "-R")
      If $iSetAttrib = 1 Then
         _FileWriteLog($sDebugLogFile, $sFuncName & "����� �������� read-only � ����� " & $sKeyFileName & " ��� ������������ ��� ��������")
      Else
         FncTestEnding(False, "�� ������� ������� �������� read-only � ����� " & $sKeyFileName)
      EndIf
      Local $iDelete = FileDelete($sKeyFileFullPath)
      if $iDelete = 1 then
         _FileWriteLog($sDebugLogFile, $sFuncName & "���� ������� �����")
      Else
         FncTestEnding(False, "�� ������� ������� ����� ��������� ���� �����: " & $sKeyFileFullPath)
      endif
   EndIf

   ;---------------------------------------------------------------------------------
   ;----- ��������� ����� -----------------------------------------------------------
   ;---------------------------------------------------------------------------------

   If FileExists($sWindowsLicenseGeneratorFolder & "\" & $sLicenseExe) Then
      _FileWriteLog($sDebugLogFile, $sFuncName & "�������� ��������� �����")
      ShellExecuteWait($sWindowsLicenseGeneratorFolder & "\" & $sLicenseExe, $sInstallationId, "", "runas")
   Else
      FncTestEnding(False, "���� " & $sLicenseExe & " ����������� � ���������� ���������� ��������")
   EndIf

   ;---------------------------------------------------------------------------------
   ;----- �������� ���������� �������� ��������� ����� ------------------------------
   ;---------------------------------------------------------------------------------

   If FileExists ($sKeyFileFullPath) Then
      _FileWriteLog($sLogFile, "������������ ���� ������� ������������")
      _FileWriteLog($sDebugLogFile, $sFuncName & "���� �� ���������������� ������������� �����: " & $sKeyFileFullPath)
   Else
      FncTestEnding(False, "�� ������� ������������� ������������ ����. ���� " & $sKeyFileFullPath & " �����������")
   EndIf

   ;---------------------------------------------------------------------------------
   ;----- ������ ����� � ���������� ���������� --------------------------------------
   ;---------------------------------------------------------------------------------

   $sLicenseKey = FileRead($sKeyFileFullPath)

   ;---------------------------------------------------------------------------------
   ;----- �������� ����� ����� ------------------------------------------------------
   ;---------------------------------------------------------------------------------

   Local $iSetAttrib = FileSetAttrib($sKeyFileFullPath, "-R")
   If $iSetAttrib = 1 Then
      _FileWriteLog($sDebugLogFile, $sFuncName & "����� �������� read-only � ����� " & $sKeyFileName & " ��� ������������ ��� ��������")
   Else
      FncTestEnding(False, "�� ������� ������� �������� read-only � ����� " & $sKeyFileName)
   EndIf

   Local $iDelete = FileDelete($sKeyFileFullPath)
   If $iDelete = 1 then
      _FileWriteLog($sDebugLogFile, $sFuncName & "���� ������� �����")
   Else
      FncTestEnding(False, "�� ������� ������� ����� ��������� ���� �����: " & $sKeyFileFullPath)
   endif

EndFunc