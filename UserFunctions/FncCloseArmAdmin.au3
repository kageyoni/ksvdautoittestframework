Func FncCloseArmAdmin()

    Local $sArmAuth28WindowName = "�������������� � ��� �������������� KSVD"

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� �������� ���������� ��� ��������������")
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "������ �������� �������� ���������� ��� ��������������")

    if $sVersion = "2.8" Then
        If WinExists($sArmAdminWindowName ) Then
            FncCloseWindowByHandle($sArmAdminWindowName)
            _FileWriteLog($sLogFile, "���������� ��� �������������� ������� �������")
        ElseIf WinExists($sArmAuth28WindowName) Then
            FncCloseWindowByHandle($sArmAuth28WindowName)
            _FileWriteLog($sLogFile, "���������� ��� �������������� ������� �������")
        Else
            FncTestEnding(False, "�� ������� ���������� �������� ���� ��� ��������������")
        EndIf

    ElseIf $sVersion = "2.10" Then
        If WinExists($sArmAdminWindowName) Then
            FncCloseWindowByHandle($sArmAdminWindowName)
            _FileWriteLog($sLogFile, "���������� ��� �������������� ������� �������")
        Else
            FncTestEnding(False, "�� ������� ���������� �������� ���� ��� ��������������")
        EndIf
    EndIf

EndFunc