Func FncParseKsvdVersion($sVersion)

    _FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "������ �������� ����������� ������ KSVD")
    _FileWriteLog($sDebugLogFile, "������, ���������� �� ����: " & $sVersion)

    Local $iIndex
    Local $bFirstDot = false
    Local $sStringLen = StringLen ($sVersion)

    For $i = 1 To StringLen($sVersion)

    Local $sChar = StringMid($sVersion, $i, 1)

    if $sChar = "." then
        If $bFirstDot = false Then
            $bFirstDot = True
        ElseIf $bFirstDot = True Then
            $iIndex = $i -1
            ExitLoop
        EndIf
    EndIf

    Next

    $iIndex = $sStringLen - $iIndex

    $sVersion = StringTrimRight ($sVersion, $iIndex)

    _FileWriteLog($sLogFile, "������ KSVD: " & $sVersion)

    Return $sVersion

EndFunc