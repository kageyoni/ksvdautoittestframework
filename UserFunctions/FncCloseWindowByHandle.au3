Func FncCloseWindowByHandle($sWindowTitile)

    _FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "����c� �������� �������� ���� " & $sWindowTitile )

    Local $aList = WinList ($sWindowTitile)
    If $aList[0][0] > 0 Then
        _FileWriteLog($sDebugLogFile, "���������� " & $aList[0][0] & " ���� � ����������: " & $sWindowTitile)
        For $i = 1 To $aList[0][0]
            _FileWriteLog($sDebugLogFile, "����� ������� ���� " & $aList[$i][0] & " Handle " & $aList[$i][1])
            WinClose ($aList[$i][1])
        Next
    Else
        FncTestEnding(False, "�� ���������� �������� ���� � ���������� " & $sWindowTitile)
    EndIf

    Sleep(2000)

    If WinExists ($sWindowTitile) Then
        Local $hWnd = WinGetHandle($sWindowTitile)
        FncTestEnding(False, "���� " & $sWindowTitile & " Handle " & $hWnd & " ���������� ����� �������� ������� ��������")
    EndIf
EndFunc