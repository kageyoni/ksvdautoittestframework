Func FncLocalUpdateV666Generator($sKSVDSetupFolder, $sInstallationVersion)

    ; ---- ������������� ����������� ���������� ----
    Local $sWindowsInstallerFolder = $sKSVDSetupFolder & "\install\windows"
    Local $sWindowsInstallerEXEName = "KsvdInstall_" & $sInstallationVersion & ".exe"
    Local $sWindowsInstallerEXEPath = $sWindowsInstallerFolder & "\" & $sWindowsInstallerEXEName
    Local $sUpdateFolder = $sKSVDSetupFolder & "\update"
    Local $sWindowsV666TempFolder = $sWindowsInstallerFolder & "\V666Update"
    Local $sWindowsV666FinalFolder = $sWindowsV666TempFolder & "\finalUpdateV666"
    Local $sWindowsLicenseGeneratorFolder = $sKSVDSetupFolder & "\supp\windows"

    _FileWriteLog($sLogFile, "�������� �������� ��������� ���������� ������ 6.6.6")

    _FileWriteLog($sDebugLogFile, "�������� ������� ����������: " & $sWindowsV666TempFolder)
    If FileExists($sWindowsV666TempFolder) Then
        DirRemove($sWindowsV666TempFolder, $DIR_REMOVE) ; Remove the directory and all sub-directories.
        _FileWriteLog($sDebugLogFile, "���������� ���������� ������ 666 ������������ � ���� �������")
	EndIf

    _FileWriteLog($sDebugLogFile, "�������� ����������: " & $sWindowsV666TempFolder)
    DirCreate($sWindowsV666TempFolder)
    _FileWriteLog($sDebugLogFile, "�������� ���������� �������� �������� ����������")
    If FileExists($sWindowsV666TempFolder) Then
        _FileWriteLog($sDebugLogFile, "�������� �������� ���������� ������� ���������")
    else
        FncTestEnding(False, "������ c������� ����������: " & $sWindowsV666TempFolder)
    Endif

    _FileWriteLog($sDebugLogFile, "�������� ����������: " & $sWindowsV666TempFolder & "\update")
    DirCreate($sWindowsV666TempFolder & "\update")
    _FileWriteLog($sDebugLogFile, "�������� ���������� �������� �������� ����������")
    If FileExists($sWindowsV666TempFolder & "\update") Then
        _FileWriteLog($sDebugLogFile, "�������� �������� ���������� ������� ���������")
    else
        FncTestEnding(False, "������ c������� ����������: " & $sWindowsV666TempFolder)
    Endif

    _FileWriteLog($sDebugLogFile, "����������� ����������: " & $sUpdateFolder & " � ����������: " &  $sWindowsV666TempFolder & "\update")
    DirCopy($sUpdateFolder, $sWindowsV666TempFolder & "\update", $FC_OVERWRITE)
    _FileWriteLog($sDebugLogFile, "�������� ���������� �������� �����������")
    If FileExists($sWindowsV666TempFolder & "\update\windows") Then
        _FileWriteLog($sDebugLogFile, "�������� ����������� ������� ���������")
    else
        FncTestEnding(False, "������ �����������. ���������� " & $sWindowsV666TempFolder & "\update\windows" & " �� �������")
    Endif

    _FileWriteLog($sDebugLogFile, "����������� �����: " & $sWindowsInstallerEXEPath & " � ����������: " &  $sWindowsV666TempFolder)
    FileCopy($sWindowsInstallerEXEPath, $sWindowsV666TempFolder & "\" , $FC_OVERWRITE)
    _FileWriteLog($sDebugLogFile, "�������� ���������� �������� �����������")
    If FileExists($sWindowsV666TempFolder & "\" & $sWindowsInstallerEXEName) Then
        _FileWriteLog($sDebugLogFile, "�������� ����������� ������� ���������")
    else
        FncTestEnding(False, "������ �����������. ���� " & $sWindowsV666TempFolder & "\" & $sWindowsInstallerEXEName & " �� ������")
    Endif

    _FileWriteLog($sDebugLogFile, "����������� ����� " & $sWindowsLicenseGeneratorFolder & "\libeay32MD.dll" & " � ����������: " &  $sWindowsV666TempFolder)
    FileCopy($sWindowsLicenseGeneratorFolder & "\libeay32MD.dll", $sWindowsV666TempFolder & "\" , $FC_OVERWRITE)
    _FileWriteLog($sDebugLogFile, "�������� ���������� �������� �����������")
    If FileExists($sWindowsLicenseGeneratorFolder & "\libeay32MD.dll") Then
        _FileWriteLog($sDebugLogFile, "�������� ����������� ������� ���������")
    else
        FncTestEnding(False, "������ �����������. ���� " & $sWindowsV666TempFolder & "\libeay32MD.dll" & " �� ������")
    Endif

    _FileWriteLog($sDebugLogFile, "����������� ����� " & $sWindowsLicenseGeneratorFolder & "\Qt5Core.dll" & " � ����������: " &  $sWindowsV666TempFolder)
    FileCopy($sWindowsLicenseGeneratorFolder & "\Qt5Core.dll", $sWindowsV666TempFolder & "\" , $FC_OVERWRITE)
    _FileWriteLog($sDebugLogFile, "�������� ���������� �������� �����������")
    If FileExists($sWindowsV666TempFolder & "\Qt5Core.dll") Then
        _FileWriteLog($sDebugLogFile, "�������� ����������� ������� ���������")
    else
        FncTestEnding(False, "������ �����������. ���� " & $sWindowsV666TempFolder & "\Qt5Core.dll" & " �� ������")
    Endif

    _FileWriteLog($sDebugLogFile, "����������� ����� " & $sWindowsLicenseGeneratorFolder & "\Qt5Network.dll" & " � ����������: " &  $sWindowsV666TempFolder)
    FileCopy($sWindowsLicenseGeneratorFolder & "\Qt5Network.dll", $sWindowsV666TempFolder & "\" , $FC_OVERWRITE)
    _FileWriteLog($sDebugLogFile, "�������� ���������� �������� �����������")
    If FileExists($sWindowsV666TempFolder & "\Qt5Network.dll") Then
        _FileWriteLog($sDebugLogFile, "�������� ����������� ������� ���������")
    else
        FncTestEnding(False, "������ �����������. ���� " & $sWindowsV666TempFolder & "\Qt5Network.dll" & " �� ������")
    Endif

    _FileWriteLog($sDebugLogFile, "����������� ����� " & $sWindowsLicenseGeneratorFolder & "\Qt5Sql.dll" & " � ����������: " &  $sWindowsV666TempFolder)
    FileCopy($sWindowsLicenseGeneratorFolder & "\Qt5Sql.dll", $sWindowsV666TempFolder & "\" , $FC_OVERWRITE)
    _FileWriteLog($sDebugLogFile, "�������� ���������� �������� �����������")
    If FileExists($sWindowsV666TempFolder & "\Qt5Sql.dll") Then
        _FileWriteLog($sDebugLogFile, "�������� ����������� ������� ���������")
    else
        FncTestEnding(False, "������ �����������. ���� " & $sWindowsV666TempFolder & "\Qt5Sql.dll" & " �� ������")
    Endif

    _FileWriteLog($sDebugLogFile, "����������� ����� " & $sWindowsLicenseGeneratorFolder & "\ksvd4_inst.exe" & " � ����������: " &  $sWindowsV666TempFolder)
    FileCopy($sWindowsLicenseGeneratorFolder & "\ksvd4_inst.exe", $sWindowsV666TempFolder & "\" , $FC_OVERWRITE)
    _FileWriteLog($sDebugLogFile, "�������� ���������� �������� �����������")
    If FileExists($sWindowsV666TempFolder & "\ksvd4_inst.exe") Then
        _FileWriteLog($sDebugLogFile, "�������� ����������� ������� ���������")
    else
        FncTestEnding(False, "������ �����������. ���� " & $sWindowsV666TempFolder & "\ksvd4_inst.exe" & " �� ������")
    Endif

    _FileWriteLog($sDebugLogFile, "����������� ����� " & $sWindowsLicenseGeneratorFolder & "\License.exe" & " � ����������: " &  $sWindowsV666TempFolder)
    FileCopy($sWindowsLicenseGeneratorFolder & "\License.exe", $sWindowsV666TempFolder & "\" , $FC_OVERWRITE)
    _FileWriteLog($sDebugLogFile, "�������� ���������� �������� �����������")
    If FileExists($sWindowsV666TempFolder & "\License.exe") Then
        _FileWriteLog($sDebugLogFile, "�������� ����������� ������� ���������")
    else
        FncTestEnding(False, "������ �����������. ���� " & $sWindowsV666TempFolder & "\License.exe" & " �� ������")
    Endif

    _FileWriteLog($sDebugLogFile, "��������� ���������� ������ 666")
    ShellExecuteWait($sWindowsV666TempFolder & "\ksvd4_inst.exe" , "v666 update finalUpdateV666" , "", "runas")
    _FileWriteLog($sDebugLogFile, "�������� ������� ��������� ���������� ����������: " & $sWindowsV666FinalFolder)
    If FileExists($sWindowsV666FinalFolder) Then
        _FileWriteLog($sLogFile, "�������� ��������� ���������� ������ 6.6.6 ������� ���������")
    Else
        FncTestEnding(False, "������ ��������� ����������. ���������� " & $sWindowsV666FinalFolder & " �� ����������")
	EndIf
Endfunc