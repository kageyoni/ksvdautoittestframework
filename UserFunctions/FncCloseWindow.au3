Func FncCloseWindow($sWindowName, $sClass = "")

	_FileWriteLog($sLogFile, "������ �������� �������� ���� " & $sWindowName)

	Local $iTryNumber = 0
   	If WinExists($sWindowName) Then
		_FileWriteLog($sLogFile, "���� " & $sWindowName & " �������")
		If $sClass <> "" Then
			While WinExists($sWindowName)
				$iTryNumber = $iTryNumber + 1
				If WinClose("[CLASS:" & $sClass & "]") Then
					_FileWriteLog($sLogFile, "����������� ������� �" & $iTryNumber & " �������� ���� �� ������: " & $sClass)
				Else
					_FileWriteLog($sLogFile, "�� ������� ������� ���� " & $sWindowName)
				EndIf
				If $iTryNumber > 5 then
					DebugGetAndSaveAllWindowsList()
					FncTestEnding(False, "�� ������� ������� ���� �� 5 �������")
				EndIf
				Sleep(500)
			WEnd
		Else
			While WinExists($sWindowName)
				$iTryNumber = $iTryNumber + 1
				If WinClose($sWindowName) Then
					_FileWriteLog($sLogFile, "����������� ������� �" & $iTryNumber & " �������� ����")
				Else
					_FileWriteLog($sLogFile, "�� ������� ������� ���� " & $sWindowName)
				EndIf
				If $iTryNumber > 5 then
					DebugGetAndSaveAllWindowsList()
					FncTestEnding(False, "�� ������� ������� ���� �� 5 �������")
				EndIf
				Sleep(500)
			WEnd
		EndIf

		Sleep(500)
		If WinExists($sWindowName) Then
			_FileWriteLog($sLogFile, "�� ������� ������� ���� " & $sWindowName)
			FncTestEnding(False, "�� ������� ������� ���� " & $sWindowName)
		Else
			_FileWriteLog($sLogFile, "���� " & $sWindowName & " ������� �������")
		EndIf

	Else
		Local $sInnerMsg = "���� " & $sWindowName & " �� �������"
		FncScreenCapture($sKeyScreenPath, $sInnerMsg)
		DebugGetAndSaveAllWindowsList()
		FncTestEnding(False, $sInnerMsg)
	EndIf

EndFunc

Func DebugGetAndSaveAllWindowsList()

	_FileWriteLog($sDebugLogFile, "---------------------------------------")
	_FileWriteLog($sDebugLogFile, "������ ���� ���� �� ������ ��������:")

	Local $aList = WinList()

	For $i = 1 To $aList[0][0]
		_FileWriteLog($sDebugLogFile, "Title: " & $aList[$i][0] & @CRLF & "Handle: " & $aList[$i][1])
	Next

EndFunc