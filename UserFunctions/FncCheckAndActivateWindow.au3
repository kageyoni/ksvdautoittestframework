Func FncCheckAndActivateWindow($sWindowNameInner, $sClass = "")

	Local $iTimeout = IniRead ( $sCommonConfig, "BasicInformation", "timeout", "$iTimeout" )
	Local $bResult = False
	Local $sError

	_FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "����c� �������� ������������� � ��������� ���� " & $sWindowNameInner )

	If WinWait($sWindowNameInner , "", $iTimeout) Then
		_FileWriteLog($sLogFile, "���� " & $sWindowNameInner & " ����������")
		If $sClass <> "" Then
			If WinActive ("[CLASS:" & $sClass & "]") Then 
				_FileWriteLog($sLogFile, "���� " & $sWindowNameInner & " ������ " & $sClass & " �������")
				$bResult = True
			Else
				If WinActivate("[CLASS:" & $sClass & "]") Then
					_FileWriteLog($sLogFile, "���� " & $sWindowNameInner & " ������ " & $sClass & " ������� ��������")
					$bResult = True
				Else
					$sError = "�� ������� ������� ���� " & $sWindowNameInner & " ������ " & $sClass & " ��������"
					If $bExitIfAuthFail = True Then
						FncTestEnding(False, $sError)
					Else
						_FileWriteLog($sLogFile, $sError & ". ���������� ����� ����� ����� ����������")
					EndIf
				EndIf
			EndIf

		Else
			If WinActive ( $sWindowNameInner ) Then 
				_FileWriteLog($sLogFile, "���� " & $sWindowNameInner & " �������")
				$bResult = True
			Else
				If WinActivate($sWindowNameInner) Then
					_FileWriteLog($sLogFile, "���� " & $sWindowNameInner & " ������� ��������")
					$bResult = True
				Else
					$sError = "�� ������� ������� ���� " & $sWindowNameInner & " ��������"
					If $bExitIfAuthFail = True Then
						FncTestEnding(False, $sError)
					Else
						_FileWriteLog($sLogFile, $sError & ". ���������� ����� ����� ����� ����������")
					EndIf
				EndIf
			EndIf
		EndIf
		
	Else
		$sError = "���� " & $sWindowNameInner & " �� ��������� �� " & $iTimeout & " ���"
		If $bExitIfAuthFail = True Then
			FncTestEnding(False, $sError)
		Else
			_FileWriteLog($sLogFile, $sError & ". ���������� ����� ����� ����� ����������")
		EndIf
	EndIf

	Return $bResult
	
EndFunc

;������� ������ �������� ����� ��������� ���������� �� ���� � ������ ��� �������� (����� ��� ��������� ����������)