Func FncDateTimeChange($sOperation, $iValue, $sCurrentDateTime)

   Local $sFuncName = "FncDateTimeChange. "

   _FileWriteLog($sLogFile, "������� �������� ����/�������: " & $sCurrentDateTime)

   Global $aMyDate, $aMyTime
   _DateTimeSplit($sCurrentDateTime, $aMyDate, $aMyTime)

   If $sOperation = "Plus" Then
	  _FileWriteLog($sLogFile, "�������� ������ ��������� ����� ����� �� " & $iValue & " �����")
	  $aMyTime[2] = $aMyTime[2] + $iValue

	  While $aMyTime[2] > 60 ;���� �������� ����� ������ 60
		 $aMyTime[1] = $aMyTime[1] + 1 ; ���������� 1 � �����
		 $aMyTime[2] = $aMyTime[2] - 60 ;�������� 60 �� �����
	  WEnd
	  _FileWriteLog($sDebugLogFile, $sFuncName & "�������� ������, ���� �������� ����� > 60: " & $aMyTime[2])
	  _FileWriteLog($sDebugLogFile, $sFuncName & "�������� ���, ���� �������� ����� > 60: " & $aMyTime[1])

	  While $aMyTime[1] > 24 ;���� �������� ����� ������ 24
		 $aMyDate[3] = $aMyDate[3] + 1 ;���������� 1 � ������� ����
		 $aMyTime[1] = $aMyTime[1] - 24 ;�������� 24 �� �����
	  WEnd

	  _FileWriteLog($sDebugLogFile, $sFuncName & "�������� ���, ���� �������� ����� > 24: " & $aMyTime[1])
	  _FileWriteLog($sDebugLogFile, $sFuncName & "�������� ����, ���� �������� ����� > 24: " & $aMyDate[3])


   ElseIf $sOperation = "Minus" Then
	  _FileWriteLog($sLogFile, "�������� ������ �������� ����� ����� �� " & $iValue & " �����")
	  $aMyTime[2] = $aMyTime[2] - $iValue

	  While $aMyTime[2] < 0 ;���� �������� ����� � ������
		 $aMyTime[1] = $aMyTime[1] - 1 ;�������� 1 �� �����
		 $aMyTime[2] = $aMyTime[2] + 60 ;���������� 60 � �������
	  WEnd
	  _FileWriteLog($sDebugLogFile, $sFuncName & "�������� ������, ���� �������� ����� < 0: " & $aMyTime[2])
	  _FileWriteLog($sDebugLogFile, $sFuncName & "�������� ���, ���� �������� ����� < 0: " & $aMyTime[1])

	  While $aMyTime[1] < 0 ;���� �������� ����� � ������
		 $aMyDate[3] = $aMyDate[3] - 1 ;�������� 1 �� ������� ����\
		 $aMyTime[1] = $aMyTime[1] + 24 ;���������� 24 � �����
	  WEnd
	  _FileWriteLog($sDebugLogFile, $sFuncName & "�������� ���, ���� �������� �����  < 0: " & $aMyTime[1])
	  _FileWriteLog($sDebugLogFile, $sFuncName & "�������� ����, ���� �������� �����  < 0: " & $aMyDate[3])
   EndIf

   InnerFncMonthCheck() ; ---- �������� ���-�� ������� (���� ������ 1 ��� ������ 12)
   InnerFncDayCheck()   ; ---- �������� �� ���-�� ���� � ���� (� ����������� �� ������)
   ;��������, ����������� ��������� ����� ������� ��������, ���� �� ����� ����� ������ � �����/�������. ����� �������� �������� �� ������� ����������� �������, �� ������������� ������� ����������� ������� �� �����������

   ; ---- ��������� �� ���� ���������� (����� ���� � �����) 0, ���� ���������� ������ 10 (��� GUI ���� �������� �������). ��������� �����, ��������, ��� 8 - 08

	  If $aMyDate[2] < 10 Then
		 $aMyDate[2] = "0" & $aMyDate[2]
	  EndIf
	  If $aMyDate[3] < 10 Then
		 $aMyDate[3] = "0" & $aMyDate[3]
	  EndIf
	  If $aMyTime[1] < 10 Then
		 $aMyTime[1] = "0" & $aMyTime[1]
	  EndIf
	  If $aMyTime[2] < 10 Then
	  $aMyTime[2] = "0" & $aMyTime[2]
	  EndIf
	  If $aMyTime[3] < 10 Then
		 $aMyTime[3] = "0" & $aMyTime[3]
	  EndIf

   Local $aFinalDateTime = $aMyDate ;�������� ��������� ������
   _ArrayAdd ($aFinalDateTime, $aMyTime)
   Local $sFinalDateTimeForLog = $aFinalDateTime[1] & "/" & $aFinalDateTime[2] & "/" & $aFinalDateTime[3] & " " & $aFinalDateTime[5] & ":" & $aFinalDateTime[6] & ":" & $aFinalDateTime[7]
   _FileWriteLog($sLogFile, "�������� �������� ���������� �������: " & $sFinalDateTimeForLog)
   Return $aFinalDateTime
EndFunc



Func InnerFncMonthCheck()

   Local $sFuncName = "FncDateTimeChange. InnerFncMonthCheck. "

	  ; ---- ���� ���������� ������ ���������� ������ 12
   	  If $aMyDate[2] > 12 Then
		 $aMyDate[1] = $aMyDate[1] + 1 ;���������� 1 � ����
		 $aMyDate[2] = $aMyDate[2] - 12 ;�������� 12 �� �������
	  EndIf
	  ; ---- ���� ���������� ������ ���������� 0
   	  If $aMyDate[2] = 0 Then
		 $aMyDate[1] = $aMyDate[1] - 1 ;�������� 1 �� ����
		 $aMyDate[2] = $aMyDate[2] + 1 ;���������� 1 � ������
	  EndIf
	  	  ; ---- ���� ���������� ������ ���������� ������ 0
   	  If $aMyDate[2] = 0 Then
		 $aMyDate[1] = $aMyDate[1] - 1 ;�������� 1 �� ����
		 $aMyDate[2] = $aMyDate[2] + 12 ;���������� 12 � ������
	  EndIf
EndFunc

Func InnerFncDayCheck() ; ---- �������� �� ���-�� ���� � ����

   Local $sFuncName = "FncDateTimeChange. InnerFncDayCheck. "


   _FileWriteLog($sDebugLogFile, $sFuncName & "��� :" & $aMyDate[1])
   _FileWriteLog($sDebugLogFile, $sFuncName & "����� :" & $aMyDate[2])
   _FileWriteLog($sDebugLogFile, $sFuncName & "���� :" & $aMyDate[3])

   Local $aLastDayInMonth2019[] = [0,31,28,31,30,31,30,31,31,30,31,30,31];����-���������. ������ �������� ������������ ���-�� ���� � ������ �� ������� 2019 ����. ������ �������� 0 -�������, ����� �� ������������ ������
	  ;���� ���� ���������� ������ ���������� � ������� ������
   	  If $aMyDate[3] > $aLastDayInMonth2019[$aMyDate[2]] Then
		 _FileWriteLog($sDebugLogFile, $sFuncName & "���� ���� ���������� ������ ���������� � ������� ������")
		 _FileWriteLog($sDebugLogFile, $sFuncName & "�������� ���� � ���������� $aMyDate[3] :" & $aMyDate[3])
		 _FileWriteLog($sDebugLogFile, $sFuncName & "������������ �������� ���� � ������� ������ :" & $aLastDayInMonth2019[$aMyDate[2]])
		 $aMyDate[2] = $aMyDate[2] + 1 ;���������� 1 �����
		 $aMyDate[3] = $aMyDate[3] - $aLastDayInMonth2019[$aMyDate[2]]
	  EndIf
	  ;���� ���� ���������� ������ ����
	  If $aMyDate[3] < 0 Then
		 _FileWriteLog($sDebugLogFile, $sFuncName & "���� ���� ���������� ������ ����")
		 $aMyDate[2] = $aMyDate[2] - 1 ;�������� 1 �����
		 $aMyDate[3] = $aLastDayInMonth2019[$aMyDate[2]] + $aMyDate[3]
		 ;���� ���� ���������� ������ ����
	  ElseIf $aMyDate[3] = 0 Then
		 _FileWriteLog($sDebugLogFile, $sFuncName & "���� ���� ���������� ������ ����")
		 $aMyDate[2] = $aMyDate[2] - 1 ;�������� 1 �����
		 $aMyDate[3] = 1
	  EndIf
EndFunc

; $sCurrentDateTime �� ���� ������� ���������� � ���������� _NowCalc()
; $iValue - ����� � �������
; Return $aFinalDateTime - ���������� �������� ������ ��� �������� 1-3 - ����, 5-7 - �����

; _DateTimeSplit($sCurrentDateTime, $aMyDate, $aMyTime) ��������� ���������� � ����� �������� �� 2 �������. ���������� ��������:
 ; $aMyDate [1] = year
 ; $aMyDate [2] = month
 ; $aMyDate [3] = day

  ; $aMyTime [1] = hour
  ; $aMyTime [2] = min
  ; $aMyTime [3] = sec

  ; ������ �������������:
  ; Local $sCurrentDateTime = _NowCalc()
  ; Local $aSelectedDateTime = FncDateTimeChange("Plus", 15, $sCurrentDateTime)