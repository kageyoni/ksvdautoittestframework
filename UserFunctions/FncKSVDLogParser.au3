Func FncKSVDLogParser($sLogPath, $sKeyWord, $sCurrentDateTime) ;$sCurrentDateTime = _NowCalc()

	Local $aDate, $aTime
   	_DateTimeSplit($sCurrentDateTime, $aDate, $aTime)
	
	Local $iYear = $aDate[1]
	Local $iMonth = $aDate[2]
	Local $iDay = $aDate[3]
	Local $iHour = $aTime[1]
	Local $iMinute = $aTime[2]
	Local $iSecond = $aTime[3]
	Local $sCurrentDateString = "Current date: "
	
	
	Local $bWriteLogStringInTempArray = false
	local $sParseLogResult
	Local $sParseLogResult2
	Local $bParseSuccess = False
	Local $iInnerCount = 0

	Local $aRawLogArray = FileReadToArray($sLogPath) ;������ ������ ����� �� ������ ��� �����
	Local $iLineCount = UBound($aRawLogArray) ;������� ���-�� ������ � �������
	Local $aFindedLinesOfCurrentDate[$iLineCount] ;�������� ������ ��� ��������� �����

	_FileWriteLog($sLogFile, "�������� �������� ������������ ���� KSVD")
	_FileWriteLog($sDebugLogFile, "��� KSVD, �� �������� ������������ �����: " & $sLogPath)
	_FileWriteLog($sDebugLogFile, "������, �� ������� ������������ �����: " & $sKeyWord)
	

	;---- ���������� �������� ��������----

	Local $iMinusOneMinute = $iMinute - 1
	Local $iPlusOneMinute = $iMinute + 1
	Local $iMinusHour = $iHour
	Local $iPlusHour = $iHour
	Local $iPlusOneDay = $iDay +1

	If $iMinusOneMinute < 0 Then
		$iMinusHour -= 1
		$iMinusOneMinute = 0
	EndIf
	If $iPlusOneMinute > 59 Then
		$iPlusHour += 1
		$iPlusOneMinute = 0
	EndIf

	If $iMinute < 10 Then
		$iMinute = "0" & $iMinute
	EndIf
	If $iDay < 10 Then
		$iDay = "0" & $iDay
	EndIf
	If $iMonth < 10 Then
		$iMonth = "0" & $iMonth
	EndIf

	;---- ���������� ����� ----

	Local $sKeyDateString = $sCurrentDateString & $iDay & "." & $iMonth & "." & $iYear
	Local $sIncorrectKeyDateString = $sCurrentDateString & $iPlusOneDay & "." & $iMonth & "." & $iYear
	_FileWriteLog($sDebugLogFile, "�������� ����� ������: " & $sKeyDateString)


	;---- ������ ������. ���� ������ � ������� ����� � ���� � ���������� ��� ����������� � ���� ���� ������ �� ���� � ������ ----
	_FileWriteLog($sDebugLogFile, "������� ������ ������")
	_FileWriteLog($sDebugLogFile, "����������� ����� ������ � �����: " & $sKeyDateString)
	For $i = 0 To $iLineCount -1

		$sParseLogResult = StringRegExp( $aRawLogArray[$i], $sKeyDateString) ;���� ������ $sKeyDateString � �����. ������� � �� ��� ��������� ������ ��������� �� ��������� ������ $aFindedLinesOfCurrentDate
		;_FileWriteLog($sDebugLogFile, "$sParseLogResult = " & $sParseLogResult)
		;_FileWriteLog($sDebugLogFile, "$aRawLogArray[$i] =  " & $aRawLogArray[$i])
		if $sParseLogResult = 1 Then
				$bParseSuccess = True
				$bWriteLogStringInTempArray = True
				_FileWriteLog($sDebugLogFile, "������� ������: " & $aRawLogArray[$i])
				_FileWriteLog($sDebugLogFile, "����������� ���������������� ������ �� ��������� ������ ���� ����� ���� �� ������� ���������� ������: " & $sIncorrectKeyDateString)
		EndIf

		$sParseLogResult2 = StringRegExp( $aRawLogArray[$i], $sIncorrectKeyDateString) ;���� ������ $sIncorrectKeyDateString � �����. ������� � �� (������������) ������ ���������� �������� �� ��������� ������ $aFindedLinesOfCurrentDate
		if $sParseLogResult2 = 1 Then
				$bWriteLogStringInTempArray = False
				_FileWriteLog($sDebugLogFile, "������� ������: " & $aRawLogArray[$i])
				_FileWriteLog($sDebugLogFile, "��������� ��������� ������ ����� ��� �� ��������� ������")
		EndIf

		If $bWriteLogStringInTempArray = True Then ;������ ��������� ����� �� ��������� ������ $aFindedLinesOfCurrentDate
			$aFindedLinesOfCurrentDate[$iInnerCount] = $aRawLogArray[$i]
			$iInnerCount +=1
		EndIf
	next

	If $bParseSuccess = False Then
		FncTestEnding(False, "����� ������ � ����� � ���� KSVD ��� ���������")
	EndIf

	$bParseSuccess = False
	$iLineCount = UBound($aFindedLinesOfCurrentDate)
	$iInnerCount = 0
	Local $aFindedKeyWordLines[$iLineCount]

	;---- ������ ������. ���� ������ � �������� ������ � ���� � ���������� ��� ����������� � ���� ���� ������ �� ���� � ������ $aFindedKeyWordLines ----
	_FileWriteLog($sDebugLogFile, "������� ������ ������")
	_FileWriteLog($sDebugLogFile, "����������� ����� ������ � �������� ������: " & $sKeyWord)

	For $i = 0 To $iLineCount -1
		$sParseLogResult = StringRegExp( $aFindedLinesOfCurrentDate[$i], $sKeyWord) ;���� ������ $aFindedLinesOfCurrentDate � �����
		
		if $sParseLogResult = 1 Then
			$aFindedKeyWordLines[$iInnerCount] = $aFindedLinesOfCurrentDate[$i]
			$iInnerCount +=1
			$bParseSuccess = True
			_FileWriteLog($sDebugLogFile, "������� ������: " & $aFindedLinesOfCurrentDate[$i])
		EndIf
	next

	If $bParseSuccess = False Then
		FncTestEnding(False, "����� ������ � �������� ������ � ���� KSVD ��� ���������")
	EndIf

	$bParseSuccess = False
	$iLineCount = UBound($aFindedKeyWordLines)

	;---- ������ ������. ���� ������ � �������� �������� (���, ������) ----
	_FileWriteLog($sDebugLogFile, "������� ������ ������")
	_FileWriteLog($sDebugLogFile, "����������� ����� ������ �� �������� : " & $iHour & ":" & $iMinute)
	_FileWriteLog($sDebugLogFile, "����������� ����� ������ �� �������� : " & $iMinusHour & ":" & $iMinusOneMinute)
	_FileWriteLog($sDebugLogFile, "����������� ����� ������ �� �������� : " & $iPlusHour & ":" & $iPlusOneMinute)

	For $i = 0 To $iLineCount -1
		$sParseLogResult = StringRegExp( $aFindedKeyWordLines[$i], $iHour & ":" & $iMinute)
		if $sParseLogResult = 1 Then
			$bParseSuccess = True
			_FileWriteLog($sDebugLogFile, "������� ������: " & $aFindedKeyWordLines[$i] & " �������")
			ExitLoop
		EndIf
		$sParseLogResult = StringRegExp( $aFindedKeyWordLines[$i], $iMinusHour & ":" & $iMinusOneMinute)
		if $sParseLogResult = 1 Then
			$bParseSuccess = True
			_FileWriteLog($sDebugLogFile, "������� ������: " & $aFindedKeyWordLines[$i] & " �������")
			ExitLoop
		EndIf
		$sParseLogResult = StringRegExp( $aFindedKeyWordLines[$i], $iPlusHour & ":" & $iPlusOneMinute)
		if $sParseLogResult = 1 Then
			$bParseSuccess = True
			_FileWriteLog($sDebugLogFile, "������� ������: " & $aFindedKeyWordLines[$i] & " �������")
			ExitLoop
		EndIf
	Next

	If $bParseSuccess = False Then
		FncTestEnding(False, "����� ������ � �������� ������ ����������� �� ������� � ���� KSVD ��� ���������")
	Else
		_FileWriteLog($sLogFile, "������: " & $aFindedKeyWordLines[$i] & " � ����: " & $sLogPath & " �������")
	EndIf


	If @error Then
		_FileWriteLog($sLogFile, "������ FncKSVDLogParser")
		_FileWriteLog($sLogFile, "Exit 0")
		_FileWriteLog($sLogFile, $TestFailed)
		_WinAPI_CreateFile($sResultsFolderPath & "\fail.txt", 1) ; �� ������� ������� ����� ������������ ��� �� �������� ����
		Exit 0
	else
	Endif
EndFunc

Func FncWriteFile($sParseLogResult) ;

	Local $hFileOpen = FileOpen($sResultsFolderPath & "\ParseLogResult.txt", $FO_APPEND)
    If $hFileOpen = -1 Then
		_FileWriteLog($sLogFile, "An error occurred whilst writing the temporary file.")
		Return False
	EndIf
	FileWrite( $hFileOpen, $sParseLogResult )

	If @error Then
		_FileWriteLog($sLogFile, "������ FncKSVDLogParser")
		_FileWriteLog($sLogFile, "Exit 0")
		_FileWriteLog($sLogFile, $TestFailed)
		_WinAPI_CreateFile($sResultsFolderPath & "\fail.txt", 1) ; �� ������� ������� ����� ������������ ��� �� �������� ����
		Exit 0
	else
	Endif
EndFunc