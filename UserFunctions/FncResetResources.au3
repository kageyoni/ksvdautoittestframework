Func FncResetResources()

   ; ---- ����� ����� ����������� ��������� � �������� ---

   Local $iID = 105
   IniWrite( $sAddedObjectsConfig, "AddedObjects", "startID", $iID)
   IniWrite( $sAddedObjectsConfig, "AddedObjects", "countOfAddedArchives ", "0")
   IniWrite( $sAddedObjectsConfig, "AddedObjects", "countOfAddedCameras", "0")
   IniWrite( $sAddedObjectsConfig, "AddedObjects", "countOfAddedRegions", "0")
   IniWrite( $sAddedObjectsConfig, "AddedObjects", "countOfAddedObjects", "0")
   IniWrite( $sAddedObjectsConfig, "AddedObjects", "totalCountOfAddedRegionsObjects", "0")
   IniWrite( $sAddedObjectsConfig, "AddedObjects", "countOfAddedRtspECHD", "0" )
   IniWrite( $sCommonConfig, "BasicInformation", "exportOperationNumber","0")
   IniWrite( $sKsvdAppsParams, "KsvdAppsParams", "armAdminWindowIsMaximized", "False")
   IniWrite( $sKsvdAppsParams, "KsvdAppsParams", "armOperatorFirstRun", "True")
   IniWrite ( $sKsvdAppsParams, "KsvdAppsParams", "armOperatorWindowIsMaximized", "False")

   local $aIniSectionToErase
   Local $iArrayLenght

   ; ---- ������� ������� �� ������� ����������� ���������
   $aIniSectionToErase = IniReadSectionNames($sAddedObjectsConfig)
   For $element IN $aIniSectionToErase
	  If $element <> "AddedObjects" Then
		 IniDelete ( $sAddedObjectsConfig, $element)
	  EndIf
   Next

   ; ---- ������� �������� ����������� ������������� KSVD
   $aIniSectionToErase = IniReadSectionNames($sUsersConfig)
   $iArrayLenght = $aIniSectionToErase[0]
   For $i = 1 To $iArrayLenght
        IniWrite ( $sUsersConfig, $aIniSectionToErase[$i], "isAdded", "False" )
   Next
   IniWrite ( $sUsersConfig, "admin", "isAdded", "True" )

      ; ---- ������� �������� ����������� �������
   $aIniSectionToErase = IniReadSectionNames($sArchivesConfig)
   $iArrayLenght = $aIniSectionToErase[0]
   For $i = 1 To $iArrayLenght
      IniWrite ( $sArchivesConfig, $aIniSectionToErase[$i], "isAdded", "False" )
		IniWrite ( $sArchivesConfig, $aIniSectionToErase[$i], "logPath", "" )
		IniWrite ( $sArchivesConfig, $aIniSectionToErase[$i], "id", "" )
      IniWrite ( $sArchivesConfig, $aIniSectionToErase[$i], "archiveRecordingStartTime", "" )
		Local $iCountOfAddedCamerasToArchive = IniRead ( $sArchivesConfig, $aIniSectionToErase[$i], "countOfAddedCamerasToArchive", "$iCountOfAddedCamerasToArchive")

		For $x = 1 To $iCountOfAddedCamerasToArchive
		   IniWrite ( $sArchivesConfig, $aIniSectionToErase[$i], "addedCamera" & $x, "")
		Next

		IniWrite ( $sArchivesConfig, $aIniSectionToErase[$i], "countOfAddedCamerasToArchive", "0" )
   Next

   ; ---- ������� �������� ����������� ��������������
   $aIniSectionToErase = IniReadSectionNames($sRTSPConfig)
   $iArrayLenght = $aIniSectionToErase[0]
   For $i = 1 To $iArrayLenght
	  IniWrite ( $sRTSPConfig, $aIniSectionToErase[$i], "isAdded", "False" )
	  IniWrite ( $sRTSPConfig, $aIniSectionToErase[$i], "id", "" )
	  IniWrite ( $sRTSPConfig, $aIniSectionToErase[$i], "retranslatedCameraId", "" )
    IniWrite ( $sRTSPConfig, $aIniSectionToErase[$i], "retranslatedCameraName", "")
   Next

   ; ---- ������� �������� ����������� ����� ����������
   $aIniSectionToErase = IniReadSectionNames($sUpdateSources)
   $iArrayLenght = $aIniSectionToErase[0]
   For $i = 1 To $iArrayLenght
	  IniWrite ( $sUpdateSources, $aIniSectionToErase[$i], "isAdded", "False" )
   Next

      ; ---- ������� �������� ����������� �������� ��������
   $aIniSectionToErase = IniReadSectionNames($sTileServersConfig) 
   $iArrayLenght = $aIniSectionToErase[0]
   For $i = 1 To $iArrayLenght
	  IniWrite ( $sTileServersConfig, $aIniSectionToErase[$i], "isAdded", "False" )
   Next

   ; ---- ������� �������� � ���������� ����������� �����
   $aIniSectionToErase = IniReadSectionNames($sCamerasConfig)
   $iArrayLenght = $aIniSectionToErase[0]
   For $i = 1 To $iArrayLenght
	  IniWrite( $sCamerasConfig, $aIniSectionToErase[$i], "isAdded", "False" )
	  IniWrite( $sCamerasConfig, $aIniSectionToErase[$i], "archiveRecordingStartTime", "" )
	  IniWrite( $sCamerasConfig, $aIniSectionToErase[$i], "logPath", "" )
	  IniWrite( $sCamerasConfig, $aIniSectionToErase[$i], "id", "" )
	  IniWrite( $sCamerasConfig, $aIniSectionToErase[$i], "orderNumber", "" )
	  IniWrite( $sCamerasConfig, $aIniSectionToErase[$i], "addedToArchive", "" )
     IniWrite( $sCamerasConfig, $aIniSectionToErase[$i], "addedToRegionObject", "" )
     IniWrite( $sCamerasConfig, $aIniSectionToErase[$i], "isAddedToRtspEchd", "False")
     IniWrite( $sCamerasConfig, $aIniSectionToErase[$i], "isAddedToArchive", "False")
     IniWrite( $sCamerasConfig, $aIniSectionToErase[$i], "isActive", "False")
   Next

   ; ---- ������� ������� �� ������� ����������� ��������
   $aIniSectionToErase = IniReadSectionNames($sRegionsConfig)
   Local $sError = @error
   If $sError = 1 Then
	  _FileWriteLog($sDebugLogFile, "������ �� �������� ��������")
   Else
	  For $element IN $aIniSectionToErase
		 _FileWriteLog($sLogFile, "$element = " &$element)
		 IniDelete ( $sRegionsConfig, $element)
	  Next
   EndIf

   ; ---- ������� ������� �� ������� ����������� �������� ��������

   $aIniSectionToErase = IniReadSectionNames($sRegionsObjectsConfig)
   Local $sError = @error
   If $sError = 1 Then
	  _FileWriteLog($sDebugLogFile, "������ �� �������� ��������")
   Else
	  For $element IN $aIniSectionToErase
		 _FileWriteLog($sLogFile, "$element = " &$element)
		 IniDelete ( $sRegionsObjectsConfig, $element)
	  Next
   EndIf

      ; ---- ������� ������� �� ������� ����������� ���������

   $aIniSectionToErase = IniReadSectionNames($sViewPanelsConfig)
   Local $sError = @error
   If $sError = 1 Then
	  _FileWriteLog($sDebugLogFile, "������ �� �������� ���������")
   Else
	  For $element IN $aIniSectionToErase
		 _FileWriteLog($sLogFile, "$element = " &$element)
		 IniDelete ( $sViewPanelsConfig, $element)
	  Next
   EndIf
   IniWrite( $sViewPanelsConfig, "CountOfAddedViewPanels", "countOfAddedViewPanels", "0")
   








 ; ---- ������� ����� ��������� �������� � ����������� ����� ������ ---

 Local $sArchiveDir = IniRead ( $sCommonConfig, "BasicInformation", "archiveDir", "$sArchiveDir" )
 Local $sExportFromArchiveDir = IniRead ( $sCommonConfig, "BasicInformation", "exportFromArchiveDir", "$sExportFromArchiveDir" )
 If FileExists ($sArchiveDir) Then
	_FileWriteLog($sDebugLogFile, "������� ����� ��������� ������ ��������")
	DirRemove ( $sArchiveDir, 1 ) ;�������� ���������� �� ����� ���������� �������
	DirCreate ( $sArchiveDir ) ;������������ ������ ����������, �.�. KSVD (�� ��� �������� ������) �� ������ ����������
 EndIf

  If FileExists ($sExportFromArchiveDir) Then
	 _FileWriteLog($sDebugLogFile, "������� ����� ��������� ����� ������ ����������� �� ��������")
	DirRemove ( $sExportFromArchiveDir, 1 ) ;�������� ���������� �� ����� ���������� �������
		DirCreate ( $sExportFromArchiveDir ) ;������������ ������ ����������, �.�. KSVD (�� ��� �������� ������) �� ������ ����������
 EndIf
EndFunc
