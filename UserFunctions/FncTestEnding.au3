Func FncTestEnding($bSuccess, $sMsg)

	_FileWriteLog($sLogFile, $sMsg)
	Local $bCleanResult = FncRemoveAddedElements()
	
	If $bSuccess = True Then
		If $bCleanResult = True Then
			InnerCheckAndCloseKSVDWindows()
			InnerCreateResultFile(True, $sMsg)
		Else
			InnerCheckAndCloseKSVDWindows()
			InnerCreateResultFile(False, "�� ������� �������� ����������� �������� KSVD")
		EndIf
	Else
		If $bCleanResult = True Then
			InnerCheckAndCloseKSVDWindows()
			InnerCreateResultFile(False, $sMsg)
		Else
			InnerCheckAndCloseKSVDWindows()
			InnerCreateResultFile(False, "�� ������� �������� ����������� �������� KSVD")
		EndIf
	EndIf

EndFunc

Func InnerCheckAndCloseKSVDWindows()

	Local $aWindows[8] = [$sArmAdminWindowName,	$sArmExportFromArchiveWindowName, $sArmOperatorWindowName, $sArmSettingsWindowName,	$sArmMonitorWindowName,	"KSVD ��������", "������������� � Kraftway Smart Video Detector", "��������� � Kraftway Smart Video Detector"]

	for $velement in $aWindows
		if WinExists($velement) Then
			_FileWriteLog($sDebugLogFile, "���������� �������� ���� " & $velement)
			if $velement = $sArmAdminWindowName Then
				FncCloseArmAdmin()
			ElseIf $velement = $sArmExportFromArchiveWindowName Then
				ActionCloseArmExportFromArchive()
			ElseIf $velement = $sArmOperatorWindowName Then
				ActionCloseArmOperator()
			Else
				FncCloseWindow($velement)
			EndIf
		EndIf
	next

EndFunc

Func InnerCreateResultFile($bSuccess, $sMsg)

	_FileWriteLog($sLogFile, "---------------------------------------")

	If $bSuccess = true Then
		_FileWriteLog($sLogFile, $sMsg)
		_WinAPI_CreateFile($sResultsFolderPath & "\success.txt", 1) ; ������ ����, �� ������� �������� ���������� �������� ����������� �����
	Else
		_FileWriteLog($sLogFile, $TestFailed)
		_FileWriteLog($sLogFile, "������: " & $sMsg)
		_WinAPI_CreateFile($sResultsFolderPath & "\fail.txt", 1) ; �� ������� ������� ����� ������������ ��� �� �������� ����
	EndIf

	; ����� ������
	If @error Then
		_FileWriteLog($sLogFile, "������ FncTestEnding")
		_FileWriteLog($sLogFile, "Exit 0")
		_FileWriteLog($sLogFile, $TestFailed)
		_WinAPI_CreateFile($sResultsFolderPath & "\fail.txt", 1) ; �� ������� ������� ����� ������������ ��� �� �������� ����
		Exit 0
	Endif

	Exit 0

EndFunc