Func FncRemoveAddedElements()

    Local $bSuccessOperation = False
    Local $aResultOfCleaning[6]

    Local $sType, $sName
    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "����c� �������� �������� ����������� ��������� KSVD")

    If $aAddedElementsInKSVD[0][0] = "" Then
        _FileWriteLog($sLogFile, "��� ����������� ��������� ��� ��������" )
        $bSuccessOperation = True
    Else
        Local $iResult = WinExists ($sArmAdminWindowName)

        if $iResult <> 1 Then
            ActionRunArmAdmin()
            ActionLogInArmAdmin("admin")
        EndIf

        _FileWriteLog($sLogFile, "������ ����������� ��������� KSVD ��� ��������:")
        For $i = 0 To $iAddedCount Step +1
            $sType = $aAddedElementsInKSVD[$i][$i]
            $sName = $aAddedElementsInKSVD[$i][$i+1]
            _FileWriteLog($sLogFile, "...�������: " & $sType & ". ���: " & $sName)
        Next


        For $i = 0 To $iAddedCount Step +1

            $sType = $aAddedElementsInKSVD[$i][$i]
            $sName = $aAddedElementsInKSVD[$i][$i+1]

            If $sType = "Camera" Then
                $aResultOfCleaning[$i] = ActionRemoveCameraInGUI($sName)
            ElseIf $sType = "RtspRetranslator" Then
                $aResultOfCleaning[$i] = ActionRemoveRtspRetranslatorInGui($sName)
            ElseIf $sType = "User" Then
                $aResultOfCleaning[$i] = ActionRemoveUserInGui($sName)
            ElseIf $sType = "Archive" Then
                $aResultOfCleaning[$i] = ActionRemoveArchiveInGui($sName)
            ElseIf $sType = "Region" Then
                $aResultOfCleaning[$i] = ActionRemoveRegion($sName)
            ElseIf $sType = "TileServer" Then
                $aResultOfCleaning[$i] = ActionRemoveTileServer($sName)
            ElseIf $sType = "UpdateSource" Then
                $aResultOfCleaning[$i] = ActionRemoveUpdateSource($sName)
            Else
                _FileWriteLog($sLogFile, "�������� ������������ �������� ��������� $sType = " & $sType)
                $bSuccessOperation = False
                return $bSuccessOperation
            EndIf

            If $aResultOfCleaning[$i] = True Then
                _FileWriteLog($sLogFile, "������� ����� ������� " & $sType & " � ������ " & $sName)
            Else
                _FileWriteLog($sLogFile, "��� �������� �������� " & $sType & " � ������ " & $sName & " ��������� ������")
                $bSuccessOperation = False
                return $bSuccessOperation
            EndIf
        Next

        $bSuccessOperation = True

        For $i = 0 To $iAddedCount
            If $aResultOfCleaning[$i] = False Then
                $bSuccessOperation = False
                ExitLoop
            EndIf
        Next
        
    EndIf

    return $bSuccessOperation

EndFunc