Func ActionRemoveRtspRetranslatorInGui($sRtspECHD)

    Local $bSuccessOperation = False
    Local $sGuiName = IniRead( $sRTSPConfig, $sRtspECHD, "guiName", "$sGuiName" )
    Local $sIsAdded = IniRead( $sRTSPConfig, $sRtspECHD, "isAdded", "$sIsAdded" )
    Local $sCountOfAddedRtspECHD = IniRead( $sAddedObjectsConfig, "AddedObjects", "countOfAddedRtspECHD", "$sCountOfAddedRtspECHD")
    Local $sAddedCameraName = IniRead( $sRTSPConfig, $sRtspECHD, "retranslatedCameraName", "$sCamera")

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� �������� ������������� " & $sGuiName)

    FncCheckAndActivateWindow($sArmAdminWindowName)

    If $sIsAdded = "False" Then
        _FileWriteLog($sLogFile, "�������� �������� ����������, ��������� ������������ " & $sGuiName & " �� �������� �� ������")
        $bSuccessOperation = True
    ElseIf $sIsAdded = "True" Then

        ActionFindRtspRetranslatorInGui($sRtspECHD)

        $sAction = "���� �� ������ �������� �������������"
        FncMouseClick("left",50,100,1)

        $sAction = "������������� �������� ��������� �������� ENTER"
        FncSendData("{ENTER}")

        $sCountOfAddedRtspECHD -= 1
        IniWrite( $sAddedObjectsConfig, "AddedObjects", "countOfAddedRtspECHD", $sCountOfAddedRtspECHD)
        IniWrite( $sRTSPConfig, $sRtspECHD, "isAdded", "False" )
        IniWrite( $sRTSPConfig, $sRtspECHD, "retranslatedCameraName", "")
        IniWrite( $sCamerasConfig, $sAddedCameraName, "isAddedToRtspEchd", "False")
        _FileWriteLog($sLogFile, "������������ " & $sRtspECHD & " ������� ������")
        $bSuccessOperation = True
    Else
        _FileWriteLog($sLogFile, "�������� ������������� �������� ��������� $sIsAdded = " & $sIsAdded )
        $bSuccessOperation = False
    EndIf
    return $bSuccessOperation
EndFunc