Func ActionAddCameraToObjectByScada($sRegionName, $sObjectName, $sCamera)

    Local $sRegionAdded = IniRead ($sRegionsConfig, $sRegionName, "isAdded", "False")
    Local $sObjectAdded = IniRead ($sRegionsObjectsConfig, $sObjectName, "isAdded", "False")
    Local $sCameraAddedToRegionObject = IniRead ($sCamerasConfig, $sCamera, "addedToRegionObject", "null")

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� ���������� ������ " & $sCamera & " � ������� " & $sObjectName & "������� " &  $sRegionName)
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "������ �������� ���������� ������ " & $sCamera & " � ������� " & $sObjectName & "������� " &  $sRegionName)

    If $sRegionAdded = "False" Then
        _FileWriteLog($sLogFile, "������ " &  $sRegionName & " �� ��������. ������ �������� ����������")
		ActionAddRegionByScada($sRegionName)
    ElseIf $sRegionAdded = "True" Then
    Else
        FncTestEnding(False, "�������� ������������� �������� ��������� $sRegionAdded " & $sRegionAdded )
    EndIf

    If $sObjectAdded = "False" Then
        ActionAddObjectToRegionByScada($sRegionName, $sObjectName)
    ElseIf $sObjectAdded = "True" Then
    Else
        FncTestEnding(False, "�������� ������������� �������� ��������� $sObjectAdded " & $sObjectAdded )
    EndIf

    If $sCameraAddedToRegionObject = $sObjectName Then
        _FileWriteLog($sLogFile, "������ " & $sCamera & " ��� ��������� � ������� " & $sObjectName)
    ElseIf $sCameraAddedToRegionObject = "null" Then
        FncTestEnding(False, "�� ������� ��������� �������� addedToRegionObject �� ������� ������ " & sCamera)
    ElseIf $sCameraAddedToRegionObject = "" Then
        InnerDoAddCameraToObject($sRegionName, $sObjectName, $sCamera)
    Else
        FncTestEnding(False, "������ " & $sCamera & " ��� ��������� � ������� ������� " & $sCameraAddedToRegionObject)
    EndIf
EndFunc

func InnerDoAddCameraToObject($sRegionName, $sObjectName, $sCamera)

    _FileWriteLog($sLogFile, "�������� �������� ���������� ������ " & $sCamera & " � ������� " & $sObjectName)

    Local $iCountOfAddedToRegionsObjectCameras = IniRead ($sRegionsObjectsConfig, $sObjectName , "countOfAddedToRegionsObjectCameras", "0")

    ActionFindRegionObjectInScada($sRegionName, $sObjectName, False)

    $sAction = "���� �� ������ ���������� ������"
	FncMouseClick("left",50,135,1)

    FncCheckAndActivateWindow("����������� �����")

	$sAction = "���� �� ���� �������"
	FncMouseClick("left",62,20,1)

	Local $sIsCameraAddedToServer = IniRead ($sCamerasConfig, $sCamera, "isAdded", "False")
	Local $sCameraGuiName = IniRead ($sCamerasConfig, $sCamera, "cameraGuiName", "$sCameraGuiName")
	_FileWriteLog($sLogFile, "$sCamera = " & $sCamera)

	If $sIsCameraAddedToServer = "False" Then
		_FileWriteLog($sLogFile, "������ " & $sCamera & " �� ��������� �� ������")
		FncTestEnding(False, "������ " & $sCamera & " �� ����� ��������� � �������, �.�. �� ��������� �� ������")
	ElseIf $sIsCameraAddedToServer = "True" Then
		$sAction = "���� ����� ������ � ���� ������"
		FncSendData($sCameraGuiName)
		$sAction = "���� �� ��������� ������"
		FncMouseClick("left",70,75,1)
		$sAction = "���� �� ������ ���������� ������ � ������"
		FncMouseClick("left",285,202,1)
		$sAction = "���� �� ������ ������� �������"
		FncMouseClick("left",253,20,1)

        $sAction = "������ " & $sCamera & " ��������� � ������� " & $sObjectName & " ������� " & $sRegionName
	  	 _FileWriteLog($sLogFile, $sAction)
	  	 FncScreenCapture($sKeyScreenPath, $sAction)

        $iCountOfAddedToRegionsObjectCameras = $iCountOfAddedToRegionsObjectCameras + 1
		IniWrite($sRegionsObjectsConfig, $sObjectName, "countOfAddedToRegionsObjectCameras", $iCountOfAddedToRegionsObjectCameras)
        IniWrite($sRegionsObjectsConfig, $sObjectName, "addedCamera" & $iCountOfAddedToRegionsObjectCameras, $sCamera)
        IniWrite($sCamerasConfig, $sCamera, "addedToRegionObject", $sObjectName)
        _FileWriteLog($sLogFile, "������ " & $sCamera & " ������� ��������� � ������� " & $sObjectName)
	EndIf

	$sAction = "���� �� ������ ��"
	FncMouseClick("left",435,440,1)
    
endfunc