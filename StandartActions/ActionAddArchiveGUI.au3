Func ActionAddArchiveGUI($sArchive)

	Local $sName = IniRead ( $sArchivesConfig, $sArchive, "name", "$sName")
	Local $sGuiName = IniRead ( $sArchivesConfig, $sArchive, "guiName", "$sGuiName")
	Local $sDescription = IniRead ( $sArchivesConfig, $sArchive, "description", "$sDescription")
	Local $sClusterSize = IniRead ( $sArchivesConfig, $sArchive, "clusterSize", "$sClusterSize")
	Local $sPageSize = IniRead ( $sArchivesConfig, $sArchive, "pageSize", "$sPageSize")
	Local $sArchiveSize = IniRead ( $sArchivesConfig, $sArchive, "archiveSize", "$sArchiveSize")
	Local $sArchiveDepthMin = IniRead ( $sArchivesConfig, $sArchive, "archiveDepthMin", "$sArchiveDepthMin")
	Local $sIsAdded = IniRead ( $sArchivesConfig, $sArchive, "isAdded", "$sIsAdded")
	Local $sLogName = "_ksvd4_repc"
	Local $sPath = $sArchiveDir & "\"
	Local $sFuncName = "ActionAddArchiveGUI. "
	Local $sFileName = $sGuiName & ".bin"
	Local $sFilePath = $sPath & $sFileName

	_FileWriteLog($sLogFile, "---------------------------------------")
	_FileWriteLog($sLogFile, "������ �������� ���������� ��������� " & $sArchive)
	_FileWriteLog($sDebugLogFile, "---------------------------------------")
	_FileWriteLog($sDebugLogFile, "������ �������� ���������� ��������� " & $sArchive)

	If $sIsAdded = "True" Then
		_FileWriteLog($sLogFile, "��������� " & $sArchive & " ����� ��� ���� ���������")
		ActionFindArchiveInGui ($sArchive)
	Else

		InnerFncCheckOldArchiveFileExists($sFilePath)

		ActionClearFilterFieldInArmAdmin()

		$sAction = "���� ������� ���������"
		FncSendData("`") ;���� ������� ������� ��������� � ������ ������ �������

		$sAction = "���� �� ������� ���������"
		If $sVersion = "2.8" Then
			FncMouseClick("left",370,185,1)
		ElseIf $sVersion = "2.10" Then
			FncMouseClick("left",370,145,1)
		EndIf
		
		$sAction = "���� �� ���� ���"
		FncMouseClick("left",160,580,1)
		$sAction = "���� ����� ���������"
		FncSendData($sName)

		$sAction = "���� �� ���� ��������"
		FncMouseClick("left",160,610,1)
		$sAction = "���� ����� ��������"
		FncSendData($sDescription)

		$sAction = "���� �� ���� ����"
		FncMouseClick("left",160,640,1)
		$sAction = "���� ���� �������� ���������"
		FncSendData($sFilePath)

		$sAction = "���� �� ���� �������"
		FncMouseClick("left",160,670,1)
		$sAction = "���� ������� ��������"
		FncSendData($sClusterSize)

		$sAction = "���� �� ���� ��������"
		FncMouseClick("left",160,700,1)
		$sAction = "���� ������� ��������"
		FncSendData($sPageSize)

		$sAction = "���� �� ���� ������ ���������"
		FncMouseClick("left",160,730,1)
		$sAction = "���� ������� ���������"
		FncSendData($sArchiveSize)

		If $sVersion = "2.10" Then
			$sAction = "���� �� ���� ����������� ������� ���������"
			FncMouseClick("left",160,760,1)
			$sAction = "���� ������� ����������� ������� ���������"
			FncSendData($sArchiveDepthMin)
		EndIf

		$sAction = "���� �� ������ ������� �� SERVERNAME"
		FncMouseClick("left",90,100,1)
		
		ActionFindArchiveInGui($sArchive)

		InnerFncGetAddedArchiveId($sArchive, $sLogName, $sFilePath)

		FncAddItemsToArrayAddedElementsInKSVD("Archive", $sArchive)

		$sAction = "��������� " & $sArchive & " ������� � GUI"
		_FileWriteLog($sLogFile, $sAction)
		FncScreenCapture($sKeyScreenPath, $sAction)
		
	EndIf
EndFunc



Func InnerFncCheckOldArchiveFileExists($sArchiveFilePath)
	_FileWriteLog($sLogFile, "�������� ������� ����� ���������� ����� ��������� �� �����")
	If FileExists($sArchiveFilePath) Then
		_FileWriteLog($sDebugLogFile, "���� ��������� " & $sArchiveFilePath & " ������, ������������ ��������� ��������")
		Local $iDelete = FileDelete ($sArchiveFilePath)
		If $iDelete Then
			_FileWriteLog($sLogFile, "����� ��������� ���� ��������� ������� �����")
		Else
			_FileWriteLog($sLogFile, "�� ������� ������� ����� ��������� ���� ���������")
			FncTestEnding(False, "�� ������� ������� ����� ��������� ���� ���������")
		EndIf
	Else
		_FileWriteLog($sLogFile, "����� ��������� ���� ��������� �� ���������. ����������� �������� ���������� ���������")
	EndIf
EndFunc

Func InnerFncGetAddedArchiveId($sArchive, $sLogName, $sFilePath)
	Local $sFuncName = "ActionAddArchiveGUI. InnerFncGetAddedArchiveId "

	$sAction = "���� �� ���� ��"
	FncMouseClick("left",120,550,1)

	Local $iID = FncCopyToClipboard()
	Local $sLogPath = $sInstallPath & "\Log\" & "000" & $iID & $sLogName &".log" ;���� �� ��� �����

	IniWrite ( $sArchivesConfig, $sArchive, "id",  $iID) ;������ ID ��������� � ������
	IniWrite ( $sArchivesConfig, $sArchive, "logPath",  $sLogPath) ;������ ���� ��������� � ������
	IniWrite ( $sArchivesConfig, $sArchive, "isAdded", "True")
	IniWrite ( $sArchivesConfig, $sArchive, "filePath",  $sFilePath)

	Local $sCountOfAddedArchives = IniRead ( $sAddedObjectsConfig, "AddedObjects", "countOfAddedArchives ", "$sCountOfAddedArchives")
	$sCountOfAddedArchives = $sCountOfAddedArchives + 1
	IniWrite ( $sAddedObjectsConfig, "AddedObjects", "countOfAddedArchives",  $sCountOfAddedArchives)
EndFunc