Func ActionFindTileServerInGui($sTileServer)

    Local $sGuiName = IniRead ( $sTileServersConfig, $sTileServer, "guiName", "$sGuiName" )
    Local $sIsAdded = IniRead ( $sTileServersConfig, $sTileServer, "isAdded", "$sIsAdded" )

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� ������ ��������� ������� " & $sTileServer )
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "������ �������� ������ ��������� ������� " & $sTileServer )

    If $sIsAdded = "False" Then
		_FileWriteLog($sLogFile, "����� ����������, ��������� �������� ������ " & $sTileServer & " �� �������� �� ������")
    ElseIf $sIsAdded = "True" Then
    
        ActionClearFilterFieldInArmAdmin()

        $sAction = "���� � ���� ������ ����� ��������� �������"
        FncSendData($sGuiName)

        $sAction = "���� �� ��������������� ��������� �������"
        If $sVersion = "2.8" Then
            FncMouseClick("left",360,115,1)
        ElseIf $sVersion = "2.10" Then
            FncMouseClick("left",350,95,1)
        EndIf
        
        $sAction = "������� ���� �� ���� ���"
        FncMouseClick("left",160,580,2)

        Local $sClipboard = FncCopyToClipboard()

        If $sGuiName = $sClipboard Then
            _FileWriteLog($sLogFile, "�������� ������ ������ � GUI")
        Else
            FncTestEnding(False, "�������� ������ " & $sTileServer & " �� ������ � GUI")
        EndIf

    Else
        FncTestEnding(False, "�������� ������������ �������� ��������� $sIsAdded: " & $sIsAdded )
    EndIf

EndFunc