Func ActionAddObjectToRegionByScada($sRegionName, $sObjectName)

    Local $sRegionAdded = IniRead ($sRegionsConfig, $sRegionName, "isAdded", "False")
    Local $sFuncName = "ActionAddObjectToRegionByScada. "
    Local $bObjectAlreadyAdded = False

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� ���������� ������� " & $sObjectName & " � ������� " & $sRegionName)
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "������ �������� ���������� ������� " & $sObjectName & " � ������� " & $sRegionName)

    If $sRegionAdded = "False" Then

        _FileWriteLog($sLogFile, "������ " &  $sRegionName & " �� ��������")
		ActionAddRegionByScada($sRegionName)

    EndIf

    Local $iCountOfAddedRegionObjects =  IniRead ($sRegionsConfig, $sRegionName, "countOfAddedRegionObjects", "$iCountOfAddedRegionObjects")

    If $iCountOfAddedRegionObjects = 0 Then

    ElseIf $iCountOfAddedRegionObjects > 0 Then
        For $i = 1 To $iCountOfAddedRegionObjects
            Local $sAddedObject = IniRead ( $sRegionsConfig, $sRegionName, "addedObject" & $i, "$sAddedObject")
            _FileWriteLog($sDebugLogFile,  "sAddedObject" & $i & " = " & $sAddedObject )
            If $sObjectName = $sAddedObject Then ;��������, �� �������� �� ��� � ������� ����������� ������
                $bObjectAlreadyAdded = True
                ExitLoop
            EndIf
        Next
    Else
        FncTestEnding(False, "�������� �������� �� ���������� �������� ��������� $iCountOfAddedRegionObjects: " & $iCountOfAddedRegionObjects)
    EndIf
        
    If $bObjectAlreadyAdded = True Then
		_FileWriteLog($sLogFile, $sFuncName & "������ " & $sObjectName & " ����� ��� ��� �������� � ������� " & $sRegionName)
	Else
		$iCountOfAddedRegionObjects = $iCountOfAddedRegionObjects + 1
		InnerDoAddObjectToRegion($sRegionName, $sObjectName, $iCountOfAddedRegionObjects)
        _FileWriteLog($sLogFile, "������ " & $sObjectName & " ������� �������� � ������� " & $sRegionName)
	EndIf
    
        
    
EndFunc

Func InnerDoAddObjectToRegion($sRegionName, $sObjectName, $iCountOfAddedRegionObjects)

    ActionFindRegionInScada($sRegionName)

    $sAction = "���� �� ������ �������� ������"
	FncMouseClick("left",50,95,1)

    Local $iTotalCountOfAddedRegionsObjects = IniRead($sAddedObjectsConfig, "AddedObjects", "totalCountOfAddedRegionsObjects", "0")
    $iTotalCountOfAddedRegionsObjects += 1

    IniWrite($sRegionsConfig, $sRegionName, "countOfAddedRegionObjects", $iCountOfAddedRegionObjects)
    IniWrite($sRegionsConfig, $sRegionName, "addedObject" & $iCountOfAddedRegionObjects, $sObjectName)
    IniWrite($sRegionsObjectsConfig, $sObjectName, "isAdded", "True")
    IniWrite($sRegionsObjectsConfig, $sObjectName, "addedToRegion", $sRegionName)
    IniWrite($sAddedObjectsConfig, "AddedObjects", "totalCountOfAddedRegionsObjects", $iTotalCountOfAddedRegionsObjects)
    ;����������� ������ ������ ����� ����� ��� "����������"

    ActionFindRegionObjectInScada($sRegionName, $sObjectName, True) ;����� ���������� ������� � ��� ��������������

    $sAction = "������ " & $sObjectName & " �������� � ������� " & $sRegionName
	_FileWriteLog($sLogFile, $sAction)
	FncScreenCapture($sKeyScreenPath, $sAction)

EndFunc