Func ActionRemoveRegion($sRegionName, $sRemoveType = "Scada")

    _FileWriteLog($sLogFile, "---------------------------------------")
	_FileWriteLog($sLogFile, "������ �������� �������� ������� " & $sRegionName )
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
	_FileWriteLog($sDebugLogFile, "������ �������� �������� ������� " & $sRegionName )

    If $sRemoveType = "ScadaScript" Then
        $bSuccessOperation = ActionRemoveRegionByScadaScripts($sRegionName)
    ElseIf $sRemoveType = "Scada" Then
        $bSuccessOperation = ActionRemoveRegionByScada($sRegionName)
    Else
        _FileWriteLog($sLogFile, "�������� ������������ �������� ��������� $sRemoveType: " & $sRemoveType)
        $bSuccessOperation = False
    EndIf

    If $bSuccessOperation = True Then
       $bSuccessOperation = ActionRemoveAllViewPanels()
    Else
        return $bSuccessOperation
    EndIf

    return $bSuccessOperation
EndFunc

