Func ActionRemoveRegionByScada($sRegionName)

    Local $bSuccessOperation = False
    Local $sRegionAdded = IniRead ($sRegionsConfig, $sRegionName, "isAdded", "$sRegionAdded")
    Local $sCountOfAddedRegions = IniRead ($sAddedObjectsConfig, "AddedObjects", "countOfAddedRegions", "0")

    If $sVersion = "2.8" Then
            _FileWriteLog($sLogFile, "�������� � ������� ������� ����� �� �������������� � KSVD v2.8. ������������ �� ����� �������� ������� ����� ����� �������" )
            $bSuccessOperation = ActionRemoveRegionByScadaScripts($sRegionName)
    ElseIf $sVersion = "2.10" Then

        If $sRegionAdded = "True" Then

            FncCheckAndActivateWindow($sArmAdminWindowName)
            ActionFindRegionInScada($sRegionName)
            
            for $i = 0 to 10
                Local $sObjectName = IniRead($sRegionsConfig, $sRegionName, "addedObject" & $i, "")
                Local $sObjectisAdded = IniRead($sRegionsObjectsConfig, $sObjectName, "isAdded" , "")
                if $sObjectisAdded = "True" then
                    InnerRemoveRegionObject($sRegionName, $sObjectName, $i)
                endif
            next

            FncClickTo("1stRegion", 1, "left")

            $sAction = "�������� �������"
            FncMouseClick("left",50,140,1)

            $sAction = "������������� �������� �������� ENTER"
            FncSendData("{ENTER}")

            _FileWriteLog($sDebugLogFile, "$sCountOfAddedRegions " & $sCountOfAddedRegions )
            $sCountOfAddedRegions -= 1
            _FileWriteLog($sDebugLogFile, "$sCountOfAddedRegions " & $sCountOfAddedRegions )

            IniWrite($sRegionsConfig, $sRegionName, "isAdded", "False")
            IniWrite($sAddedObjectsConfig, "AddedObjects", "countOfAddedRegions", $sCountOfAddedRegions)

            $bSuccessOperation = True
            _FileWriteLog($sLogFile, "�������� ������� " & $sRegionName & " ������� ���������" )

        ElseIf $sRegionAdded = "False" Then
            _FileWriteLog($sLogFile, "������ " & $sRegionName & " �� �������� � KSVD, �������� �������� ����������")
            $bSuccessOperation = True
        Else
            _FileWriteLog($sLogFile, "�������� ������������ �������� ��������� $sRegionAdded: " & $sRegionAdded)
            $bSuccessOperation = False
        EndIf
    EndIf

    return $bSuccessOperation

EndFunc

Func InnerRemoveRegionObject($sRegionName, $sObjectName, $iObjectNumber)

    _FileWriteLog($sLogFile, "�������� ������� " & $sObjectName & " ������� " & $sRegionName )

    ActionFindRegionObjectInScada($sRegionName, $sObjectName, False)

    $sAction = "�������� ������� �������"
	FncMouseClick("left",50,265,1)

    $sAction = "������������� �������� �������� ENTER"
    FncSendData("{ENTER}")

    Local $iCountOfAddedRegionObjects = IniRead($sRegionsConfig, $sRegionName, "countOfAddedRegionObjects", "0")
    Local $iTotalCountOfAddedRegionsObjects = IniRead($sAddedObjectsConfig, "AddedObjects", "totalCountOfAddedRegionsObjects", "0")
    Local $iCountOfAddedToRegionsObjectCameras = IniRead($sRegionsObjectsConfig, $sObjectName, "countOfAddedToRegionsObjectCameras", 0)

    _FileWriteLog($sLogFile, "$sRegionName " & $sRegionName )
    _FileWriteLog($sLogFile, "$sObjectName " & $sObjectName )

    _FileWriteLog($sDebugLogFile, "$iCountOfAddedRegionObjects " & $iCountOfAddedRegionObjects )
    $iCountOfAddedRegionObjects -=1
    _FileWriteLog($sDebugLogFile, "$iCountOfAddedRegionObjects " & $iCountOfAddedRegionObjects )

    _FileWriteLog($sDebugLogFile, "$iTotalCountOfAddedRegionsObjects " & $iTotalCountOfAddedRegionsObjects )
    $iTotalCountOfAddedRegionsObjects -= 1
    _FileWriteLog($sDebugLogFile, "$iTotalCountOfAddedRegionsObjects " & $iTotalCountOfAddedRegionsObjects )

    IniWrite($sAddedObjectsConfig, "AddedObjects", "totalCountOfAddedRegionsObjects", $iTotalCountOfAddedRegionsObjects)
    IniWrite($sRegionsConfig, $sRegionName, "countOfAddedRegionObjects", $iCountOfAddedRegionObjects)
    IniWrite($sRegionsConfig, $sRegionName, "addedObject" & $iObjectNumber, "")
    IniWrite($sRegionsObjectsConfig, $sObjectName, "isAdded", "False")
    IniWrite($sRegionsObjectsConfig, $sObjectName, "addedToRegion", "")
    IniWrite($sRegionsObjectsConfig, $sObjectName, "countOfAddedToRegionsObjectCameras", 0)
    IniWrite($sRegionsObjectsConfig, $sObjectName , "countOfAddedToRegionsObjectViewPanels", 0)
    For $i = 1 To $iCountOfAddedToRegionsObjectCameras Step +1
        IniWrite($sRegionsObjectsConfig, $sObjectName, "addedCamera" & $i, "")
    Next
    

    _FileWriteLog($sLogFile, "�������� ������� " & $sObjectName & " ������� " & $sRegionName & " ������� ���������")

EndFunc