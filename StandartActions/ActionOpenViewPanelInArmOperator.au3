Func ActionOpenViewPanelInArmOperator($sViewPanel, $iMonitor)

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� �������� ��������� " & $sViewPanel & " � ��� ���������")
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "������ �������� �������� ��������� " & $sViewPanel & " � ��� ���������")

    If $sVersion = "2.8" Then

        $sAction = "�������������� �������"
        FncMouseClick("left",1660,127,1)

        $sAction = "�������������� ������� �������"
        FncMouseClick("left",1685,145,1)

        $sAction = "�������� ��������� ������� ������"
        FncMouseClick("left",1750,185,2)

        Sleep(5000)
        FncSavePrintScreen("����������� ����� ��������� " & $sViewPanel & " � ��� ��������� �� �������� " & $iMonitor)

        If $iMonitor = 1 Then
            _FileWriteLog($sLogFile, "�������� ��������� � ������� ALT+F4")
            $sAction = "ALTDOWN"
            FncSendData("{ALTDOWN}")
            $sAction = "������� ������ F4"
            FncSendData("{F4}")
            $sAction = "ALTUP"
            FncSendData("{ALTUP}")
        EndIf
        
    ElseIf $sVersion = "2.10" Then

        ActionFindViewPanelInArmOperator($sViewPanel)

        If $iMonitor = 1 Then
            ActionHideShowArmOperatorWindow("hide")
        EndIf

        Sleep(5000)
        FncSavePrintScreen("����������� ����� ��������� " & $sViewPanel & " � ��� ��������� �� �������� " & $iMonitor)

        If $iMonitor = 1 Then
            ActionHideShowArmOperatorWindow("show")
        EndIf
        
    EndIf

EndFunc