Func ActionRemoveCameraInGUI($sCamera)

    Local $bSuccessOperation = False
    Local $sFuncname = "ActionRemoveCameraInGUI. "
    Local $sIsAdded = IniRead( $sCamerasConfig, $sCamera, "isAdded", "$sIsAdded")
    Local $sIsAddedToArchive = IniRead( $sCamerasConfig, $sCamera, "isAddedToArchive", "$sIsAddedToArchive")
    Local $sIsAddedToRtspEchd = IniRead( $sCamerasConfig, $sCamera, "isAddedToRtspEchd", "$sIsAddedToRtspEchd")
    Local $iCountOfAddedCameras = IniRead( $sAddedObjectsConfig, "AddedObjects", "countOfAddedCameras", "$iCountOfAddedCameras")
    
    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� �������� ������ " & $sCamera)

    FncCheckAndActivateWindow($sArmAdminWindowName)

    If $sIsAdded = "True" Then

        ActionFindCameraInGUI($sCamera)

        If $sIsAddedToRtspEchd = "True" Then
            $sAction = "���������� ������ �� �������������"
            FncMouseClick("left",50,100,1)
            IniWrite( $sCamerasConfig, $sCamera, "isAddedToRtspEchd", "False")
            ActionFindCameraInGUI($sCamera)
        ElseIf $sIsAddedToRtspEchd = "False" Then
            _FileWriteLog($sDebugLogFile, $sFuncname & "���������� ������ " & $sCamera & " �� ������������� �� ���������")
        Else
            _FileWriteLog($sLogFile, $sFuncname & "�������� ������������ �������� ��������� $sIsAddedToRtspEchd: " & $sIsAddedToRtspEchd )
            $bSuccessOperation = False
            return $bSuccessOperation
        EndIf

        $sAction = "���������� ������ �� �������"
        FncMouseClick("left",50,100,1)

        If $sIsAddedToArchive = "True" Then
            $sAction = "������������� ���������� ������ �������� ENTER"
            FncSendData("{ENTER}")
            IniWrite( $sCamerasConfig, $sCamera, "isAddedToArchive", "False")
        ElseIf $sIsAddedToArchive = "False" Then
            _FileWriteLog($sDebugLogFile, $sFuncname & "������ " & $sCamera & " �� ��������� � ���������")
        Else
            _FileWriteLog($sLogFile, $sFuncname & "�������� ������������ �������� ��������� $sIsAddedToArchive: " & $sIsAddedToArchive )
            $bSuccessOperation = False
            return $bSuccessOperation
        EndIf

        IniWrite( $sCamerasConfig, $sCamera, "isActive", "False")

        ActionFindCameraInGUI($sCamera)

        $sAction = "���� �� ������ �������� ������"
        FncMouseClick("left",50,100,1)

        $sAction = "������������� �������� ������ �������� ENTER"
        FncSendData("{ENTER}")

        $iCountOfAddedCameras -= 1
        IniWrite( $sAddedObjectsConfig, "AddedObjects", "countOfAddedCameras", $iCountOfAddedCameras)
        IniWrite( $sCamerasConfig, $sCamera, "isAdded", "False")
        IniWrite( $sCamerasConfig, $sCamera, "addedToRegionObject", "" )
        
        _FileWriteLog($sLogFile, "������ " & $sCamera & " ������� �������")
        $bSuccessOperation = True

    ElseIf $sIsAdded = "False" Then
        _FileWriteLog($sLogFile, $sFuncname & "�������� �������� ����������. ������ " & $sCamera & " �� ��������� �� ������")
        $bSuccessOperation = True
    Else
        _FileWriteLog($sLogFile, $sFuncname & "�������� ������������ �������� ��������� $sIsAdded: " & $sIsAdded )
        $bSuccessOperation = False
    EndIf

    return $bSuccessOperation

EndFunc
