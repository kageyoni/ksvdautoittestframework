Func ActionRemoveUserInGui($sUser)

    Local $bSuccessOperation = False
    Local $sGuiName = IniRead ( $sUsersConfig, $sUser, "guiName", "$sGuiName" )
    Local $sIsAdded = IniRead ( $sUsersConfig, $sUser, "isAdded", "$sIsAdded" )

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "����c� �������� �������� ������������ " & $sUser )

    FncCheckAndActivateWindow($sArmAdminWindowName)

    If $sIsAdded = "False" Then
		_FileWriteLog($sLogFile, "�������� ����������, ��������� ������������ " & $sUser & " �� �������� �� ������")
        $bSuccessOperation = True
    ElseIf $sIsAdded = "True" Then
    
        ActionFindUserInGui($sUser)

        $sAction = "���� �� ������ �������� ������������"
        FncMouseClick("left",50,50,1)

        $sAction = "������������� �������� ������������ �������� ENTER"
        FncSendData("{ENTER}")

        ActionClearFilterFieldInArmAdmin()

        IniWrite( $sUsersConfig, $sUser, "isAdded", "False" )

        _FileWriteLog($sLogFile, "�������� ������������ " & $sUser & " ������� ���������")
        $bSuccessOperation = True

    Else
        _FileWriteLog($sLogFile, "�������� ������������ �������� ��������� $sIsAdded: " & $sIsAdded )
        $sSuccessOperation = False
    EndIf
    return $bSuccessOperation
EndFunc