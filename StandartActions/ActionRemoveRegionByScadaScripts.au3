Func ActionRemoveRegionByScadaScripts($sRegionName)

    _FileWriteLog($sLogFile, "---------------------------------------")
	_FileWriteLog($sLogFile, "������ �������� �������� ������� " & $sRegionName & " ����� �����-�������" )
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
	_FileWriteLog($sDebugLogFile, "������ �������� �������� ������� " & $sRegionName & " ����� �����-�������" )

    Local $bSuccessOperation = False
    Local $sRegionAdded = IniRead ($sRegionsConfig, $sRegionName, "isAdded", "$sRegionAdded")
    Local $sCountOfAddedRegions = IniRead ($sAddedObjectsConfig, "AddedObjects", "countOfAddedRegions", "0")
    

    If $sRegionAdded = "True" Then
    ;���� �� ����� ������� ���-�� ����������� ��������, �.�. �������������� ������������� ������ ������ �������, ������� � ���������

        FncClickTo("ScadaScriptsTab", 1, "left")

        $sAction = "��������� �������"
        If $sVersion = "2.8" Then
            FncMouseClick("left",282,543,1)
        ElseIf $sVersion = "2.10" Then
            FncMouseClick("left",282,455,1)
        EndIf

        $sAction = "�������� �������"
        If $sVersion = "2.8" Then
            FncMouseClick("left",1895,570,1)
        ElseIf $sVersion = "2.10" Then
            FncMouseClick("left",1895,490,1)
        EndIf
        
        $sAction = "��������� �������"
        FncMouseClick("left",282,95,1)

        $sAction = "�������� �������"
        FncMouseClick("left",1895,127,1)

        $sAction = "���� �� ������ �������� �������� � �������� ��� ���������� �������� � ��"
        FncMouseClick("left",100,145,1)

        $sAction = "������������� �������� ��������� �������� ENTER"
        FncSendData("{ENTER}")

        ;---- ���������� � ������� ----
        for $i = 1 to 10 
            Local $sObjectName = IniRead($sRegionsConfig, $sRegionName, "addedObject" & $i, "")
            Local $sObjectisAdded = IniRead($sRegionsObjectsConfig, $sObjectName, "isAdded" , "")
            Local $iCountOfAddedToRegionsObjectCameras = IniRead($sRegionsObjectsConfig, $sObjectName, "countOfAddedToRegionsObjectCameras", 0)
            if $sObjectisAdded = "True" then
                IniWrite($sRegionsConfig, $sRegionName, "addedObject" & $i, "")
                IniWrite($sRegionsObjectsConfig, $sObjectName, "isAdded", "False")
                IniWrite($sRegionsObjectsConfig, $sObjectName, "addedToRegion", "")
                IniWrite($sRegionsObjectsConfig, $sObjectName , "countOfAddedToRegionsObjectViewPanels", 0)
                For $i = 1 To $iCountOfAddedToRegionsObjectCameras Step +1
                    IniWrite($sRegionsObjectsConfig, $sObjectName, "addedCamera" & $i, "")
                Next
            endif
        next
            
        IniWrite($sRegionsConfig, $sRegionName, "isAdded", "False")
        IniWrite($sRegionsConfig, $sRegionName, "countOfAddedRegionObjects", 0)
        IniWrite($sAddedObjectsConfig, "AddedObjects", "countOfAddedRegions", 0)
        IniWrite($sAddedObjectsConfig, "AddedObjects", "totalCountOfAddedRegionsObjects", 0)
        IniWrite($sRegionsObjectsConfig, $sObjectName, "countOfAddedToRegionsObjectCameras", 0)

        
        
        $bSuccessOperation = True
        _FileWriteLog($sLogFile, "�������� ������� " & $sRegionName & " ������� ���������" )

    ElseIf $sRegionAdded = "False" Then
        _FileWriteLog($sLogFile, "������ " & $sRegionName & " �� �������� � KSVD, �������� �������� ����������")
        $bSuccessOperation = True
    Else
        _FileWriteLog($sLogFile, "�������� ������������ �������� ��������� $sRegionAdded: " & $sRegionAdded)
        $bSuccessOperation = False
    EndIf

    return $bSuccessOperation
EndFunc