Func ActionChangeArmSettings($aParameters)

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� ��������� �������� ��� KSVD")
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "������ �������� ��������� �������� ��� KSVD")

    ;---- ������������� ���������� ----
    If $sVersion = "2.8" Then
        Local $sChangeDbOrUserSettings = $aParameters[0] ;"DB" or "LocalUser"
        Local $sStyle = $aParameters[1] ; "2.8+" or "2.7"
        Local $sScale = $aParameters[2] ; "KeepAspectRatio" or "Fill"
    ElseIf $sVersion = "2.10" Then
        Local $sChangeDbOrUserSettings = $aParameters[0] ;"DB" or "LocalUser"
        Local $sStyle = $aParameters[1] ; "Modern" or "2.7"
        Local $sScale = $aParameters[2] ; "KeepAspectRatio" or "Fill"
        Local $bUsingSmooth = $aParameters[3] ; False or True
        Local $sPlacingControls = $aParameters[4] ; "UP" or "DOWN"
        Local $sShowCameraName = $aParameters[5] ; False or True
        Local $sShowFPS = $aParameters[6] ; False or True
    EndIf

    If $sVersion = "2.8" Then

        $sAction = "���� �� �������� ������ �� ��� ������������"
        FncMouseClick("left",70,40,1)

        If $sChangeDbOrUserSettings = "DB" Then
            $sAction = "������� ������ DOWN"
            FncSendData("{DOWN}")
            $sAction = "����� �������� ���� ������ �������� ������ UP"
            FncSendData("{UP}")
            $sAction = "������� ������ ENTER"
            FncSendData("{ENTER}")
        ElseIf $sChangeDbOrUserSettings = "LocalUser" Then
            $sAction = "����� �������� ��������� ������������ �������� ������ DOWN"
            FncSendData("{DOWN}")
            $sAction = "������� ������ ENTER"
            FncSendData("{ENTER}")
        EndIf

        If $sStyle = "2.8+" Then
            $sAction = "���� �� ��������� 2.8"
            FncMouseClick("left",125,320,1)
        ElseIf $sStyle = "2.7" Then
            $sAction = "���� �� ��������� ������ 2.7"
            FncMouseClick("left",215,320,1)
        EndIf

        If $sScale = "KeepAspectRatio" then
            $sAction = "���� �� ��������� ��������� ���������"
            FncMouseClick("left",125,358,1)
        ElseIf $sScale = "Fill" then
            $sAction = "���� �� ��������� ������ ����������"
            FncMouseClick("left",360,358,1)
        EndIf

        $sAction = "���� �� ������ ���������"
        FncMouseClick("left",660,410,1)

        $sAction = "�������� ��������� KSVD"
        _FileWriteLog($sLogFile, $sAction)
        FncScreenCapture($sKeyScreenPath, $sAction)

        FncCloseWindow("��� ��������� KSVD")

    ElseIf $sVersion = "2.10" Then

        $sAction = "���� �� �������� ������ �� ��� ������������"
        FncMouseClick("left",200,55,1)

        If $sChangeDbOrUserSettings = "DB" Then
            $sAction = "����� �������� ���� ������ �������� ������ UP"
            FncSendData("{UP}")
            $sAction = "������� ������ ENTER"
            FncSendData("{ENTER}")
        ElseIf $sChangeDbOrUserSettings = "LocalUser" Then
            $sAction = "����� �������� ��������� ������������ �������� ������ DOWN"
            FncSendData("{DOWN}")
            $sAction = "������� ������ ENTER"
            FncSendData("{ENTER}")
        EndIf

        If $sStyle = "Modern" Then
            $sAction = "���� �� ��������� �����������"
            FncMouseClick("left",205,227,1)
        ElseIf $sStyle = "2.7" Then
            $sAction = "���� �� ��������� ������ 2.7"
            FncMouseClick("left",315,227,1)
        EndIf

        If $sScale = "KeepAspectRatio" then
            $sAction = "���� �� ��������� ��������� ���������"
            FncMouseClick("left",205,267,1)
        ElseIf $sScale = "Fill" then
            $sAction = "���� �� ��������� ������ ����������"
            FncMouseClick("left",415,267,1)
        EndIf

        If $bUsingSmooth = True Then
            $sAction = "���� �� ��������� ������������ �����������"
            FncMouseClick("left",345,306,1)
        ElseIf $bUsingSmooth = False Then
            $sAction = "���� �� ��������� �� ������������ �����������"
            FncMouseClick("left",205,306,1)
        EndIf

        If $sPlacingControls = "UP" Then
            $sAction = "���� �� ��������� ������������ ������"
            FncMouseClick("left",206,331,1)
        ElseIf $sPlacingControls = "DOWN" Then
            $sAction = "���� �� ��������� ������������ �����"
            FncMouseClick("left",272,332,1)
        EndIf

        If $sShowCameraName = True Then
            $sAction = "���� �� ��������� ���������� �������� ������"
            FncMouseClick("left",205,358,1)
        ElseIf $sShowCameraName = False Then
            $sAction = "���� �� ��������� �������� �������� ������"
            FncMouseClick("left",300,358,1)
        EndIf

        If $sShowFPS = True Then
            $sAction = "���� �� ��������� ���������� ��������� ������"
            FncMouseClick("left",205,384,1)
        ElseIf $sShowFPS = False Then
            $sAction = "���� �� ��������� �������� ��������� ������"
            FncMouseClick("left",300,384,1)
        EndIf

        $sAction = "���� �� ������ ���������"
        FncMouseClick("left",500,420,1)

        $sAction = "�������� ��������� KSVD"
        _FileWriteLog($sLogFile, $sAction)
        FncScreenCapture($sKeyScreenPath, $sAction)

        WinWait("Run", "", 5)

    EndIf  

EndFunc