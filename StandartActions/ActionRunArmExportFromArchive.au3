Func ActionRunArmExportFromArchive($sUserName, $sPassword, $bFirstRun)

   Local $sInstallPath = IniRead ($sCommonConfig, "InstallInformation", "installPath", "$sInstallPath")
   Local $sFileName = IniRead ($sKsvdAppsParams, "KsvdAppsParams", "armExportFromArchiveExe", "$sFileName")
   Local $sWindowName = IniRead ($sKsvdAppsParams, "KsvdAppsParams", "armExportFromArchiveName", "$sWindowName")
   Local $sAppPath = $sInstallPath & "\" & $sFileName

   FncRunApp($sAppPath) ;������ ����������

   If $sVersion = "2.8" Then
      FncCheckAndActivateWindow("�������������� � ��� �������� ������� KSVD")
   ElseIf $sVersion = "2.10" Then
      FncCheckAndActivateWindow($sWindowName) ;�������� ������������� ����, ��� ���������, � ��� �� ���������� �� ��������, ���� ���� ��� � �� ���������
   EndIf

   $sAction = "���� �� ���� ����� ����� ������������"
   If $sVersion = "2.8" Then
      FncMouseClick("left",230,215,1)
      FncMouseClick("right",230,215,1)
   ElseIf $sVersion = "2.10" Then
      FncMouseClick("right",230,390,1)
   EndIf

   $sAction = "������� ������� UP"
   FncSendData("{UP}")
   $sAction = "������� ������� ENTER"
   FncSendData("{ENTER}")
   $sAction = "�������� ����������� ���� ������� DELETE"
   FncSendData("{DEL}")

   $sAction = "���� ����� ������������"
   FncSendData($sUserName)
   $sAction = "������� � ���� ����� ������ � ������� ������� TAB" ;����� ���� ���������� �� ���. ���� ��������������
   FncSendData("{TAB}")
   $sAction = "���� ������"
   FncSendData($sPassword)
   $sAction = "������� ������ ENTER"
   FncSendData("{ENTER}")
   Sleep(2000)

   If $sVersion = "2.8" Then
      FncCheckAndActivateWindow($sWindowName)
   EndIf

   If $bFirstRun = True Then
      _FileWriteLog($sLogFile, "������������� ���� ���������� �� ���� �����")
      $sAction = "���� �� ��������� ����"
      FncMouseClick("right",60,-15,1)
      $sAction = "������� ������ DOWN"
      FncSendData("{DOWN}")
      FncSendData("{DOWN}")
      FncSendData("{DOWN}")
      FncSendData("{DOWN}")
      FncSendData("{DOWN}")
      $sAction = "������� ������ ENTER"
      FncSendData("{ENTER}")
      Sleep(2000)
   EndIf

EndFunc