Func ActionAddCameraToArchive($sArchive, $sCamera)

	Local $iCountOfAddedCamerasToArchive = IniRead ( $sArchivesConfig, $sArchive, "countOfAddedCamerasToArchive", "$iCountOfAddedCamerasToArchive")
   	;Local $sCameraGuiName = IniRead ( $sCamerasConfig, $sCamera, "cameraGuiName", "$sCameraGuiName")

   	Local $sFuncName = "ActionAddArchiveToCamera. "

	_FileWriteLog($sLogFile, "---------------------------------------")
	_FileWriteLog($sLogFile, "������ �������� ���������� ��������� " &$sArchive& " � ������ " & $sCamera)
	_FileWriteLog($sDebugLogFile, "---------------------------------------")
	_FileWriteLog($sDebugLogFile, "������ �������� ���������� ��������� " &$sArchive& " � ������ " & $sCamera)

	If $iCountOfAddedCamerasToArchive = 0 Then
		$iCountOfAddedCamerasToArchive = 1
		InnerDoAddCameraToArchive($sArchive, $iCountOfAddedCamerasToArchive, $sCamera)

   ElseIf $iCountOfAddedCamerasToArchive > 0 Then
    	_FileWriteLog($sDebugLogFile,  "iCountOfAddedCamerasToArchive = " & $iCountOfAddedCamerasToArchive )
		Local $bCameraAlreadyAdded = False
		For $i = 1 To $iCountOfAddedCamerasToArchive
			Local $sAddedCamera = IniRead ( $sArchivesConfig, $sArchive, "addedCamera" & $i, "$sArchiveGuiName")
			_FileWriteLog($sDebugLogFile,  "sAddedCamera = " & $sAddedCamera )
			If $sCamera = $sAddedCamera Then
			_FileWriteLog($sDebugLogFile,  "sCamera = " & $sCamera )
				$bCameraAlreadyAdded = True
				ExitLoop
			EndIf
		Next

	  If $bCameraAlreadyAdded = True Then
		 _FileWriteLog($sLogFile,  "������ " & $sCamera & " ����� ��� ���� ��������� � ��������� " & $sArchive)
	  Else
		 $iCountOfAddedCamerasToArchive = $iCountOfAddedCamerasToArchive + 1
		 InnerDoAddCameraToArchive($sArchive, $iCountOfAddedCamerasToArchive, $sCamera)
	  EndIf
   EndIf

EndFunc


Func InnerFncCheckArchiveFileExists($sArchive)

	Sleep(5000)
	_FileWriteLog($sLogFile, "�������� ������� ����� ���������� ��������� �� �����")

	Local $sArchiveFilePath = IniRead ( $sArchivesConfig, $sArchive, "filePath", "$sArchiveFilePath")

	If FileExists($sArchiveFilePath) Then
		_FileWriteLog($sLogFile, "���� ��������� ������� ������")
	Else
		_FileWriteLog($sLogFile, "������ �������� ���������. �� ������� ����� ���� ���������")
		_FileWriteLog($sLogFile, "������������� ����: " & $sArchiveFilePath)
		FncTestEnding(False, "������ ���������� ��������� " & $sArchive)
	EndIf
EndFunc

Func InnerFncCheckArchiveLogFileEsists ($sArchive) ; ---- �������� �� �������� ����� ���� ������ �� ID

	Local $sFuncName = "ActionAddArchiveToCamera. InnerFncCheckArchiveLogFileEsists. "

	Local $sLogPath = IniRead ( $sArchivesConfig, $sArchive, "logPath", "$sLogPath")

	_FileWriteLog($sDebugLogFile, $sFuncName & "��������� ���� ����: " & $sLogPath)
	Sleep(1000)

	If FileExists($sLogPath) Then
		_FileWriteLog($sLogFile, "��� ���� ��������� ������������")
	Else
		_FileWriteLog($sLogFile, "����������� ��������� ��� ���� �� ������������ ��������� � ���������� ���������")
		_FileWriteLog($sLogFile, "������������� ����: " & $sLogPath)
		FncTestEnding(False, "������ ���������� ��������� " & $sArchive)
	EndIf
EndFunc

Func InnerDoAddCameraToArchive($sArchive, $iCountOfAddedCamerasToArchive, $sCamera)

	Local $iCountOfAddedCameras = IniRead ( $sAddedObjectsConfig, "AddedObjects", "countOfAddedCameras", "$sAddedToServer")
	Local $sArchiveGuiName = IniRead ( $sArchivesConfig, $sArchive, "guiName", "$sArchiveGuiName")

	ActionFindArchiveInGui ($sArchive)

	If $iCountOfAddedCameras > 1 Then
		$sAction = "���� �� ������ ���������� � ������"
		FncMouseClick("left",115,145,1)
		Local $iCameraOrderNumber = IniRead ( $sCamerasConfig, $sCamera, "orderNumber", "$iCameraOrderNumber")
		$sAction = "������� ������ DOWN"
		FncSendData("{DOWN}") ;������� �� ������ �������, �.�. ������ ��������� ����� ������ ������. ���� ������� ���������� ����� �������� �����, ����� ����������� ���������� � ������ ����� ��� �����
		For $i = 1 To $iCameraOrderNumber
		Sleep(1000)
		$sAction = "������� ������ DOWN"
		FncSendData("{DOWN}")
		Next
		$sAction = "������� ������ ENTER"
		FncSendData("{ENTER}")
		$sAction = "���� �� ������ ������������"
		FncMouseClick("left",115,235,1)
	ElseIf $iCountOfAddedCameras = 1 Then
		$sAction = "���� �� ������ ���������� � ������"
		FncMouseClick("left",115,145,1)
		Sleep(1000)
		$sAction = "������� ������ ENTER"
		FncSendData("{ENTER}")
		$sAction = "���� �� ������ ������������"
		FncMouseClick("left",115,190,1)
	ElseIf $iCountOfAddedCameras < 1 Then
		_FileWriteLog($sLogFile, "��� �� ����� ����������� ������")
		FncTestEnding(False, "������ ���������� ��������� � ������. ��� �� ����� ����������� ������")
	EndIf

	InnerFncCheckArchiveLogFileEsists ($sArchive)
	InnerFncCheckArchiveFileExists($sArchive)

	$sAction = "������ " & $sCamera & " ��������� � ��������� " & $sArchive
	_FileWriteLog($sLogFile, $sAction)
	FncScreenCapture($sKeyScreenPath, $sAction)

	Local $sCurrentDateTime = _NowCalc() ;���������� �������� ������� � ������ � ������
	IniWrite ( $sArchivesConfig, $sArchive, "addedCamera" & $iCountOfAddedCamerasToArchive, $sCamera)
	IniWrite ( $sArchivesConfig, $sArchive, "countOfAddedCamerasToArchive" ,$iCountOfAddedCamerasToArchive)
	IniWrite ( $sArchivesConfig, $sArchive, "archiveRecordingStartTime", $sCurrentDateTime)
	IniWrite ( $sCamerasConfig, $sCamera, "addedToArchive", $sArchiveGuiName)
	IniWrite ( $sCamerasConfig, $sCamera, "isAddedToArchive", "True")
	IniWrite ( $sCamerasConfig, $sCamera, "archiveRecordingStartTime", $sCurrentDateTime)

EndFunc