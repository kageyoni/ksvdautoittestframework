Func ActionAddObjectToRegionByScadaScripts($sRegionName, $sObjectName)

    Local $sRegionAdded = IniRead ($sRegionsConfig, $sRegionName, "isAdded", "False")
    Local $sFuncName = "ActionAddObjectToRegionByScadaScripts. "
    Local $bObjectAlreadyAdded = False

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� ���������� ������� " & $sObjectName & " � ������� " & $sRegionName & " ����� ����� �������")
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "������ �������� ���������� ������� " & $sObjectName & " � ������� " & $sRegionName & " ����� ����� �������")

    If $sRegionAdded = "False" Then

        _FileWriteLog($sLogFile, "������ " &  $sRegionName & " �� ��������. ������ �������� ����������")
		ActionAddRegionByScadaScripts($sRegionName)

    EndIf

    Local $iCountOfAddedRegionObjects =  IniRead ($sRegionsConfig, $sRegionName, "countOfAddedRegionObjects", "0")

    If $iCountOfAddedRegionObjects > 0 Then
        For $i = 1 To $iCountOfAddedRegionObjects
            Local $sAddedObject = IniRead ( $sRegionsConfig, $sRegionName, "addedObject" & $i, "$sAddedObject")
            _FileWriteLog($sDebugLogFile,  "sAddedObject" & $i & " = " & $sAddedObject )
            If $sObjectName = $sAddedObject Then ;��������, �� �������� �� ��� � ������� ����������� ������
                $bObjectAlreadyAdded = True
                ExitLoop
            EndIf
        Next
    ElseIf $iCountOfAddedRegionObjects = 0 Then
        _FileWriteLog($sDebugLogFile, "$iCountOfAddedRegionObjects = " & $iCountOfAddedRegionObjects)
    Else
        FncTestEnding(False, "�������� �������� �� ���������� �������� ��������� $iCountOfAddedRegionObjects: " & $iCountOfAddedRegionObjects)
    EndIf
        
    If $bObjectAlreadyAdded = True Then
		_FileWriteLog($sLogFile,  "������ " & $sObjectName & " ����� ��� ��� �������� � ������� " & $sRegionName)
	Else
		InnerDoAddObjectToRegionByScadaScripts($sRegionName, $sObjectName, $iCountOfAddedRegionObjects)
	EndIf
    
        
    
EndFunc

Func InnerDoAddObjectToRegionByScadaScripts($sRegionName, $sObjectName, $iCountOfAddedRegionObjects)

    Local $iCountOfAddedRegions = IniRead ($sAddedObjectsConfig, "AddedObjects", "countOfAddedRegions", "0")
    Local $iTotalCountOfAddedRegionsObjects = IniRead($sAddedObjectsConfig, "AddedObjects", "totalCountOfAddedRegionsObjects", "0")
    $iTotalCountOfAddedRegionsObjects += 1

    $sAction = "���� �� ������� ����� �������"
    If $sVersion = "2.8" Then
        FncMouseClick("left",490,15,1)
    ElseIf $sVersion = "2.10" Then
        FncMouseClick("left",580,15,1)
    EndIf
				
	$sAction = "���� �� ������ ��������� �� ��"
	FncMouseClick("left",50,50,1)

    $sAction = "������� ������ ENTER �� ������ ���� �������� ���� �������������"
	FncSendData("{ENTER}")

    $sAction = "���� �� ������ ������ ������� ��������"
	FncMouseClick("left",300,100,1)

    If $iCountOfAddedRegions > 1 Then
        $sAction = "������� �� ������� ���� �������� DOWN"
        for $i = 2 to $iCountOfAddedRegions
            FncSendData("{DOWN}")
        next
    EndIf

    Local $sClipboard = FncCopyToClipboard()
    If $sRegionName = $sClipboard Then
        _FileWriteLog($sLogFile, "������ ������ � GUI")
    Else
        _FileWriteLog($sLogFile, "�� ������� ����� ������ � GUI")
		FncTestEnding(False, "������ �� ������ � GUI")
    EndIf

    $sAction = "���� �� ������ ������ ������� ��������"
	FncMouseClick("left",300,555,1)

    if $iTotalCountOfAddedRegionsObjects > 1 then
        $sAction = "������� �� ������� ���� �������� DOWN"
        for $i = 1 to $iTotalCountOfAddedRegionsObjects
            FncSendData("{DOWN}")
        next
    EndIf

    $sAction = "���� ������ �������"
    FncSendData($iCountOfAddedRegions)

    $sAction = "������� � ���� �������� �������� TAB"
    FncSendData("{TAB}")

    $sAction = "���� �������� �������"
    FncSendData($sObjectName)

    $sAction = "���� �� ������ �������� �������� � ��������"
	FncMouseClick("left",50,145,1)

    $sAction = "������� ������ ENTER"
	FncSendData("{ENTER}")

    $sAction = "���� �� ������������ �����"
	FncMouseClick("left",290,800,1)

    $iCountOfAddedRegionObjects += 1
    IniWrite($sRegionsConfig, $sRegionName, "countOfAddedRegionObjects", $iCountOfAddedRegionObjects)
    IniWrite($sRegionsConfig, $sRegionName, "addedObject" & $iCountOfAddedRegionObjects, $sObjectName)
    IniWrite($sRegionsObjectsConfig, $sObjectName, "isAdded", "True")
    IniWrite($sRegionsObjectsConfig, $sObjectName, "addedToRegion", $sRegionName)
    IniWrite($sAddedObjectsConfig, "AddedObjects", "totalCountOfAddedRegionsObjects", $iTotalCountOfAddedRegionsObjects)

    If $sVersion = "2.8" Then
        ;��� ������� ����� ��� 2.8. ���� ��� �������� ��� ���. ��������
    ElseIf $sVersion = "2.10" Then
        ActionFindRegionObjectInScada($sRegionName, $sObjectName, False)
    EndIf

    $sAction = "������ " & $sObjectName & " �������� � ������� " & $sRegionName
	_FileWriteLog($sLogFile, $sAction)
	FncScreenCapture($sKeyScreenPath, $sAction)

EndFunc