Func ActionRunArmOperator ($sUser)

   Local $sFuncName = "ActionRunArmOperator. "
   $sArmOperatorLoginSuccess = False

   Local $sUserName = IniRead ( $sUsersConfig, $sUser, "username", "$sUserName" ) ;��� ������������
   Local $sPassword = IniRead ( $sUsersConfig, $sUser, "password", "$sPassword" ) ;������ ������������
   Local $sUserGuiName = IniRead ( $sUsersConfig, $sUser, "guiName", "$sUserGuiName" ) ;��� ������������
   Local $sArmOperator = IniRead ( $sKsvdAppsParams, "KsvdAppsParams", "armOperatorExe", "$sArmOperator") ;��� ����� ������� ��� ���������
   Local $sUserIsAdded = IniRead ( $sUsersConfig, $sUserName, "isAdded", "$sUserIsAdded" ) ;��� ������������
   Local $bFirstRun = IniRead ( $sKsvdAppsParams, "KsvdAppsParams", "armOperatorFirstRun", "$bFirstRun")
   Local $bArmOperatorWindowIsMaximized = IniRead ( $sKsvdAppsParams, "KsvdAppsParams", "armOperatorWindowIsMaximized", "$bFirstRun")

   _FileWriteLog($sLogFile, "---------------------------------------")
   _FileWriteLog($sLogFile, "������ ��� ���������")
   _FileWriteLog($sDebugLogFile, "---------------------------------------")
   _FileWriteLog($sDebugLogFile, "������ ��� ���������")

   FncRunApp ($sInstallPath & "\" & $sArmOperator)

   If $sVersion = "2.8"  Then
   _FileWriteLog($sDebugLogFile, "$bFirstRun = " & $bFirstRun)
      If $bFirstRun =  "True" Then
         ;---------------------------------------------------------------------------------
         ;----- ������ ������ ��� ���������. ���������� � ������ --------------------------
         ;---------------------------------------------------------------------------------
         ;----- ��� 1. �������� ��������� -------------------------------------------------
         ;---------------------------------------------------------------------------------
         _FileWriteLog($sLogFile, "������ ������ ��� ���������. ���������� � ������")
         _FileWriteLog($sLogFile, "��� 1. �������� ��������� ��� ���������")
         local $iKsvd4_armdPID = ProcessExists ( "ksvd4_armd.exe" )
         Local $iKsvd4_ctrlPID =  ProcessExists ( "ksvd4_ctrl.exe" )

         If $iKsvd4_armdPID <> 0 Then
            Local $iClose = ProcessClose ( $iKsvd4_armdPID )
               _FileWriteLog($sDebugLogFile, "PID �������� ksvd4_armd.exe: " & $iKsvd4_armdPID)
            If $iClose = 1 Then
               _FileWriteLog($sLogFile, "������� ksvd4_armd.exe PID: " & $iKsvd4_armdPID & " ������� ����������")
            Else
               FncTestEnding(False, "�� ������� ���������� ������� ������� ksvd4_armd.exe PID: " & $iKsvd4_armdPID)
            EndIf
         Else
            FncTestEnding(False, "������� ksvd4_armd.exe �� �������")
         EndIf

         If $iKsvd4_ctrlPID <> 0 Then
            Local $iClose = ProcessClose ( $iKsvd4_ctrlPID )
               _FileWriteLog($sDebugLogFile, "PID �������� ksvd4_ctrl.exe: " & $iKsvd4_ctrlPID)
            If $iClose = 1 Then
               _FileWriteLog($sLogFile, "������� ksvd4_ctrl.exe PID: " & $iKsvd4_ctrlPID & " ������� ����������")
            Else
               FncTestEnding(False, "�� ������� ���������� ������� ������� ksvd4_ctrl.exe PID: " & $iKsvd4_ctrlPID)
            EndIf
         Else
            FncTestEnding(False, "������� ksvd4_ctrl.exe �� �������")
         EndIf

         ;---------------------------------------------------------------------------------
         ;----- ��� 2. ��������� ������ ������ ��� ��������� � ��� �������������� ---------
         ;---------------------------------------------------------------------------------
         _FileWriteLog($sLogFile, "��� 2. ��������� ������ ������ ��� ��������� � ��� ��������������")

         ActionRunArmAdmin()
         ActionLogInArmAdmin("admin")

         ActionClearFilterFieldInArmAdmin()

         $sAction = "���� � ���� ������ ���"
         FncSendData("���")

         $sAction = "���� �� ��� ���������"
         FncMouseClick("left",330,95,1)

         $sAction = "������� ������� RIGHT"
         FncSendData("{RIGHT}")

         $sAction = "������� ������� DOWN"
         FncSendData("{DOWN}")

         $sAction = "������� ���� �� ������ ������� ��� ���������"
         FncMouseClick("left",150,890,2)

         $sAction = "���� �� �������� ������"
         FncMouseClick("left",188,886,1)

         FncCloseArmAdmin()

         IniWrite ( $sKsvdAppsParams, "KsvdAppsParams", "armOperatorFirstRun", "False")

         FncRunApp ($sInstallPath & "\" & $sArmOperator)
      EndIf

   ElseIf $sVersion = "2.10" Then
      FncCheckAndActivateWindow($sArmOperatorWindowName)
   EndIf

   
   ;---------------------------------------------------------------------------------
   ;----- ���� ��������������� ������ -----------------------------------------------
   ;---------------------------------------------------------------------------------

   $sAction = "���� �� ���� �����"
   If $sVersion = "2.8" Then
      FncMouseClick("left",300,210,1)
      FncMouseClick("right",300,210,1)
   ElseIf $sVersion = "2.10" Then
      FncMouseClick("right",233,390,1)
   EndIf

   $sAction = "������� ������� UP"
   FncSendData("{UP}")
   $sAction = "������� ������� ENTER"
   FncSendData("{ENTER}")
   $sAction = "�������� ����������� ���� ������� DELETE"
   FncSendData("{DEL}")

   $sAction = "���� ������"
   FncSendData($sUserName)
   $sAction = "������� ������� TAB"
   FncSendData("{TAB}")
   $sAction = "���� ������"
   FncSendData($sPassword)
   $sAction = "������� ������� ENTER"
   FncSendData("{ENTER}")
   ;FncCheckAndActivateWindow($sArmOperatorWindowName)
   _FileWriteLog($sLogFile, "������ �����. ������� ��� ���������")

   Sleep(1000)

   If $sVersion = "2.8" And $bArmOperatorWindowIsMaximized = "False" Then
      _FileWriteLog($sLogFile, "�������������� ���� " & $sArmOperatorWindowName & " �� ���� �����")

      $sAction = "���� ������ ������� ���� �� ��������� ����"
      FncMouseClick("right",100,-15,1)

      $sAction = "������� ������� UP"
      FncSendData("{UP}")

      $sAction = "������� ������� UP"
      FncSendData("{UP}")

      $sAction = "������� ������� ENTER"
      FncSendData("{ENTER}")

      _FileWriteLog($sLogFile, "���� " & $sArmOperatorWindowName & " ���� ������� ��������� �� ���� �����")
      IniWrite ( $sKsvdAppsParams, "KsvdAppsParams", "armOperatorWindowIsMaximized", "True")

   EndIf

   Sleep(1000)

   ;---------------------------------------------------------------------------------
   ;----- �������� ���������� ����������� -------------------------------------------
   ;---------------------------------------------------------------------------------

   _FileWriteLog($sLogFile, "�������� ���������� ����������� ������������ " & $sUser)
   If $sVersion = "2.8" Then
      Local $bLogInSuccess = FncCheckAndActivateWindow($sArmOperatorWindowName)
      $hArmOperatorWindowHandle = WinGetHandle ($sArmOperatorWindowName)
   ElseIf $sVersion = "2.10" Then
      Local $bLogInSuccess = FncCheckAndActivateWindow($sArmOperatorWindowName & " [" & $sUserGuiName & "]")
      $hArmOperatorWindowHandle = WinGetHandle ($sArmOperatorWindowName & " [" & $sUserGuiName & "]")
   EndIf

   If $bLogInSuccess = True Then
      _FileWriteLog($sLogFile, "����������� ������������ " & $sUser & " ������ �������")
      If $sVersion = "2.10" Then
         $sArmOperatorLoginSuccess = True
      EndIf
   Else
      If $bExitIfAuthFail = True Then
         FncTestEnding(False, "����������� ������������ " & $sUser & " �� �������")
      ElseIf $bExitIfAuthFail = False Then
         _FileWriteLog($sLogFile, "������ ����������� ����������, ����������� ���������� �����")
      EndIf
   EndIf
      
EndFunc