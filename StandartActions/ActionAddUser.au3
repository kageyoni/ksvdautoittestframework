Func ActionAddUser($sUser)

	Local $sUserName = IniRead ( $sUsersConfig, $sUser, "username", "$sUserName" ) ;��� ������������
	Local $sPassword = IniRead ( $sUsersConfig, $sUser, "password", "$sPassword" ) ;������ ������������
	Local $sGuiName = IniRead ( $sUsersConfig, $sUser, "guiName", "$sGuiName" ) ;�������� ������������ � GUI
	Local $sDescription = IniRead ( $sUsersConfig, $sUser, "description ", "$sDescription" ) ;��������
	Local $sIsAdmin = IniRead ( $sUsersConfig, $sUser, "isAdmin", "$sIsAdmin" ) ;��� ������������ (������������� ��?)
	Local $sIsAdded = IniRead ( $sUsersConfig, $sUser, "isAdded", "$sIsAdded" ) ;�������� �� ������������ �� ������

	Local $sFuncName = "ActionAddUser. "

	_FileWriteLog($sLogFile, "---------------------------------------")
	_FileWriteLog($sLogFile, "������ �������� �������� ������ ������������ " & $sUser)
	_FileWriteLog($sDebugLogFile, "---------------------------------------")
	_FileWriteLog($sDebugLogFile, "������ �������� �������� ������ ������������ " & $sUser)

   	If $sIsAdded = "False" Then

		ActionClearFilterFieldInArmAdmin()

		$sAction = "������� � ��������"
		FncMouseClick("left",320,43,1)
		$sAction = "���� ������� ���������"
		FncSendData("`") ;���� ������� ������� ��������� � ������ ������ �������

	  	$sAction = "���� �� ������� ������������"
		If $sVersion = "2.8" Then
			FncMouseClick("left",400,275,1)
		ElseIf $sVersion = "2.10" Then
			FncMouseClick("left",400,236,1)
		EndIf
	  	
		$sAction = "���� �� ���� ���"
		FncMouseClick("left",110,575,1)

		$sAction = "���� �������� ������������ � ���� ���"
		FncSendData($sUserName)

		$sAction = "���� �� ���� ��������"
		FncMouseClick("left",110,610,1)

		$sAction = "���� ��������"
		FncSendData($sDescription)

		$sAction = "���� �� ���� �����"
		FncMouseClick("left",110,640,1)

		$sAction = "���� ������"
		FncSendData($sUserName)

		$sAction = "���� �� ���� ������"
		FncMouseClick("left",110,670,1)

		$sAction = "���� ������"
		FncSendData($sPassword)

		$sAction = "������� ���� �� ���� �������������"
		FncMouseClick("left",110,700,2)

		If $sIsAdmin = "False" Then
			$sAction = "���� �� �������� False"
			If $sVersion = "2.8" Then
				FncMouseClick("left",107,707,1)
			ElseIf $sVersion = "2.10" Then
				FncMouseClick("left",105,695,1)
			EndIf
		ElseIf $sIsAdmin = "True" Then
			$sAction = "���� �� �������� True"
			If $sVersion = "2.8" Then
				FncMouseClick("left",188,707,1)
			ElseIf $sVersion = "2.10" Then
				FncMouseClick("left",188,695,1)
			EndIf
		Else
			_FileWriteLog($sLogFile, $sFuncName & "������! ������ ������������ ��� ������������: " & $sIsAdmin)
		EndIf

	  $sAction = "���� �� ������ �������� ������������"
	  FncMouseClick("left",55,50,1)

	  ActionClearFilterFieldInArmAdmin()

	  IniWrite ( $sUsersConfig, $sUser, "isAdded", "True" )
	  FncAddItemsToArrayAddedElementsInKSVD("User", $sUser)

	  $sAction = "������������ " & $sUser & " ������� �������� �� ������"
	  _FileWriteLog($sLogFile, $sAction)

   ElseIf $sIsAdded = "True" Then
	  _FileWriteLog($sLogFile, "������������ " & $sUser & " ��� �������� �� ������")
   Else
	  _FileWriteLog($sLogFile, "����� ������������ �������� ������������ " & $sIsAdded)
	  FncTestEnding(False, "������ ���������� ������������")
   EndIf
EndFunc
