Func ActionRemoveTileServer($sTileServer)

    Local $bSuccessOperation = False
    Local $sGuiName = IniRead ( $sTileServersConfig, $sTileServer, "guiName", "$sGuiName" )
    Local $sIsAdded = IniRead ( $sTileServersConfig, $sTileServer, "isAdded", "$sIsAdded" )

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� �������� ��������� ������� " & $sTileServer )

    FncCheckAndActivateWindow($sArmAdminWindowName)

    If $sIsAdded = "False" Then
		_FileWriteLog($sLogFile, "�������� ����������, ��������� �������� ������ " & $sTileServer & " �� �������� �� ������")
        $bSuccessOperation = True
    ElseIf $sIsAdded = "True" Then

        ActionFindTileServerInGui($sTileServer)

        $sAction = "���� �� ������ ��������"
        FncMouseClick("left",50,50,1)

        $sAction = "������������� �������� ��������� ������� �������� ENTER"
        FncSendData("{ENTER}")

        ActionClearFilterFieldInArmAdmin()

        IniWrite( $sTileServersConfig, $sTileServer, "isAdded", "False" )

        _FileWriteLog($sLogFile, "�������� ��������� ������� " & $sTileServer & " ������� ���������")
        $bSuccessOperation = True
    Else
        _FileWriteLog($sLogFile, "�������� ������������ �������� ��������� $sIsAdded: " & $sIsAdded )
        $bSuccessOperation = False
    EndIf
    return $bSuccessOperation
EndFunc