Func ActionLogInArmAdmin($sUser)

    Local $sArmAdminWindowIsMaximized = IniRead( $sKsvdAppsParams, "KsvdAppsParams", "armAdminWindowIsMaximized", "$sArmAdminWindowIsMaximized")
    Local $sUserName = IniRead( $sUsersConfig, $sUser, "username", "$sUserName" )
    Local $sPassword = IniRead( $sUsersConfig, $sUser, "password", "$sPassword" )
    Local $sUserIsAdded = IniRead( $sUsersConfig, $sUser, "isAdded", "$sUserIsAdded" )
    Local $hArmAdminHandle

    ;---------------------------------------------------------------------------------
    ;----- ������������ --------------------------------------------------------------
    ;---------------------------------------------------------------------------------

    _FileWriteLog($sLogFile, "�������� �������� ����������� � ��� ��������������")

    If $sVersion = "2.8" Then
        FncCheckAndActivateWindow("�������������� � ��� �������������� KSVD")
    ElseIf $sVersion = "2.10" Then
        FncCheckAndActivateWindow($sArmAdminWindowName)
    EndIf
    
    $sAction = "���� �� ���� ����� ����� ������������ ������ ������� ����"
    If $sVersion = "2.8" Then
        FncMouseClick("right",250,235,1)
    ElseIf $sVersion = "2.10" Then
        FncMouseClick("right",380,410,1)
    EndIf
    
    $sAction = "������� ������� UP"
    FncSendData("{UP}")
    $sAction = "������� ������� ENTER"
    FncSendData("{ENTER}")
    $sAction = "�������� ����������� ���� ������� DELETE"
    FncSendData("{DEL}")

    $sAction = "���� ����� ������������"
    FncSendData($sUserName)
    $sAction = "������� � ���� ����� ������ � ������� ������� TAB"
    FncSendData("{TAB}")
    $sAction = "���� ������"
    FncSendData($sPassword)
    $sAction = "������� ������ ENTER"
    FncSendData("{ENTER}")
    Sleep(2000)

    ;---------------------------------------------------------------------------------
    ;----- �������������� ���� ���������� --------------------------------------------
    ;---------------------------------------------------------------------------------

    If $sArmAdminWindowIsMaximized = "False" Then

        _FileWriteLog($sDebugLogFile, "������������� ���� ���������� �� ���� �����")

        $sAction = "���� ������ ������� ���� �� ����� ����"
        FncMouseClick("right",60,-15,1)

        $sAction = "������� ������ UP"
        FncSendData("{UP}")

        $sAction = "������� ������ UP"
        FncSendData("{UP}")

        $sAction = "������� ������ ENTER"
        FncSendData("{ENTER}")

        IniWrite( $sKsvdAppsParams, "KsvdAppsParams", "armAdminWindowIsMaximized", "True")

    ElseIf $sArmAdminWindowIsMaximized = "True" Then
        _FileWriteLog($sDebugLogFile, "���� ��� ���������, �������������� �������� �� ���������")
    Else
        FncTestEnding(False, "� ������� ������� ������������ �������� ��������� $sArmAdminWindowIsMaximized: " & $sArmAdminWindowIsMaximized)
    EndIf

    ;---------------------------------------------------------------------------------
    ;----- �������� ���������� ����������� -------------------------------------------
    ;---------------------------------------------------------------------------------

    If $sVersion = "2.8" Then
        Local $sAuthSuccess = FncCheckAndActivateWindow($sArmAdminWindowName)

        If $sAuthSuccess = True Then
            If $sUserIsAdded = "True" Then
                _FileWriteLog($sLogFile, "����������� ������ �������")
            Else
                _FileWriteLog($sLogFile, "������������, ��� ������� ������������� �����, �� �������� � KSVD. ������ ��� - ����������� ���������� ��� ����� �������������.")
            EndIf
        Else
            If $bExitIfAuthFail = True Then
                FncTestEnding(False, "������ �����������")
            ElseIf $bExitIfAuthFail = False Then
                _FileWriteLog($sLogFile, "������ ����������� ����������, ����������� ���������� �����")
            EndIf
        EndIf

    ElseIf $sVersion = "2.10" Then
        _FileWriteLog($sLogFile, "�������� ���������� �����������")
        _FileWriteLog($sDebugLogFile, "����������� ������� ������������ ������������ � GUI")

        $sAction = "���� �� ������ ��������"
        FncMouseClick("left",50,50,1)

        $sAction = "���� �� ���� ������������ � ������"
        FncMouseClick("left",310,305,1)

        Sleep(500)
        $sAction = "��������� ���� ������� RIGHT"
        FncSendData("{RIGHT}")
        Sleep(500)
        $sAction = "������� � ������������ ������������ ������� DOWN"
        FncSendData("{DOWN}")
        Sleep(500)
        $sAction = "���� �� ���� ���"
        FncMouseClick("left",110,575,2)

        Local $sClipboard = FncCopyToClipboard()
        _FileWriteLog($sDebugLogFile, "� ������ ������ Windows ����������: " & $sClipboard)

        If $sClipboard = "�������������" Then
            If $sUserIsAdded = "True" Then
                _FileWriteLog($sLogFile, "����������� ������������ ������, ����������� ������ �������")
            Else
                _FileWriteLog($sLogFile, "������������, ��� ������� ������������� �����, �� �������� � KSVD. ������ ��� - ����������� ���������� ��� ����� �������������.")
                FncTestEnding(False, "������. �������� ����������� � ��������� ����������������� �������")
            EndIf
        Else
            _FileWriteLog($sLogFile, "����������� ������������ �� ������, ������ �����������")
            _FileWriteLog($sLogFile, "$bExitIfAuthFail = " & $bExitIfAuthFail)
            If $bExitIfAuthFail = True Then
                FncTestEnding(False, "������ �����������")
            ElseIf $bExitIfAuthFail = False Then
                _FileWriteLog($sLogFile, "������ ����������� ����������, ����������� ���������� �����")
            EndIf
        EndIf
    EndIf

Endfunc