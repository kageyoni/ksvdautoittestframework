Func ActionPlayFileInVlc($sFileName, $sFullPath)

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� ��������������� ����� �� ����� " & $sFileName & " � ������������������ VLC")

    Local $sVlcStandartWindowTitle = "������������������ VLC"
    Local $sVlcWindowTitle = $sFileName & " - ������������������ VLC"

    ShellExecute("C:\Program Files (x86)\VideoLAN\VLC\vlc.exe" , '"' & $sFullPath & '"', "", "runas")
    Sleep(2000)

    If WinExists("������") Then
        FncCloseWindow($sVlcStandartWindowTitle)
		FncTestEnding(False, "�� ������� ������������� ����� �� ����� " & $sFileName)
    Else
        FncCheckAndActivateWindow($sVlcWindowTitle)
        Sleep(6000)
        $sAction = "����������� �������� ��������������� ����� �� ����� " & $sFileName & " � ������������������ VLC"
        _FileWriteLog($sLogFile, $sAction)
        FncScreenCapture($sKeyScreenPath, $sAction)
        FncCloseWindow($sVlcWindowTitle)
    EndIf
EndFunc