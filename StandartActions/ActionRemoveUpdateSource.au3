Func ActionRemoveUpdateSource($sUpdateSource)

    Local $bSuccessOperation = False
    Local $sUpdateSourceGUIName = IniRead( $sUpdateSources, $sUpdateSource, "guiName", "$sUpdateSourceGUIName")
    Local $sIsAdded = IniRead( $sUpdateSources, $sUpdateSource, "isAdded", "$sIsAdded")

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� �������� ����� ���������� " & $sUpdateSource)

    FncCheckAndActivateWindow($sArmAdminWindowName)

    If $sIsAdded = "False" then
        _FileWriteLog($sLogFile, "����� ���������� " & $sUpdateSource & " �� ���������� �� �������, �������� �� ���������")
        $bSuccessOperation = True
    Else

        ActionClearFilterFieldInArmAdmin()

        $sAction = "���� �����"
        FncSendData($sUpdateSourceGUIName)

        $sAction = "���� �� ����� ����������"
        If $sVersion = "2.8" Then
            FncMouseClick("left",350,115,1)
        ElseIf $sVersion = "2.10" Then
            FncMouseClick("left",350,100,1)
        EndIf

        $sAction = "���� �� ���� ��� ����� ����������"
        FncMouseClick("left",120,580,1)

        Local $sClipboard = FncCopyToClipboard()
        _FileWriteLog($sDebugLogFile, "� ������ ������ Windows ����������: " & $sClipboard)
        If $sClipboard = $sUpdateSourceGUIName Then
            _FileWriteLog($sLogFile, "����� ���������� ������� � GUI")
        Else
            FncTestEnding(False, "������ ActionRemoveUpdateSource. ��������� ����� ���������� �� ������� � GUI")
        EndIf

        $sAction = "���� �� ������ �������"
        FncMouseClick("left",50,50,1)

        $sAction = "������������� �������� ������� ENTER"
        FncSendData("{ENTER}")

        IniWrite( $sUpdateSources, $sUpdateSource, "isAdded", "False")

        _FileWriteLog($sLogFile, "����� ���������� " & $sUpdateSource & " ������� �������")
        $bSuccessOperation = True
    EndIf
    return $bSuccessOperation
Endfunc