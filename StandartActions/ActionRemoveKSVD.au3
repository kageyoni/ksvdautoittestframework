Func ActionRemoveKSVD()

    ; ---- ������������� ����������� ���������� ----
    Local $sInstallPath = IniRead ( $sCommonConfig, "InstallInformation", "installPath", "$sInstallPath" )
    Local $sUninstallerWindowName = "������������� � Kraftway Smart Video Detector"

    ;---------------------------------------------------------------------------------
    ;----- ������ �������������� � ������ �������� �������� --------------------------
    ;---------------------------------------------------------------------------------

    _FileWriteLog($sLogFile, "�������� �������� �������� KSVD")

    Local $bKSVDInstalled = FncCheckKSVDInstallation()

    If $bKSVDInstalled = True Then
        _FileWriteLog($sLogFile, "������ ��������������")
        FncRunApp ($sInstallPath & "\unins000.exe")
    Else
        FncTestEnding(False, "�������� KSVD ����������, ��������� �� ������ ����: " & $sInstallPath & "\unins000.exe")
    EndIf

    FncCheckAndActivateWindow($sUninstallerWindowName)

    $sAction = "������������� ��������"
    FncControlClick($sUninstallerWindowName,"&��","Button1")

    ;---------------------------------------------------------------------------------
    ;----- �������������� ���� ���������� �������� � ����������� �� ��� ����������� --
    ;---------------------------------------------------------------------------------

    Local $sFullIUinstallCompleteWindow =  WinWaitActive($sUninstallerWindowName, "��������� Kraftway Smart Video Detector ���� ��������� ������� � ������ ����������.", 20) ;AutoIt ����� 0 � ������, ���� �������� ���� �� �������
                                                                                                                                                                            ;20 - ������� �������� ���� 
    if $sFullIUinstallCompleteWindow <> 0 Then 
        _FileWriteLog($sLogFile, "������������� KSVD ��������� �������")
    Else
        Local $sUninstallerCantDeleteAllElements = WinWaitActive($sUninstallerWindowName, "����� ��������� �� ������� �������. �� ������ ������� �� ��������������.", 20)
        if $sUninstallerCantDeleteAllElements <> 0 then
            _FileWriteLog($sLogFile, "������������� KSVD �� ���� ������� ����� ���������")
        Else
            FncTestEnding(False, "�� ������� ���������� �������� ���� ���������� ��������")
        endif
    EndIf

    ;---------------------------------------------------------------------------------
    ;----- ���������� �������� -------------------------------------------------------
    ;---------------------------------------------------------------------------------

    $sAction = "������� ������ ENTER"
    FncSendData("{ENTER}") ;�������� ������������ ������� Enter, �.�. AutoIT �� ������� ������

Endfunc