Func ActionFindArchiveInGui ($sArchive)

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� �������� ������� ��������� " & $sArchive & " � GUI")
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "������ �������� �������� ������� ��������� " & $sArchive & " � GUI")

    Local $sGuiName = IniRead ( $sArchivesConfig, $sArchive, "guiName", "$sGuiName")
    Local $sFuncName = "ActionAddArchiveGUI. InnerFncFindArchiveInGui. "

    ActionClearFilterFieldInArmAdmin()

    $sAction = "���� � ���� ������ ����� ���������"
    FncSendData($sGuiName)

    $sAction = "���� �� ��������������� ���������"
    If $sVersion = "2.8" Then
        FncMouseClick("left",370,130,1)
    ElseIf $sVersion = "2.10" Then
        FncMouseClick("left",370,110,1)
    EndIf
    
    $sAction = "������� ���� �� ���� ���"
    FncMouseClick("left",160,580,2)

    Local $sClipboard = FncCopyToClipboard()

    If $sGuiName = $sClipboard Then
        _FileWriteLog($sLogFile, "��������� ������� � GUI")
    Else
        ;� ������, ���� ��������� ��� ��������� � �����-�� ������, ��� ����� ���������� ������� ����
        $sAction = "���� �� ��������������� ��������� ������� ����"
        If $sVersion = "2.8" Then
            FncMouseClick("left",385,170,1)
        ElseIf $sVersion = "2.10" Then
            FncMouseClick("left",385,130,1)
        EndIf
        $sAction = "������� ���� �� ���� ���"
        FncMouseClick("left",160,580,2)
        Local $sClipboard = FncCopyToClipboard()
            If $sGuiName = $sClipboard Then
                _FileWriteLog($sLogFile, "��������� ������� � GUI")
            Else
                _FileWriteLog($sLogFile, "�� ������� ����� ��������� � GUI")
                FncTestEnding(False, "��������� �� ������� � GUI")
            EndIf
    EndIf
EndFunc