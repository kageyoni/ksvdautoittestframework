Func ActionAddViewPanelByScada($aParameters) ;($sRegionName, $sObjectName, ViewPanel, $aCameras)

	;������� �� ������� �� ���� ������ ����� ��� ����������, �.�. ��������� ��� ������, ����������� � �������
	;���� � ������� ��� �� ����� ������, ����������� ���� ������ Camera1

	$sFuncName = "ActionAddViewPanelByScada. "

	Local $sRegionName = $aParameters[0] ;��� �������
	Local $sObjectName = $aParameters[1] ;��� �������
	Local $sViewPanelName = $aParameters[2] ;��� ���������
	Local $iUsingMonitor = $aParameters[3] ;����� ��������
	Local $iGridX = $aParameters[4] ;������ �����. ������ ��������
	Local $iGridY = $aParameters[5] ;������ �����. ������ ��������

	Local $sRegionAdded = IniRead ($sRegionsConfig, $sRegionName, "isAdded", "False")
    Local $sObjectAdded = IniRead ($sRegionsObjectsConfig, $sObjectName, "isAdded", "False")
	Local $sViewPanelAdded = IniRead ($sViewPanelsConfig, $sViewPanelName, "isAdded", "False")
	Local $iCountOfAddedToRegionsObjectCameras = IniRead ($sRegionsObjectsConfig, $sObjectName , "countOfAddedToRegionsObjectCameras", "0")
	Local $iCountOfAddedToRegionsObjectViewPanels = IniRead ($sRegionsObjectsConfig, $sObjectName , "countOfAddedToRegionsObjectViewPanels", "0")

	_FileWriteLog($sLogFile, "---------------------------------------")
	_FileWriteLog($sLogFile, "������ �������� ���������� ��������� " & $sViewPanelName & " ����� ������� �����")
	_FileWriteLog($sDebugLogFile, "---------------------------------------")
	_FileWriteLog($sDebugLogFile, "������ �������� ���������� ��������� " & $sViewPanelName & " ����� ������� �����")

	If $sRegionAdded = "False" Then
		_FileWriteLog($sLogFile, "������ " &  $sRegionName & " �� ��������")
		ActionAddRegionByScada($sRegionName)
	EndIf

	If $sObjectAdded = "False" Then
		_FileWriteLog($sLogFile, "������ " &  $sObjectName & " �� ��������")
		ActionAddObjectToRegionByScada($sRegionName, $sObjectName)
	EndIf

	If $iCountOfAddedToRegionsObjectCameras = 0 Then
		_FileWriteLog($sLogFile, $sFuncName & "� ������� ��� �� ����� ����������� ������. ��������� Camera1")
		ActionAddCameraToObjectByScada($sRegionName, $sObjectName, "Camera1")
		$iCountOfAddedToRegionsObjectCameras += 1
	EndIf

	If $sViewPanelAdded = "True" Then
		_FileWriteLog($sLogFile, "��������� " & $sViewPanelName & " ��� ���������")
	ElseIf $sViewPanelAdded = "False" Then
		InnerDoAddViewPanel($aParameters)
		$sAction = "��������� " & $sViewPanelName & " ������� ���������"
	  	_FileWriteLog($sLogFile, $sAction)
	  	FncScreenCapture($sKeyScreenPath, $sAction)
	Else
		_FileWriteLog($sLogFile, "�������� ������������� �������� ��������� $sViewPanelAdded " & $sViewPanelAdded)
	EndIf
EndFunc

func InnerDoAddViewPanel($aParameters)
	
	Local $sRegionName = $aParameters[0] ;��� �������
	Local $sObjectName = $aParameters[1] ;��� �������
	Local $sViewPanelName = $aParameters[2] ;��� ���������
	Local $iUsingMonitor = $aParameters[3] ;����� ��������
	Local $iGridX = $aParameters[4] ;������ �����. ������ ��������
	Local $iGridY = $aParameters[5] ;������ �����. ������ ��������

	Local $iCountOfAddedToRegionsObjectCameras = IniRead ($sRegionsObjectsConfig, $sObjectName , "countOfAddedToRegionsObjectCameras", "0")
	Local $iCountOfAddedToRegionsObjectViewPanels = IniRead ($sRegionsObjectsConfig, $sObjectName , "countOfAddedToRegionsObjectViewPanels", "0")
	Local $iTotalCountOfAddedViewPanels = IniRead( $sViewPanelsConfig, "CountOfAddedViewPanels", "countOfAddedViewPanels", "$iTotalCountOfAddedViewPanels")

	ActionFindRegionObjectInScada($sRegionName, $sObjectName, False)

	$sAction = "���� �� ������ ������� ���������"
	FncMouseClick("left",50,100,1)

	$sAction = "���� �� ������� " & $sObjectName
	FncMouseClick("left",340,105,1)

	Local $iInnerCount = $iCountOfAddedToRegionsObjectCameras + $iCountOfAddedToRegionsObjectViewPanels
	_FileWriteLog($sDebugLogFile, "����� ���-�� ����������� ����� � ��������� ������� " & $iInnerCount)


	$sAction = "������� � ��������� ��������� �������� DOWN"
	for $i = 0 to $iInnerCount
		FncSendData("{DOWN}")
	next

	$sAction = "���� �� ���� ��������"
	FncMouseClick("left",110,550,1)

	$sAction = "���� ����� ���������" & $sViewPanelName
	FncSendData($sViewPanelName)

	$sAction = "���������� �������� ��������� �������� ENTER"
	FncSendData("{ENTER}")

	Local $sClipboard = FncCopyToClipboard()
    _FileWriteLog($sDebugLogFile, "� ������ ������ Windows ����������: " & $sClipboard)
    If $sViewPanelName = $sClipboard Then
		_FileWriteLog($sLogFile, "��������� " & $sViewPanelName & " ������� � GUI")
	Else
		FncTestEnding(False, "�� ������� ����� ��������� " & $sViewPanelName & " � GUI")
	EndIf

	$sAction = "���� �� ������� " & $sObjectName
	FncMouseClick("left",340,105,1)

	Local $iInnerCount = $iCountOfAddedToRegionsObjectCameras + $iCountOfAddedToRegionsObjectViewPanels
	_FileWriteLog($sDebugLogFile, "����� ���-�� ����������� ����� � ��������� ������� " & $iInnerCount)

	$sAction = "������� � ��������� ��������� �������� DOWN"
	for $i = 0 to $iInnerCount
		FncSendData("{DOWN}")
	next

	;---- ���������� ��������� ���������� �������� ----
	Local $iMonitorCoordY = 54 
	for $i = 0 to $iUsingMonitor -1
		$iMonitorCoordY += 36
	next

	$sAction = "���� �� ������ ������������ ������� �" & $iUsingMonitor
	FncMouseClick("left",880,$iMonitorCoordY,1)

	$sAction = "���� �� ������ ���� ������� �����"
	FncMouseClick("left",1485,45,1)

	$sAction = "������� ����"
	FncSendData("{BACKSPACE}")


	$sAction = "���� ��������" & $iGridX &  " � ����"
	FncSendData($iGridX)
	$sAction = "������� �� ������ ���� ������� ����� �������� TAB"
	FncSendData("{TAB}")
	$sAction = "���� ��������" & $iGridY &  " � ����"
	FncSendData($iGridY)

	Local $aMods = InnerCoordCalculate($iGridX, $iGridY) ;������� ��������� �����
	Local $iCellXCoor = 1042 ;���������� ��� 1920*1080
	Local $iCellYCoor = $aMods[1] + 65 ;���������� ��� 1920*1080
	Local $iCellNumberX = 0
	Local $iCellNumberY = 1

	;---- ���������� ����� � ����� ----
	for $i = 1 to $iCountOfAddedToRegionsObjectCameras

	_FileWriteLog($sDebugLogFile, "$i =  " & $i & " $iCountOfAddedToRegionsObjectCameras = " & $iCountOfAddedToRegionsObjectCameras)

		$iCellXCoor += $aMods[0]
		$iCellNumberX += 1
		
		
		if $iCountOfAddedToRegionsObjectCameras > $iGridX then
			$iCellYCoor += $aMods[1]
			$iCellNumberY += 1
		endif

		$sAction = "���� ������ ������� �� ������ " & $iCellNumberX & " x " & $iCellNumberY
		FncMouseClick("right",$iCellXCoor,$iCellYCoor,1)

		$sAction = "����� ������ ��������� ������ �������� DOWN"
		FncSendData("{DOWN}")

		$sAction = "������� � ����� ��������� ������ �������� ENTER"
		FncSendData("{ENTER}")

		$sAction = "���� �� ������ ����������� � ���� ������"
		FncMouseClick("left",20,90,1)

		$sAction = "���� �� ���� �������"
		FncMouseClick("left",50,65,1)

		
		Local $sAddedCamera = IniRead ($sRegionsObjectsConfig, $sObjectName , "addedCamera" & $i, "null")
		Local $sAddedCameraGuiName = IniRead ($sCamerasConfig, $sAddedCamera, "cameraGuiName", "null")
		$sAction = "���� �������� ������ " & $sAddedCameraGuiName
		FncSendData($sAddedCameraGuiName)

		$sAction = "���� �� ��������� ������ " & $sAddedCameraGuiName
		FncMouseClick("left",95,115,1)

		$sAction = "��������� ������ � ������ ������� ENTER"
		FncSendData("{ENTER}")

	next

	
	$iCountOfAddedToRegionsObjectViewPanels += 1
	$iTotalCountOfAddedViewPanels += 1
	IniWrite($sRegionsObjectsConfig, $sObjectName , "countOfAddedToRegionsObjectViewPanels", $iCountOfAddedToRegionsObjectViewPanels)
	IniWrite( $sViewPanelsConfig, "CountOfAddedViewPanels", "countOfAddedViewPanels", $iTotalCountOfAddedViewPanels)
	IniWrite($sViewPanelsConfig, $sViewPanelName, "isAdded", "True")

	_FileWriteLog($sDebugLogFile, "$iCountOfAddedToRegionsObjectCameras = " & $iCountOfAddedToRegionsObjectCameras)
endfunc

Func InnerCoordCalculate($iGridX, $iGridY)

	Local $iVewPanelXSize = 860
	Local $iVewPanelYSize = 935

	Local $iXMod = $iVewPanelXSize/$iGridX - 15 ;115
	Local $iYMod = $iVewPanelYSize/$iGridY - 15 ; 77
	
	Local $aMods = [$iXMod, $iYMod]
	return $aMods
EndFunc