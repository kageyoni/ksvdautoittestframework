Func ActionCloseArmExportFromArchive()

    Local $sArmExportFromArchiveAuth28WindowName = "�������������� � ��� �������� ������� KSVD"

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� �������� ���������� ��� �������� �������")
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "������ �������� �������� ���������� ��� �������� �������")

    if $sVersion = "2.8" Then
        If WinExists($sArmExportFromArchiveWindowName ) Then
            FncCloseWindowByHandle($sArmExportFromArchiveWindowName)
            _FileWriteLog($sLogFile, "���������� ��� �������� ������� ������� �������")
        ElseIf WinExists($sArmExportFromArchiveAuth28WindowName) Then
            FncCloseWindowByHandle($sArmExportFromArchiveAuth28WindowName)
            _FileWriteLog($sLogFile, "���������� ��� �������� ������� ������� �������")
        Else
            FncTestEnding(False, "�� ������� ���������� �������� ���� ��� �������� �������")
        EndIf

    ElseIf $sVersion = "2.10" Then
        If WinExists($sArmExportFromArchiveWindowName) Then
            FncCloseWindowByHandle($sArmExportFromArchiveWindowName)
            _FileWriteLog($sLogFile, "���������� ��� �������� ������� ������� �������")
        Else
            FncTestEnding(False, "�� ������� ���������� �������� ���� ��� �������� �������")
        EndIf
    EndIf
EndFunc