Func ActionAddUpdateSourse($sUpdateSource, $sKSVDSetupFolder)

    ; ---- ������ ������� ����� ���������� ----
    Local $sUpdateSourceName = IniRead( $sUpdateSources, $sUpdateSource, "name", "$sUpdateSourceName")
    Local $sUpdateSourceGUIName = IniRead( $sUpdateSources, $sUpdateSource, "guiName", "$sUpdateSourceGUIName")
    Local $sDescription = IniRead( $sUpdateSources, $sUpdateSource, "description", "$sDescription")
    Local $sUpdateTo666 = IniRead( $sUpdateSources, $sUpdateSource, "updateTo666", "$sUpdateTo666")
    Local $sLogin = IniRead( $sUpdateSources, $sUpdateSource, "login", "$sLogin")
    Local $sPassword = IniRead( $sUpdateSources, $sUpdateSource, "password", "$sPassword")
    Local $sPeriod = IniRead( $sUpdateSources, $sUpdateSource, "period", "$sPeriod")
    Local $sIsAdded = IniRead( $sUpdateSources, $sUpdateSource, "isAdded", "$sIsAdded")
    Local $sIsHTTP = IniRead( $sUpdateSources, $sUpdateSource, "isHTTP", "$sIsHTTP")
    Local $sURI = IniRead( $sUpdateSources, $sUpdateSource, "URI", "$sURI")

    ; ---- ������������� ����������� ���������� ----
    Local $sWindowsInstallerFolder = $sKSVDSetupFolder & "\install\windows"
    Local $sUpdateFolder = $sKSVDSetupFolder & "\update\windows"
    Local $sWindowsV666TempFolder = $sWindowsInstallerFolder & "\V666Update"
    Local $sWindowsV666FinalFolder = $sWindowsV666TempFolder & "\finalUpdateV666"

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� ���������� ����� ���������� " & $sUpdateSource)
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "������ �������� ���������� ����� ���������� " & $sUpdateSource)

    if $sIsAdded = "True" then
        _FileWriteLog($sLogFile, "����� ���������� " & $sUpdateSource & " ��� ��������� �� ������")
        ActionClearFilterFieldInArmAdmin()

        $sAction = "���� ����� ��������� ����� ����������"
        FncSendData("LocalUpdateToV666")

        $sAction = "���� �� ����� ����������"
        If $sVersion = "2.8" Then
            FncMouseClick("left",350,115,1)
        ElseIf $sVersion = "2.10" Then
            FncMouseClick("left",350,100,1)
        EndIf
        
    else

        $sAction = "������� � ��������"
        FncMouseClick("left",320,43,1)

        $sAction = "���� ������� ��������"
        FncSendData("`") ;���� ������� ������� ��������� � ������ ������ �������

        $sAction = "���� �� ������� ����� ����������"
        If $sVersion = "2.8" Then
            FncMouseClick("left",350,295,1)
        ElseIf $sVersion = "2.10" Then
            FncMouseClick("left",350,255,1)
        EndIf
        
        $sAction = "���� �� ���� ��� ����� ����������"
        FncMouseClick("left",120,580,1)

        $sAction = "���� �����"
        FncSendData($sUpdateSourceName)

        $sAction = "������� � ���� �������� ����������� ������� ������ DOWN"
        FncSendData("{DOWN}")

        $sAction = "���� ��������"
        FncSendData($sDescription)

        $sAction = "������� � ���� URI ����������� ������� ������ DOWN"
        FncSendData("{DOWN}")

        $sAction = "���� ������ � ����� URI"
        If $sUpdateTo666 = "True" Then
            FncSendData("file://" & $sWindowsV666FinalFolder)
        Else
            If $sIsHTTP = "False" Then
                FncSendData("file://" & $sUpdateFolder)
            ElseIf $sIsHTTP = "True" Then
                FncCheckNetworkFileExists($sURI)
                FncSendData($sURI)
            EndIf
        EndIf

        $sAction = "���� �� ������ �������"
        FncMouseClick("left",120,50,1)

        $sAction = "���������� ����� ����������"
        FncScreenCapture($sKeyScreenPath, $sAction)

        FncAddItemsToArrayAddedElementsInKSVD("UpdateSource", $sUpdateSource)

        IniWrite( $sUpdateSources, $sUpdateSource, "isAdded", "True")
        _FileWriteLog($sLogFile, "����� ���������� " & $sUpdateSource & " ������� ���������")
        
    EndIf
Endfunc