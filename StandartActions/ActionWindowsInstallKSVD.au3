Func ActionWindowsInstallKSVD($aSetupParameters)

    Local $bDefaultInstall = $aSetupParameters[0]
    Local $bUseExistingLicenseKey = $aSetupParameters[1]
    Local $sInstallationVersion = $aSetupParameters[2]
    Local $sKSVDSetupFolder = $aSetupParameters[3]
    Local $sInstallPath = $aSetupParameters[4]

    Local $sChooseLicenseKeyWindows = "����� ����� ������������� �����"
    Local $sInstallerWindowName = "��������� � Kraftway Smart Video Detector"
    Local $sWindowsInstallerFolder = $sKSVDSetupFolder & "\install\windows"
    Local $sWindowsInstallerEXEName = "KsvdInstall_" & $sInstallationVersion & ".exe"
    Local $sWindowsInstallerEXEPath = $sWindowsInstallerFolder & "\" & $sWindowsInstallerEXEName

    _FileWriteLog($sLogFile, "�������� �������� ��������� KSVD")
    _FileWriteLog($sLogFile, "����������� ���������: " & $bDefaultInstall)
    _FileWriteLog($sLogFile, "������������� ������� ���������������� ����� ��� ���������: " & $bUseExistingLicenseKey)


    ;---------------------------------------------------------------------------------
    ;----- ������ ���������� ����� ---------------------------------------------------
    ;---------------------------------------------------------------------------------
    ;----- �������� ������� ������ KSVD ----------------------------------------------
    ;---------------------------------------------------------------------------------

    Local $bKSVDInstalled = FncCheckKSVDInstallation()

    If $bKSVDInstalled = True Then
        ActionRemoveKSVD()
    EndIf

    ;---------------------------------------------------------------------------------
    ;----- ����� ���������� ���������� -----------------------------------------------
    ;---------------------------------------------------------------------------------

    FncResetResources()

    ;---------------------------------------------------------------------------------
    ;----- �������� ���������� ��������� KSVD ----------------------------------------
    ;---------------------------------------------------------------------------------

    if FileExists($sInstallPath)Then
        Local $iRemoveResult =  DirRemove($sInstallPath, $DIR_REMOVE) ;Success:	1. Failure:	0.
        _FileWriteLog($sLogFile, "���������� ��������� KSVD " & $sInstallPath & " �������� ����� ���������� ���������")
        If $iRemoveResult = 1 Then
            _FileWriteLog($sLogFile, "���������� " & $sInstallPath & " ������� �������")
        Else
            _FileWriteLog($sLogFile, "�� ������� ������� ���������� " & $sInstallPath & ". �������� ������ ��� ���������� �������� ��������� KSVD")
        EndIf
    Else
        _FileWriteLog($sLogFile, "���������� ��������� KSVD " & $sInstallPath & " �� �������� ����� ���������� ���������")
    EndIf

    ;---------------------------------------------------------------------------------
    ;----- ������ ������������ -------------------------------------------------------
    ;---------------------------------------------------------------------------------


    _FileWriteLog($sLogFile, "������ �����������")
    _FileWriteLog($sDebugLogFile, "���������� �����������: " & $sWindowsInstallerFolder)

    FncRunApp ($sWindowsInstallerEXEPath)
    FncCheckAndActivateWindow($sInstallerWindowName)

    ;---------------------------------------------------------------------------------
    ;----- ���� ���������� ��������� -------------------------------------------------
    ;---------------------------------------------------------------------------------

    if $sVersion = "2.8" Then
        $sAction = "�����������. ������� ������ �����"
        FncControlClick($sInstallerWindowName,'&����� >','TNewButton1')
    EndIf


    $sAction = "����� ����� ���������. ������� ������ �����"

    If $sVersion = "2.8" Then
        FncControlClick($sInstallerWindowName,'&����� >','TNewButton3')
    ElseIf $sVersion = "2.10" Then
        FncControlClick($sInstallerWindowName,'&����� >','TNewButton2')
    EndIf
    

    If WinExists("����� ����������") Then
        $sAction = "����� �������� ��, ���� ����� ��������� ��� ����������"
        FncControlClick("����� ����������","&��","Button1")
    EndIf

    $sAction = "����� ����������� ���������. ������� ������ �����"
    FncControlClick($sInstallerWindowName,'&����� >','TNewButton3')

    If $bDefaultInstall = "True" Then                                       ;����������� ���������
        $sAction = "��������� ��. ������� ������ �����"
        FncControlClick($sInstallerWindowName,'&����� >','TNewButton3')

        $sAction = "��������� ���������� ��. ������� ������ �����"
        FncControlClick($sInstallerWindowName,'&����� >','TNewButton4')

        $sAction = "��������� �������. ������� ������ �����"
        FncControlClick($sInstallerWindowName,'&����� >','TNewButton4')

        $sAction = "�������� � ���� ����. ������� ������ �����"
        FncControlClick($sInstallerWindowName,'&����� >','TNewButton5')
    Else                                                                    ;��������� ���������, ��� �� �������
        ;$sAction = "��������� ��. ���� ������"
        ;FncControlClick($sInstallerWindowName,'&����� >','TNewButton3')

        FncTestEnding(False, "��� �� �������")
    EndIf

    
    $sAction = "������� ������ ����������"
    FncControlClick($sInstallerWindowName,'&����������','TNewButton5')

    ;---------------------------------------------------------------------------------
    ;----- ����� ������ ������ � ��������� -------------------------------------------
    ;---------------------------------------------------------------------------------

    FncCheckAndActivateWindow($sChooseLicenseKeyWindows)

    Sleep(2000)

    If $bUseExistingLicenseKey = "True" Then
        $sAction = "���� ���� �� ���������� � ������"
        _FileWriteLog($sLogFile, $sAction)
        FncScreenCapture($sDebugScreenPath, $sAction) ;������ ������� �����, �.�. ������� ���� ���� ��� �� �������� ���������������� ����������
        ControlSetText($sChooseLicenseKeyWindows,'',"Edit1",$sWindowsInstallerFolder & "\" & $sKeyFileName)
        $sAction = "������� ������ ������� ����� ����� ���� �� ������������� �����"
        FncControlClick($sChooseLicenseKeyWindows,"&�������","Button1")
    Else
        _FileWriteLog($sLogFile, "������ ������� ��������� ��� ������������� �����")
        $sAction = "������� ������ ������"
        FncControlClick($sChooseLicenseKeyWindows,"������",'Button2')

        _FileWriteLog($sDebugLogFile, "�������� ���������� ���� ��������� MsgBox")
        WinWaitActive("[CLASS:#32770]")
        _FileWriteLog($sDebugLogFile, "���� [CLASS:#32770] �������")
        $sAction = "������� ������ OK ����� ���������� ��������� �������� ENTER"
        FncSendData("{ENTER}") 
    EndIf

    ;---------------------------------------------------------------------------------
    ;----- ���������� ��������� ------------------------------------------------------
    ;---------------------------------------------------------------------------------

    _FileWriteLog($sDebugLogFile, "�������� ���������� ���� Setup")
    WinWaitActive("Setup")

    _FileWriteLog($sDebugLogFile, "�������� ���������� ���� ��������� MsgBox")
    WinWaitActive("[CLASS:#32770]")
    _FileWriteLog($sDebugLogFile, "���� [CLASS:#32770] �������")

    $sAction = "������� ������ OK ����� ���������� ���������"
    FncSendData("{ENTER}") 

    _FileWriteLog($sDebugLogFile, "�������� ���������� ��������� � ��������� ������ ���������")
    WinWaitActive($sInstallerWindowName,"&���������")

    $sAction = "������� ������ ���������"
    FncControlClick($sInstallerWindowName,"&���������","TNewButton5")

    ;---------------------------------------------------------------------------------
    ;----- �������� ���������� ��������� ---------------------------------------------
    ;---------------------------------------------------------------------------------

    If FileExists($sInstallPath & "\" & $sArmAdmin) Then
        IniWrite( $sCommonConfig, "InstallInformation", "installPath", $sInstallPath )
        _FileWriteLog($sLogFile, "��������� KSVD ������ �������")
    Else
        FncTestEnding(False, "�� ������ ���� " & $sArmAdmin & " � ���������� ���������")
    EndIf

    ;---------------------------------------------------------------------------------
    ;----- �������� ���������� �������� GUI �� ������� ��������� ---------------------
    ;---------------------------------------------------------------------------------

    Local $sSettingsFolder = @AppDataDir & "\Kraftway"
    _FileWriteLog($sLogFile, "$sSettingsFolder = " & $sSettingsFolder)
    If FileExists ($sSettingsFolder) Then ;�������� �� ��������, ������ ������� ����� ��������� ��� ������������� �����, ��� � ������ ����������
        DirRemove($sSettingsFolder, $DIR_REMOVE)
        _FileWriteLog($sLogFile, "����������� �������� ���������� � ����������� GUI KSVD")
    EndIf

    _FileWriteLog($sLogFile, "�������� ��������� KSVD ����������� �������")

    ;---------------------------------------------------------------------------------
    ;----- ������ ��� �������������� � ��������� �������� ----------------------------
    ;---------------------------------------------------------------------------------

    If $bUseExistingLicenseKey = "True" Then
        ActionRunArmAdmin()
        ActionLogInArmAdmin("admin")
    Else
        ActionRunArmAdmin()
        ActionEnterLicenseKeyInArmAdmin()
        ActionLogInArmAdmin("admin")
    EndIf

Endfunc