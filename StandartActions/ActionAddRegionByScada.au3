Func ActionAddRegionByScada($sRegionName)
	
	Local $sRegionAdded = IniRead ($sRegionsConfig, $sRegionName, "isAdded", "$sRegionAdded")
	Local $sCountOfAddedRegions = IniRead ($sAddedObjectsConfig, "AddedObjects", "countOfAddedRegions", "0")

	_FileWriteLog($sLogFile, "---------------------------------------")
	_FileWriteLog($sLogFile, "������ �������� ���������� ������� " & $sRegionName )
	_FileWriteLog($sDebugLogFile, "---------------------------------------")
	_FileWriteLog($sDebugLogFile, "������ �������� ���������� ������� " & $sRegionName )

	If $sVersion = "2.8" Then
		FncTestEnding(False, "���������� �������� ����� ����� �� ����������� � KSVD 2.8")
	EndIf

	If $sRegionAdded = "True" Then
		_FileWriteLog($sLogFile, "���������� �� ���������, ��������� ������ " & $sRegionName & " ��� �������� �� ������")
	Else
		$sAction = "���� �� ������� �����"
		FncMouseClick("left",475,15,1)

		$sAction = "���� ������ ������� �� ����� ������"
		FncMouseClick("right",800,85,1)

		$sAction = "������� �� ����� �������� �������� ������� UP"
		FncSendData("{UP}")

		$sAction = "����� ������ �������� �������� ������� ENTER"
		FncSendData("{ENTER}")

		$sAction = "���� �� ������ �������� ������"
		FncMouseClick("left",50,50,1)

		$sAction = "���� ����� �������"
		FncSendData($sRegionName)

		$sAction = "������� ������ ��"
		FncMouseClick("left",345,75,1)

		ActionFindRegionInScada($sRegionName)

		$sCountOfAddedRegions += 1
		IniWrite($sRegionsConfig, $sRegionName, "regionName", $sRegionName)
		IniWrite($sRegionsConfig, $sRegionName, "isAdded", "True")
		IniWrite($sRegionsConfig, $sRegionName, "countOfAddedRegionObjects", "0")
		IniWrite($sAddedObjectsConfig, "AddedObjects", "countOfAddedRegions", $sCountOfAddedRegions)

		FncAddItemsToArrayAddedElementsInKSVD("Region", $sRegionName)

		$sAction = "�������� ������ " & $sRegionName
		_FileWriteLog($sLogFile, $sAction)
		FncScreenCapture($sKeyScreenPath, $sAction)

	EndIf
EndFunc