Func ActionAddCamera($sCamera)

	Local $sCameraName = IniRead ( $sCamerasConfig, $sCamera, "name", "$sCameraName")
	Local $sUri = IniRead ( $sCamerasConfig, $sCamera, "uri", "$sUri")
	Local $sLogin = IniRead ( $sCamerasConfig, $sCamera, "login", "$sLogin")
	Local $sPassword = IniRead ( $sCamerasConfig, $sCamera, "password", "$sPassword")
	Local $sModule = IniRead ( $sCamerasConfig, $sCamera, "module", "$sModule")
	Local $sTransport = IniRead ( $sCamerasConfig, $sCamera, "transport", "$sTransport")
	Local $sConnections = IniRead ( $sCamerasConfig, $sCamera, "connections", "$sConnections")
	Local $sUri2 = IniRead ( $sCamerasConfig, $sCamera, "Uri2", "$sUri2")
	Local $sAddedToServer  = IniRead ( $sCamerasConfig, $sCamera, "isAdded", "$sAddedToServer")
	Local $iCountOfAddedCameras = IniRead ( $sAddedObjectsConfig, "AddedObjects", "countOfAddedCameras", "$sAddedToServer")
	Local $sFuncName = "ActionAddCamera. "
	$sKeyWord = "Source Connected"
	Local $sLogName = "_ksvd4_video"

	_FileWriteLog($sLogFile, "---------------------------------------")
	_FileWriteLog($sLogFile, "������ �������� ���������� ������ " & $sCamera)
	_FileWriteLog($sDebugLogFile, "---------------------------------------")
	_FileWriteLog($sDebugLogFile, "������ �������� ���������� ������ " & $sCamera)

	If $sAddedToServer = "True" Then
		_FileWriteLog($sLogFile, "������: " & $sCamera & " ��� ���� ����� ��������� �� ������")
	Else
		$iCountOfAddedCameras = $iCountOfAddedCameras + 1
		IniWrite ( $sAddedObjectsConfig, "AddedObjects", "countOfAddedCameras",  $iCountOfAddedCameras)
		IniWrite ( $sCamerasConfig, $sCamera, "orderNumber",  $iCountOfAddedCameras)
		;

		; ---- ��������� �������� ����������
		; �������� ����������, ���������� �������� ������� ����� ��������� � GUI
		; ������ ��������� ����� 1, �.�. ����� �� ������������� ����� ����������� �������
		Local $sGuiName = $sCameraName & " " & "1"

		ActionClearFilterFieldInArmAdmin()

		$sAction = "������� � ��������"
		FncMouseClick("left",320,43,1)
		FncSendData("`") ;���� ������� ������� ��������� � ������ ������ �������

		FncClickTo("CameraTemplate", 1, "left")
		Sleep(1000)

		$sAction =  "���� �� ���� ���"
		FncMouseClick("left",160,580,1)
		Sleep(1000)

		$sAction =  "���������� ���� ���"
		FncSendData($sCameraName)

		$sAction = "���� �� ���� URI"
		FncMouseClick("left",160,640,1)
		Sleep(1000)

		$sAction =  "���������� ���� URI"
		FncSendData($sUri)
		$sAction = "������� ������ ENTER"
		FncSendData("{ENTER}")
		Sleep(1000)

		$sAction = "������� ���� �� ���� ������"
		FncMouseClick("left",125,725,2)
		If $sModule = "live555" Then
			$sAction = "����� " & $sModule
			FncMouseClick("left",130,725,1)
		ElseIf $sModule = "ffmpeg" Then
			FncClickTo("ModuleFFMPEG", 1, "left")
		EndIf ;����� �� �������� ����� �������� ��� ��� ������ �������� ������� ������, ���� �����������

		$sTime =_NowTime () ;�������� ������� ����� ���������� ������ ��� ������ �� �����

		$sAction = "���� �� ������ ������� �� SERVERNAME"
		FncMouseClick("left",100,100,1)
		
		IniWrite( $sCamerasConfig, $sCamera, "cameraGuiName", $sGuiName)
		IniWrite( $sCamerasConfig, $sCamera, "isAdded", "True")
		IniWrite( $sCamerasConfig, $sCamera, "isActive", "True")
		FncAddItemsToArrayAddedElementsInKSVD("Camera", $sCamera)

		Sleep(1000)
		ActionClearFilterFieldInArmAdmin()

		Local $aCoords = ActionFindCameraInGUI($sCamera)
		
		InnerFncGetCameraIDAndCheckLogFileExists($sCamera, $sLogName)

		$sAction = "����� ������������ ���� ������"
		FncMouseClick("right",$aCoords[0] ,$aCoords[1] ,1)

		$sAction = "������� ������ UP"
		FncSendData("{UP}")
		$sAction = "������� ������ ENTER"
		FncSendData("{ENTER}")
		Sleep(1000)

		$sAction = "������ " & $sCamera & " ��������� � GUI"
		_FileWriteLog($sLogFile, $sAction)

		Local $sMoment = @HOUR & "_" & @MIN & "_" & @SEC
		FncScreenCapture($sKeyScreenPath, $sMoment & ". ��������� ������ " & $sCameraName)

		ActionClearFilterFieldInArmAdmin()

	EndIf


EndFunc

Func InnerFncGetCameraIDAndCheckLogFileExists($sCamera, $sLogName)

	Local $sFuncName = "ActionAddCamera. InnerFncGetIDAndCheckLogFileExists. "

	$sAction = "���� �� ���� ��"
	FncMouseClick("left",120,550,1)

	Local $iID = FncCopyToClipboard()

	IniWrite ( $sCamerasConfig, $sCamera, "id",  $iID) ;������ ID ������ � ������

	Local $sLogPath = $sInstallPath & "\Log\" & "000" & $iID & $sLogName &".log" ;���� �� ��� �����

	_FileWriteLog($sDebugLogFile, $sFuncName & "��������� ���� ����: " & $sLogPath)

	Sleep(1000)

	If FileExists($sLogPath) Then
		_FileWriteLog($sLogFile, "������ " & $sCamera & " ������� ���������, ��� ���� KSVD ������������")
		IniWrite ( $sCamerasConfig, $sCamera, "logPath",  $sLogPath) ;������ ���� ������ � ������
	Else
		IniWrite( $sCamerasConfig, $sCamera, "isAdded", "False")
		IniWrite( $sCamerasConfig, $sCamera, "isActive", "False")

		_FileWriteLog($sLogFile, "����������� ��������� ��� ���� �� ����������� ������ � ���������� ���������")
		_FileWriteLog($sLogFile, "������������� ����: " & $sLogPath)
		FncTestEnding(False, "������ ���������� ������ " & $sCamera)
	EndIf
EndFunc