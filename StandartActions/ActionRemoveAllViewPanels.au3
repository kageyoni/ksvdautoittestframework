Func ActionRemoveAllViewPanels()

    _FileWriteLog($sLogFile, "---------------------------------------")
	_FileWriteLog($sLogFile, "������ �������� �������� ���� ���������" )
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
	_FileWriteLog($sDebugLogFile, "������ �������� �������� ���� ���������" )

    Local $bSuccessOperation = False

    Local $iTotalCountOfAddedViewPanels = IniRead( $sViewPanelsConfig, "CountOfAddedViewPanels", "countOfAddedViewPanels", "$iTotalCountOfAddedViewPanels")

    If $iTotalCountOfAddedViewPanels = 0 Then
        _FileWriteLog($sLogFile, "�������� ��������� �� ���������, ��� ��� ��� �� ����� ����������� ���������")
        $bSuccessOperation = True
    ElseIf $iTotalCountOfAddedViewPanels = 1 Then

        ActionClearFilterFieldInArmAdmin()

        $sAction = "����� ������������ ���� �� �������"
        If $sVersion = "2.8" Then
            FncMouseClick("right",350,95,1)
        ElseIf $sVersion = "2.10" Then
            FncMouseClick("right",350,80,1)
        EndIf

        $sAction = "����� ���������� ������ �������� DOWN"
        FncSendData("{DOWN}")

        $sAction = "������� ���������� ������ �������� ENTER"
        FncSendData("{ENTER}")

        $sAction = "���� �� ������ ��������"
        FncMouseClick("left",870,50,1)

        FncClickTo("ViewPanelsNodeInMainTree", 1, "left")

        $sAction = "��������� ��������� �������� RIGHT"
        FncSendData("{RIGHT}")

        $sAction = "������� � ��������� �������� DOWN"
        FncSendData("{DOWN}")

        $sAction = "���� �� ������ ������� ���������"
        FncMouseClick("left",50,50,1)

        $sAction = "������������� �������� �������� ENTER"
        FncSendData("{ENTER}")

        ; ---- ������� ������� �� ������� ����������� ���������
        $aIniSectionToErase = IniReadSectionNames($sViewPanelsConfig)
        Local $sError = @error
        If $sError = 1 Then
            _FileWriteLog($sLogFile, "������ �� �������� ���������")
        Else
            For $element IN $aIniSectionToErase
                _FileWriteLog($sLogFile, "$element = " &$element)
                IniDelete ( $sViewPanelsConfig, $element)
            Next
        EndIf
        IniWrite( $sViewPanelsConfig, "CountOfAddedViewPanels", "countOfAddedViewPanels", "0")
        $bSuccessOperation = True

        _FileWriteLog($sLogFile, "�������� ����� ��������� ������� ���������")

    ElseIf $iTotalCountOfAddedViewPanels > 1 Then

        ActionClearFilterFieldInArmAdmin()

        $sAction = "����� ������������ ���� �� �������"
        If $sVersion = "2.8" Then
            FncMouseClick("right",350,95,1)
        ElseIf $sVersion = "2.10" Then
            FncMouseClick("right",350,80,1)
        EndIf

        $sAction = "����� ���������� ������ �������� DOWN"
        FncSendData("{DOWN}")

        $sAction = "������� ���������� ������ �������� ENTER"
        FncSendData("{ENTER}")

        $sAction = "���� �� ������ ��������"
        FncMouseClick("left",870,50,1)

        FncClickTo("ViewPanelsNodeInMainTree", 1, "left")

        $sAction = "��������� ��������� �������� RIGHT"
        FncSendData("{RIGHT}")

        $sAction = "������� � ��������� �������� DOWN"
        FncSendData("{DOWN}")

        $sAction = "���� �� ������ �������� ���������� ���������"
        FncMouseClick("left",50,85,1)

        $sAction = "������� � ������ �������� ����� ������������� �������� DOWN"
        FncSendData("{DOWN}")

        $sAction = "�������� ����� ������������� �������� ENTER"
        FncSendData("{ENTER}")

        FncCheckAndActivateWindow("������� ���������...")

        $sAction = "���� �� ������ ���"
        FncMouseClick("left",20,545,1)

        $sAction = "�������� ��������� �������� ENTER"
        FncSendData("{ENTER}")

        $sAction = "������������� �������� �������� ENTER"
        FncSendData("{ENTER}")

        FncCheckAndActivateWindow($sArmAdminWindowName)

        ; ---- ������� ������� �� ������� ����������� ���������
        $aIniSectionToErase = IniReadSectionNames($sViewPanelsConfig)
        Local $sError = @error
        If $sError = 1 Then
            _FileWriteLog($sLogFile, "������ �� �������� ���������")
        Else
            For $element IN $aIniSectionToErase
                IniDelete ( $sViewPanelsConfig, $element)
            Next
        EndIf
        IniWrite( $sViewPanelsConfig, "CountOfAddedViewPanels", "countOfAddedViewPanels", "0")
        $bSuccessOperation = True
        _FileWriteLog($sLogFile, "�������� ���������� ��������� ������� ���������")

    Else
        _FileWriteLog($sLogFile, "�������� ������������ �������� ��������� $iTotalCountOfAddedViewPanels: " & $iTotalCountOfAddedViewPanels)
        $bSuccessOperation = False
    EndIf

    return $bSuccessOperation
EndFunc