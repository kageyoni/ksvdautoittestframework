Func ActionPlayLiveAndArchiveVideoArmOperator($sCamera)

    Local $sCameraGuiName = IniRead ( $sCamerasConfig, $sCamera, "cameraGuiName", "$sCameraGuiName")
    Local $sCameraName = IniRead ( $sCamerasConfig, $sCamera, "name", "$sCameraName")
    Local $sClass = "Qt5QWindowIcon"

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "����c� �������� ��������������� ������ ������ ������ " & $sCamera & " � ��� ���������" )
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "����c� �������� ��������������� ������ ������ ������ " & $sCamera & " � ��� ���������" )

    If $sVersion = "2.8" Then

        If $bArmOperator28CameraViewerIsOpened = False Then
            $sAction = "���� �� ������ �������� ������"
            FncMouseClick("left",25,310,1)
            $sAction = "�������������� ����� ������"
            FncMouseClick("left",75,45,1)
            $sAction = "���������� ����������� � ���� ������"
            FncMouseClick("left",75,95,1)
            $bArmOperator28CameraViewerIsOpened = True
        EndIf

        $sAction = "���� �� ���� ������ ������ ������� ����"
        FncMouseClick("right",200,65,1)
        _FileWriteLog($sDebugLogFile, "������� ���� ������")
        $sAction = "������� ������� UP"
		FncSendData("{UP}")
		$sAction = "������� ������� ENTER"
		FncSendData("{ENTER}")
		$sAction = "�������� ����������� ���� ������� DELETE"
		FncSendData("{DEL}")

        $sAction = "���� ����� ������"
        FncSendData($sCameraGuiName)
        $sAction = "������� ���� �� ��������� ������"
        FncMouseClick("left",180,120,2)

    ElseIf $sVersion = "2.10" Then

        Sleep(2000)
        $sAction = "��������� ������ ���� ���������"
        FncMouseClick("left",1588,415,1)
        Sleep(2000)
        $sAction = "���������� ����������� � ���� ������"
        FncMouseClick("left",815,50,1)
        $sAction = "���� �� ������ �������"
        FncMouseClick("left",865,20,1)
        $sAction = "���� ����� ������"
        FncSendData($sCameraGuiName)
        $sAction = "����� ������ ������� ������"
        FncMouseClick("left",900,75,2)

    EndIf

    Sleep(10000)
    $sAction = "����������� �������� ��������������� ������ ������ ������ " & $sCameraGuiName
    _FileWriteLog($sLogFile, $sAction)
    FncScreenCapture($sKeyScreenPath, $sAction)

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "����c� �������� ��������������� ��������� ������ ������ " & $sCamera & " � ��� ���������" )
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "����c� �������� ��������������� ��������� ������ ������ " & $sCamera & " � ��� ���������" )

    If $sVersion = "2.8" Then

        FncCheckAndActivateWindow($sArmOperatorWindowName, $sClass)

        $sAction = "������������ �� ����� ��������������� ��������� ������"
        FncMouseClick("left",900,45,1)

        $sAction = "������� ���� �� ��������. ����� �����������"
        _FileWriteLog($sDebugLogFile, $sAction)
        FncScreenCapture($sDebugScreenPath, $sAction)
        MouseMove(1000,967)
        $sAction = "������� ���� �� ��������. ����� ����������"
        _FileWriteLog($sDebugLogFile, $sAction)
        FncScreenCapture($sDebugScreenPath, $sAction)

        $sAction = "���� �� ��������. ����� �����������"
        _FileWriteLog($sDebugLogFile, $sAction)
        FncScreenCapture($sDebugScreenPath, $sAction)
        MouseDown("left")
        $sAction = "���� �� ��������. ����� ����������"
        _FileWriteLog($sDebugLogFile, $sAction)
        FncScreenCapture($sDebugScreenPath, $sAction)

        $sAction = "����������� ��������. ����� �����������"
        _FileWriteLog($sDebugLogFile, $sAction)
        FncScreenCapture($sDebugScreenPath, $sAction)
        MouseMove(1250,967)
        MouseUp("left")
        $sAction = "����������� ��������. ����� ����������"
        _FileWriteLog($sDebugLogFile, $sAction)
        FncScreenCapture($sDebugScreenPath, $sAction)

    ElseIf $sVersion = "2.10" Then

        $sAction = "�������� ������ ����������"
        FncMouseClick("left",1100,535,1)

        $sAction = "���� �� ������ ������������� �� �����"
        FncMouseClick("left",845,810,1)

        $sAction = "������� ���� �� ��������. ����� �����������"
        _FileWriteLog($sDebugLogFile, $sAction)
        FncScreenCapture($sDebugScreenPath, $sAction)
        MouseMove(1200,810)
        $sAction = "������� ���� �� ��������. ����� ����������"
        _FileWriteLog($sDebugLogFile, $sAction)
        FncScreenCapture($sDebugScreenPath, $sAction)

        $sAction = "���� �� ��������. ����� �����������"
        _FileWriteLog($sDebugLogFile, $sAction)
        FncScreenCapture($sDebugScreenPath, $sAction)
        MouseDown("left")
        $sAction = "���� �� ��������. ����� ����������"
        _FileWriteLog($sDebugLogFile, $sAction)
        FncScreenCapture($sDebugScreenPath, $sAction)

        $sAction = "����������� ��������. ����� �����������"
        _FileWriteLog($sDebugLogFile, $sAction)
        FncScreenCapture($sDebugScreenPath, $sAction)
        MouseMove(1500,810)
        MouseUp("left")
        $sAction = "����������� ��������. ����� ����������"
        _FileWriteLog($sDebugLogFile, $sAction)
        FncScreenCapture($sDebugScreenPath, $sAction)
    EndIf

    Sleep(2000)
    $sAction = "����������� �������� ��������������� ��������� ������ ������ " & $sCameraGuiName
    _FileWriteLog($sLogFile, $sAction)
    FncScreenCapture($sKeyScreenPath, $sAction)

    If $sVersion = "2.10"  Then
        _FileWriteLog($sLogFile, "�������� ������������� ���������" )

        Local $sCurrentDateTime = _NowCalc() ;��������� �������� �������
        Local $aStartTime = FncDateTimeChange("Minus", 2, $sCurrentDateTime) ; ���������� �������� ������ ��� �������� 1-3 - ����, 5-7 - �����

        $sAction = "���� �� ������ ���������"
        FncMouseClick("left",882,808,1)

        $sAction = "���� � ���� ������ ������� �� �������� �����"
        FncMouseClick("left",336,245,1)

        FncCheckAndActivateWindow("KSVD")

        $sAction = "������� ������ LEFT"
        FncSendData("{LEFT}")
        $sAction = "������� ������ LEFT"
        FncSendData("{LEFT}")
        $sAction = "������� ������ DEL"
        FncSendData("{DEL}")
        $sAction = "������� ������ DEL"
        FncSendData("{DEL}")

        $sAction = "���� �����"
        FncSendData($aStartTime[5]) ;���
        Sleep(500)
        $sAction = "���� �����"
        FncSendData($aStartTime[6]) ;������
        Sleep(500)
        $sAction = "���� ������"
        FncSendData($aStartTime[7]) ;�������
        Sleep(500)
        $sAction = "����������� �������� ��������������� ��������� ������ ������ " & $sCameraGuiName & " ����� ������� ������� � ���������"
        _FileWriteLog($sLogFile, $sAction)
        FncScreenCapture($sKeyScreenPath, $sAction)

        $sAction = "���� �� ������ OK"
        FncMouseClick("left",320,275,1)

        Sleep(1000)
        FncCheckAndActivateWindow($sArmOperatorWindowName)

        $sAction = "����������� �������� ��������������� ��������� ������ ������ " & $sCameraGuiName & " ����� ������ ������� � ���������"
        _FileWriteLog($sLogFile, $sAction)
        FncScreenCapture($sKeyScreenPath, $sAction)

    EndIf

EndFunc