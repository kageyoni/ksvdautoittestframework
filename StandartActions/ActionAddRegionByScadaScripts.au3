Func ActionAddRegionByScadaScripts($sRegionName)

    Local $sRegionAdded = IniRead ($sRegionsConfig, $sRegionName, "isAdded", "$sRegionAdded")
    Local $iCountOfAddedRegions = IniRead ($sAddedObjectsConfig, "AddedObjects", "countOfAddedRegions", "$iCountOfAddedRegions")
    Local $sFuncName = "ActionAddRegionByScadaScripts. "

	_FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� ���������� ������� " & $sRegionName & " ����� ����� �������")
	_FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "������ �������� ���������� ������� " & $sRegionName & " ����� ����� �������")

    If $sRegionAdded = "True" Then
		_FileWriteLog($sLogFile, "������ " & $sRegionName & " ����� ��� ��� ��������")
    Else

		$sAction = "���� �� ������� ����� �������"
		If $sVersion = "2.8" Then
			FncMouseClick("left",490,15,1)
		ElseIf $sVersion = "2.10" Then
			FncMouseClick("left",580,15,1)
		EndIf
	
	  	$sAction = "���� �� ������ ��������� �� ��"
	  	FncMouseClick("left",50,50,1)

		$sAction = "������� ������ ENTER �� ������ ���� �������� ���� �������������"
		FncSendData("{ENTER}")

	  	Local $iInner = 1
	  	Local $iYCor = 100

		if $iInner <= $iCountOfAddedRegions Then
			for $i = 1 to $iCountOfAddedRegions
			  	$iYCor = $iYCor + 30 ;����������� ���������� Y ��� ����������� ��������� �  ������ ������ �������
			next
		EndIf
		
	  	$sAction = "���� �� ���� �������� �������"
	  	FncMouseClick("left",370,$iYCor,1)

	  	$sAction = "���� ��������"
	  	FncSendData($sRegionName)

	  	;$sAction = "���� �� ������ ������������ �������"
	  	;FncMouseClick("left",120,100,1)

	  	$sAction = "���� �� ������ �������� �������� � ��������"
	  	FncMouseClick("left",120,145,1)

	  	$sAction = "������� ������ ENTER"
	  	FncSendData("{ENTER}")

		$sAction = "���� �� ������������ �����"
		FncMouseClick("left",290,800,1)

		If $sVersion = "2.8" Then
			;��� ������� ����� ��� 2.8. ���� ��� �������� ��� ���. ��������
		ElseIf $sVersion = "2.10" Then
			ActionFindRegionInScada($sRegionName)
		EndIf
	
		$iCountOfAddedRegions += 1
	    IniWrite($sRegionsConfig, $sRegionName, "regionName", $sRegionName)
	    IniWrite($sRegionsConfig, $sRegionName, "isAdded", "True")
		IniWrite($sRegionsConfig, $sRegionName, "countOfAddedRegionObjects", "0")
    	IniWrite($sAddedObjectsConfig, "AddedObjects", "countOfAddedRegions", $iCountOfAddedRegions)
		FncAddItemsToArrayAddedElementsInKSVD("Region", $sRegionName)

		$sAction = "�������� ������ " & $sRegionName
		_FileWriteLog($sLogFile, $sAction)
		FncScreenCapture($sKeyScreenPath, $sAction)

   	EndIf

EndFunc
