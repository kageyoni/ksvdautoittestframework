Func ActionFindRtspRetranslatorInGui($sRtspECHD)

    Local $sGuiName = IniRead( $sRTSPConfig, $sRtspECHD, "guiName", "$sGuiName" )
    Local $sIsAdded = IniRead( $sRTSPConfig, $sRtspECHD, "isAdded", "$sIsAdded" )

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� ������ ������������� " & $sGuiName)
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "������ �������� ������ ������������� " & $sGuiName)

    If $sIsAdded = "False" Then
        FncTestEnding(False, "�������� ������ ����������, ��������� ������������ " & $sGuiName & " �� �������� �� ������")
    ElseIf $sIsAdded = "True" Then

        ActionClearFilterFieldInArmAdmin()
        $sAction = "���� � ���� ������ ����� ���������� �������������"
        FncSendData($sGuiName)

        $sAction = "���� �� ��������������� �������������"
        If $sVersion = "2.8" Then
            FncMouseClick("left",370,130,1)
        ElseIf $sVersion = "2.10" Then
            FncMouseClick("left",370,110,1)
        EndIf

        $sAction = "������� ���� �� ���� ���"
        FncMouseClick("left",160,580,2)

        Local $sClipboard = FncCopyToClipboard()

        If $sGuiName = $sClipboard Then
            _FileWriteLog($sLogFile, "������������ " & $sGuiName & "  ������ � GUI")
        Else
            FncTestEnding(False, "�� ������� ����� ������������ " & $sGuiName & "  � GUI")
        EndIf
    Else
        FncTestEnding(False, "�������� ������������� �������� ��������� $sIsAdded = " & $sIsAdded )
    EndIf
EndFunc