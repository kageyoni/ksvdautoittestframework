Func ActionAddRtspRetranslatorGUI($sRtspECHD)

	Local $sGuiName = IniRead ( $sRTSPConfig, $sRtspECHD, "guiName", "$sGuiName" ) ;�������� ������������� � GUI
	Local $sDescription = IniRead ( $sRTSPConfig, $sRtspECHD, "description", "$sDescription" ) ;��������
	Local $sPort = IniRead ( $sRTSPConfig, $sRtspECHD, "port", "$sPort" ) ;����
	Local $sUserName = IniRead ( $sRTSPConfig, $sRtspECHD, "username", "$sUserName" ) ;��� ������������
	Local $sPassword = IniRead ( $sRTSPConfig, $sRtspECHD, "password", "$sPassword" ) ;������ ������������
	Local $sIsAdded = IniRead ( $sRTSPConfig, $sRtspECHD, "isAdded", "$sIsAdded" ) ;�������� �� ������������ �� ������

	Local $sLogName = "_ksvd4_rtspe"
	Local $sCountOfAddedRtspECHD = IniRead ( $sAddedObjectsConfig, "AddedObjects", "countOfAddedRtspECHD", "$sCountOfAddedRtspECHD")

	Local $sFuncName = "ActionAddRtspRetranslatorGUI. "


	If $sIsAdded = "False" Then

		_FileWriteLog($sLogFile, "---------------------------------------")
		_FileWriteLog($sLogFile, "������ �������� ���������� ������������� " & $sRtspECHD)
		_FileWriteLog($sDebugLogFile, "---------------------------------------")
		_FileWriteLog($sDebugLogFile, "������ �������� ���������� ������������� " & $sRtspECHD)

		$sAction = "������� � ��������"
		FncMouseClick("left",320,43,1)
		$sAction = "���� ������� ���������"
		FncSendData("`") ;���� ������� ������� ��������� � ������ ������ �������

		$sAction = "���� �� ������� RTSP ����"
		If $sVersion = "2.8" Then
            FncMouseClick("left",330,240,1)
        ElseIf $sVersion = "2.10" Then
            FncMouseClick("left",330,200,1)
        EndIf

		$sAction = "���� �� ���� ���"
		FncMouseClick("left",110,575,1)

		$sAction = "���� �������� ������������ � ���� ���"
		FncSendData($sUserName)

		$sAction = "���� �� ���� ��������"
		FncMouseClick("left",110,610,1)

		$sAction = "���� ��������"
		FncSendData($sDescription)

		$sAction = "���� �� RTSP ����"
		FncMouseClick("left",110,640,1)

		$sAction = "���� RTSP �����"
		FncSendData($sPort)

		$sAction = "���� �� ���� �����"
		FncMouseClick("left",110,670,1)

		$sAction = "���� ������"
		FncSendData($sUserName)

		$sAction = "���� �� ���� ������"
		FncMouseClick("left",110,700,1)

		$sAction = "���� ������"
		FncSendData($sPassword)

		$sAction = "���� �� ������ �������"
		FncMouseClick("left",50,100,1)

		$sCountOfAddedRtspECHD += 1
		IniWrite( $sAddedObjectsConfig, "AddedObjects", "countOfAddedRtspECHD",  $sCountOfAddedRtspECHD)
		IniWrite( $sRTSPConfig, $sRtspECHD, "isAdded", "True" )

		ActionFindRtspRetranslatorInGui($sRtspECHD)

		InnerFncGetRetranslatorIDAndCheckLogFileExists($sRtspECHD, $sLogName)

		FncAddItemsToArrayAddedElementsInKSVD("RtspRetranslator", $sRtspECHD)

		$sAction = "������������ " & $sRtspECHD & " ������� ��������"
		_FileWriteLog($sLogFile, $sAction)
		FncScreenCapture($sKeyScreenPath, $sAction)


	ElseIf $sIsAdded = "True" Then
		_FileWriteLog($sLogFile, "������������ " & $sGuiName & " ��� ��������")
	Else
		_FileWriteLog($sLogFile, "����� ������������ ������������: " & $sRtspECHD)
	EndIf

	EndFunc


Func InnerFncGetRetranslatorIDAndCheckLogFileExists($sRtspECHD, $sLogName)

	Local $sFuncName = "ActionAddRtspRetranslatorGUI. InnerFncGetIDAndCheckLogFileExists. "

	$sAction = "���� �� ���� ��"
	FncMouseClick("left",120,550,1)

	Local $iID = FncCopyToClipboard()

	IniWrite( $sRTSPConfig, $sRtspECHD, "id",  $iID) ;������ ID ������������� � ������

	Local $sLogPath = $sInstallPath & "\Log\" & "000" & $iID & $sLogName &".log" ;���� �� ��� �����

	_FileWriteLog($sDebugLogFile, $sFuncName & "��������� ���� ����: " & $sLogPath)

	If FileExists($sLogPath) Then
		_FileWriteLog($sLogFile, "������������ ������� ��������, ��� ���� KSVD ������������")
	Else
		_FileWriteLog($sLogFile, "��� ���� ���������� ������������� �� ������!")
		_FileWriteLog($sLogFile, "��������� ���: " & $sLogPath)
		IniWrite( $sRTSPConfig, $sRtspECHD, "isAdded", "False" )
		FncTestEnding(False, "������ ���������� �������������")
	EndIf

EndFunc

