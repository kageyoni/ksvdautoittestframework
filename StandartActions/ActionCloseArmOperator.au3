Func ActionCloseArmOperator()

    Local $sArmOperatorAuth28WindowName = "�������������� � ��� ��������� KSVD"

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� �������� ���������� ��� ���������")
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "������ �������� �������� ���������� ��� ���������")

    If $sVersion = "2.8" Then

        If WinExists($sArmOperatorWindowName ) Then
            FncCloseWindowByHandle($sArmOperatorWindowName)
            _FileWriteLog($sLogFile, "���������� ��� ��������� ������� �������")
        ElseIf WinExists($sArmOperatorAuth28WindowName) Then
            FncCloseWindowByHandle($sArmOperatorAuth28WindowName)
            _FileWriteLog($sLogFile, "���������� ��� ��������� ������� �������")
        Else
            FncTestEnding(False, "KSVD v2.8. �� ������� ���������� �������� ���� ��� ���������")
        EndIf

    ElseIf $sVersion = "2.10" Then

        If WinExists($sArmOperatorWindowName) Then
            If $sArmOperatorLoginSuccess = True Then
                FncCheckAndActivateWindow($hArmOperatorWindowHandle)
                $sAction = "���� �� ������ �������� ��� ���������"
                FncMouseClick("left",20,810,1)
            Else
                FncCloseWindowByHandle($sArmOperatorWindowName)
            EndIf
            Sleep(1000)
            If WinExists($sArmOperatorWindowName) Then
                FncTestEnding(False, "�� ������� ������� ���������� ��� ���������")
            Else
                _FileWriteLog($sLogFile, "���������� ��� ��������� ������� �������")
            EndIf

        Else
            FncTestEnding(False, "�� ������� ���������� �������� ���� ��� ���������")
        EndIf
        
    EndIf

EndFunc