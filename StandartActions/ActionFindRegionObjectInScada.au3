Func ActionFindRegionObjectInScada($sRegionName, $sObjectName, $bRenameObject)

    Local $sFuncName = "ActionFindRegionObjectInScada. "
    Local $iCountOfAddedRegionObjects = IniRead ($sRegionsConfig, $sRegionName, "countOfAddedRegionObjects", "0")

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� ������ ������� " &  $sObjectName & " ������� " & $sRegionName &" �� ������� �����")
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "������ �������� ������ ������� " &  $sObjectName & " ������� " & $sRegionName &" �� ������� �����")

    ActionFindRegionInScada($sRegionName)

    $sAction = "��������� ��������� ������� ������� RIGHT"
	FncSendData("{RIGHT}")

    If $iCountOfAddedRegionObjects = 0 Then
        ;$sAction = "���� �� ������� " & $sObjectName
	    ;FncMouseClick("left",345,105,1)
        FncTestEnding(False, "���������� ����� ������ " & $sObjectName & " ��������� ���-�� ����������� �������� " & $iCountOfAddedRegionObjects)
    ElseIf $iCountOfAddedRegionObjects > 0 Then
        for $i = 1 to $iCountOfAddedRegionObjects
            $sAction = "������� ������ DOWN"
            FncSendData("{DOWN}")
        next
    Else
        FncTestEnding(False, $sFuncName & "�������� �������� �� ���������� �������� ��������� $iCountOfAddedRegionObjects: " & $iCountOfAddedRegionObjects)
    EndIf

    $sAction = "������� ���� �� ���� �������� �������"
	FncMouseClick("left",110,550,2)

    If $bRenameObject = True Then
        Local $sDefaulName = "����������"
        Local $sClipboard = FncCopyToClipboard()
        _FileWriteLog($sDebugLogFile, $sFuncName & "� ������ ������ Windows ����������: " & $sClipboard)
        If $sDefaulName = $sClipboard Then
		    _FileWriteLog($sLogFile, "������ " & $sDefaulName & " ������ � GUI")
	    Else
		    FncTestEnding(False, "�� ������� ����� ������ " & $sDefaulName & " � GUI")
	    EndIf
        $sAction = "���������������� �������. ����� ��� " & $sObjectName
        FncSendData($sObjectName)
    Else
        Local $sClipboard = FncCopyToClipboard()
        _FileWriteLog($sDebugLogFile, $sFuncName & "� ������ ������ Windows ����������: " & $sClipboard)
        If $sObjectName = $sClipboard Then
		    _FileWriteLog($sLogFile, "������ " & $sObjectName & " ������ � GUI")
	    Else
		    FncTestEnding(False, "�� ������� ����� ������ " & $sObjectName & " � GUI")
	    EndIf
    EndIf
EndFunc