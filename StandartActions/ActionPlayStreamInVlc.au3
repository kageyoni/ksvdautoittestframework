Func ActionPlayStreamInVlc($sStreamURI)

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� ��������������� �����-������ " & $sStreamURI & " � ������������������ VLC")

    Local $bSuccessOperation = False
    Local $sVlcStandartWindowTitle = "������������������ VLC"
    Local $sVlcWindowTitle = $sStreamURI & " - ������������������ VLC"

    ShellExecute("C:\Program Files (x86)\VideoLAN\VLC\vlc.exe" , $sURI, "", "runas")
    Sleep(2000)

    If WinExists("������") Then
        FncCloseWindow($sVlcStandartWindowTitle)
		_FileWriteLog($sLogFile, "�� ������� ������������� �����-����� " & $sStreamURI)
        $bSuccessOperation = False
    Else
        FncCheckAndActivateWindow($sVlcWindowTitle)
        Sleep(6000)
        $sAction = "����������� �������� ��������������� �����-������ � ������������������ VLC"
        _FileWriteLog($sLogFile, $sAction)
        FncScreenCapture($sKeyScreenPath, $sAction)
        FncCloseWindow($sVlcWindowTitle)
        $bSuccessOperation = True
    EndIf
    return $bSuccessOperation
EndFunc