Func ActionRemoveArchiveInGui($sArchive)

    Local $bSuccessOperation = False
    Local $sGuiName = IniRead( $sArchivesConfig, $sArchive, "guiName", "$sGuiName")
    Local $sIsAdded = IniRead( $sArchivesConfig, $sArchive, "isAdded", "$sIsAdded")
    Local $iCountOfAddedCamerasToArchive = IniRead( $sArchivesConfig, $sArchive, "countOfAddedCamerasToArchive", "$iCountOfAddedCamerasToArchive")
    Local $sCountOfAddedArchives = IniRead( $sAddedObjectsConfig, "AddedObjects", "countOfAddedArchives ", "$sCountOfAddedArchives")
    Local $sPath = $sArchiveDir
    Local $sFileName = $sGuiName & ".bin"
    Local $sFilePath = $sPath & "\" & $sFileName

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� �������� ��������� " & $sArchive)

    FncCheckAndActivateWindow($sArmAdminWindowName)

    If $sIsAdded = "True" Then

        ActionClearFilterFieldInArmAdmin()

        ActionFindArchiveInGui($sArchive)

        $sAction = "���� �� ������ �������� ���������"
        FncMouseClick("left",50,100,1)

        $sAction = "������������� �������� ��������� �������� ENTER"
        FncSendData("{ENTER}")

        If FileExists($sFilePath) Then
            Local $iDeletionResult = FileDelete($sFilePath)
            If $iDeletionResult = 1 Then
                _FileWriteLog($sLogFile, "������� ����� ���� ��������� " & $sFilePath)
            Else
                FncTestEnding(False, "�� ������� ������� ���� ��������� " & $sFilePath)
            EndIf
        Else
            _FileWriteLog($sLogFile, "���� ��������� �� ���� " & $sFilePath & " �� ���������")
        EndIf

        Local $sCountOfAddedArchives = IniRead ( $sAddedObjectsConfig, "AddedObjects", "countOfAddedArchives ", "$sCountOfAddedArchives")
        $sCountOfAddedArchives -= 1
        IniWrite( $sAddedObjectsConfig, "AddedObjects", "countOfAddedArchives",  $sCountOfAddedArchives)
        IniWrite( $sArchivesConfig, $sArchive, "isAdded", "False")

        For $x = 1 To $iCountOfAddedCamerasToArchive
		   IniWrite( $sArchivesConfig, $sArchive, "addedCamera" & $x, "")
		Next

        _FileWriteLog($sLogFile, "��������� " & $sArchive & " ������� �������")
        $bSuccessOperation = True

    ElseIf $sIsAdded = "False" Then
        _FileWriteLog($sLogFile, "�������� �������� ����������, ��������� ��������� " & $sArchive & " �� ���������")
        $bSuccessOperation = True
    Else
        _FileWriteLog($sLogFile, "�������� ������������� �������� ��������� $sIsAdded = " & $sIsAdded)
        $bSuccessOperation = False
    EndIf
    return $bSuccessOperation
EndFunc
