Func ActionRunArmMonitor($sUser)

    Local $sUserName = IniRead ( $sUsersConfig, $sUser, "username", "$sUserName" ) ;��� ������������
    Local $sPassword = IniRead ( $sUsersConfig, $sUser, "password", "$sPassword" ) ;������ ������������
    Local $sArmMonitor = IniRead ( $sKsvdAppsParams, "KsvdAppsParams", "armMonitor", "$sArmMonitor") ;��� ����� ������� ��� �����������

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ ��� �����������")
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "������ ��� �����������")

    FncRunApp ($sInstallPath & "\" & $sArmMonitor)

    If $sVersion = "2.8"  Then
        FncCheckAndActivateWindow("�������������� � ��� ����������� KSVD")
        $sAction = "���� �� ���� �����"
        FncMouseClick("right",230,235,1)
    ElseIf $sVersion = "2.10" Then
        FncCheckAndActivateWindow($sArmMonitorWindowName)
        $sAction = "���� �� ���� �����"
        FncMouseClick("right",230,410,1)
    EndIf

    $sAction = "������� ������� UP"
    FncSendData("{UP}")
    $sAction = "������� ������� ENTER"
    FncSendData("{ENTER}")
    $sAction = "�������� ����������� ���� ������� DELETE"
    FncSendData("{DEL}")

    $sAction = "���� ������"
    FncSendData($sUserName)
    $sAction = "������� ������� TAB"
    FncSendData("{TAB}")
    $sAction = "���� ������"
    FncSendData($sPassword)
    $sAction = "������� ������� ENTER"
    FncSendData("{ENTER}")
    FncCheckAndActivateWindow($sArmMonitorWindowName)
    _FileWriteLog($sLogFile, "������ �����. ������� ��� �����������")

    $hArmMonitorWindowHandle = WinGetHandle ($sArmMonitorWindowName)
    
    ;---- �������� ���������� ����������� ----
    _FileWriteLog($sLogFile, "�������� ���������� �����������")

    sleep(2000)

    $sAction = "���� �� ���� ������"
    If $sVersion = "2.8"  Then
        FncMouseClick("left",100,90,1)
    ElseIf $sVersion = "2.10" Then
        FncMouseClick("left",100,100,1)
    EndIf

    $sAction = "���� ����� ������������ � ����"
    FncSendData($sUserName)

    $sAction = "��������� ����������� ���� � ������� ������������ ����"

    $sAction = "���� �� ���� ������ ������ ������� ����"
    If $sVersion = "2.8"  Then
        FncMouseClick("right",100,90,1)
    ElseIf $sVersion = "2.10" Then
        FncMouseClick("right",100,100,1)
    EndIf
    
    $sAction = "������� ������� UP"
    FncSendData("{UP}")
    $sAction = "������� ������� ENTER"
    FncSendData("{ENTER}")

    Local $sClipboard = FncCopyToClipboard()
    _FileWriteLog($sLogFile, "� ������ ������ Windows ����������: " & $sClipboard)

    _FileWriteLog($sLogFile, "$bExitIfAuthFail = " & $bExitIfAuthFail)
    Local $sUserIsAdded = IniRead ( $sUsersConfig, $sUserName, "isAdded", "$sUserIsAdded" ) ;��� ������������

    If $sClipboard = $sUser Then
	    If $sUserIsAdded = "True" Then
		    _FileWriteLog($sLogFile, "����������� ������ �������")
	    Else
		    _FileWriteLog($sLogFile, "������������, ��� ������� ������������� �����, �� �������� � KSVD. ������ ��� - ����������� ���������� ��� ����� �������������.")
		    FncTestEnding(False, "������. �������� ����������� � ��������� ����������������� �������")
	    EndIf
    Else
	    _FileWriteLog($sLogFile, "����������� �� �������")
	    If $bExitIfAuthFail = True Then
		    FncTestEnding(False, "������ �����������")
	    ElseIf $bExitIfAuthFail = False Then
		    _FileWriteLog($sLogFile, "������ ����������, ����������� ���������� �����")
	    EndIf
    EndIf

    $sAction = "�������� ����������� ���� �������� DEL"
    FncSendData("{DEL}")

EndFunc