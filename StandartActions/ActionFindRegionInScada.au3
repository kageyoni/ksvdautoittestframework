Func ActionFindRegionInScada($sRegionName)

    Local $sFuncName = "ActionFindRegionInScada. "

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� ������ ������� " & $sRegionName &" �� ������� �����")
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "������ �������� ������ ������� " & $sRegionName &" �� ������� �����")

    $sAction = "���� �� ������� �����"
    FncMouseClick("left",475,15,1)

    $sAction = "���� ������ ������� �� ����� ������"
	FncMouseClick("right",800,85,1)

    $sAction = "������� �� ����� �������� �������� ������� UP"
    FncSendData("{UP}")

    $sAction = "����� ������ �������� �������� ������� ENTER"
	FncSendData("{ENTER}")

    $sAction = "���� �� ���� ������"
    FncMouseClick("left",320,35,1)

    _FileWriteLog($sDebugLogFile, $sFuncName & "������� ������ �������")
    ClipPut ( "" ) ;� ������ 2.10, ���� � ���� ������ ���, �� ��������� ������� � ���� ����������� �� ������, ������� ��� ���� �������� �� ������ ������

    $sAction = "���� �� ���� ������ ������ ������� ����"
    FncMouseClick("right",320,35,1)
    $sAction = "������� ������� UP"
    FncSendData("{UP}")
    $sAction = "������� ������� ENTER"
    FncSendData("{ENTER}")
    $sAction = "�������� ����������� ���� ������� DELETE"
    FncSendData("{DEL}")
    $sAction = "������� ������� ESC �� ������ ���� ���� ������"
	FncSendData("{ESC}")


    $sAction = "���� �������� ������� � ���� ������"
	FncSendData($sRegionName)

    FncClickTo("1stRegion", 1, "left")

    $sAction = "������� ���� �� ���� ��������"
    FncMouseClick("left",120,550,2)
    
    Local $sClipboard = FncCopyToClipboard()

    If $sRegionName = $sClipboard Then
        _FileWriteLog($sLogFile, "������ " & $sRegionName & " ������ � GUI")
        FncClickTo("1stRegion", 1, "left")
    Else
		FncTestEnding(False, "������ " & $sRegionName & " �� ������ � GUI")
    EndIf
EndFunc