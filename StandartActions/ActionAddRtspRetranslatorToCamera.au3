Func ActionAddRtspRetranslatorToCamera($sRetranslator, $sCamera)

	Local $iCountOfAddedCameras = IniRead ( $sAddedObjectsConfig, "AddedObjects", "countOfAddedCameras", "$sAddedToServer")
	Local $sCameraGuiName = IniRead ( $sCamerasConfig, $sCamera, "cameraGuiName", "$sCameraGuiName")
	Local $iCameraId = IniRead ( $sCamerasConfig, $sCamera, "id", "$iCameraId")

	Local $sFuncName = "ActionAddRtspRetranslatorToCamera. "

	_FileWriteLog($sLogFile, "---------------------------------------")
	_FileWriteLog($sLogFile, "������ �������� ���������� ������������� " & $sRetranslator & " � ������ " & $sCamera)
	_FileWriteLog($sDebugLogFile, "---------------------------------------")
	_FileWriteLog($sDebugLogFile, "������ �������� ���������� ������������� " & $sRetranslator & " � ������ " & $sCamera)

	ActionFindRtspRetranslatorInGui($sRetranslator)

	If $iCountOfAddedCameras > 1 Then
		_FileWriteLog($sDebugLogFile, $sFuncName & "���� �� ������ ���������� � ������")
		FncMouseClick("left",115,145,1)
		Local $iCameraOrderNumber = IniRead ( $sCamerasConfig, $sCamera, "orderNumber", "$iCameraOrderNumber")
		FncSendData("{DOWN}") ;������� �� ������ �������, �.�. ������ ��������� ����� ������ ������. ���� ������� ���������� ����� �������� �����, ����� ����������� ���������� � ������ ����� ��� �����
		For $i = 1 To $iCameraOrderNumber
			Sleep(1000)
			FncSendData("{DOWN}")
		Next
		FncSendData("{ENTER}")
	ElseIf $iCountOfAddedCameras = 1 Then
		_FileWriteLog($sDebugLogFile, $sFuncName & "���� �� ������ ���������� � ������")
		FncMouseClick("left",115,145,1)
		Sleep(1000)
		FncSendData("{ENTER}")
		$sAction = "������������ " & $sRetranslator & " �������� � ������ " & $sCamera
		_FileWriteLog($sLogFile, $sAction)
		FncScreenCapture($sKeyScreenPath, $sAction)
	ElseIf $iCountOfAddedCameras < 1 Then
		_FileWriteLog($sLogFile, "��� �� ����� ����������� ������")
		FncTestEnding(False, "������ ���������� ������������� � ������. ��� �� ����� ����������� ������")
	EndIf

	IniWrite( $sRTSPConfig, $sRetranslator, "retranslatedCameraId", $iCameraId)
	IniWrite( $sRTSPConfig, $sRetranslator, "retranslatedCameraName", $sCamera)
	IniWrite( $sCamerasConfig, $sCamera, "isAddedToRtspEchd", "True")

EndFunc