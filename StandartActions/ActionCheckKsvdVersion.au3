Func ActionCheckKsvdVersion($sExpectedVersion, $bEndTestIfNotmatch)

    Local $bVersionIsMatched

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� �������� ������ KSVD")
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "������ �������� �������� ������ KSVD")

    $sAction = "���� �� ������ ������� KSVD"
    If $sVersion = "2.8" Then
        FncMouseClick("left",650,115,1)
    ElseIf $sVersion = "2.10" Then
        FncMouseClick("left",650,95,1)
    EndIf

    Local $sCurrentVersion = FncCopyToClipboard()
    _FileWriteLog($sDebugLogFile, "� ������ ������ Windows ����������: " & $sCurrentVersion)
    If $sCurrentVersion = $sExpectedVersion Then
        $bVersionIsMatched = True
        _FileWriteLog($sLogFile, "������� ������ �������: " & $sCurrentVersion)
    Else
        $bVersionIsMatched = False
        If $bEndTestIfNotmatch = False Then
            _FileWriteLog($sLogFile, "��������� ������ �������: " & $sExpectedVersion & " ������� ������ �������: " & $sCurrentVersion & ". ����������� ���������� �����")
        Else
            FncTestEnding(False, "��������� ������ �������: " & $sExpectedVersion & " ������� ������ �������: " & $sCurrentVersion)
        EndIf
    EndIf

    Return $bVersionIsMatched

EndFunc