Func ActionAddViewPanelByScadaScripts($aParameters)

	;������� �� ������� �� ���� ������ ����� ��� ����������, �.�. KSVD ��������� ��� ������, ������� ���������� � �������� ���
	;���� � ������� ��� �� ����� ������, ����������� ���� ������ Camera1

	Local $sRegionName = $aParameters[0] ;��� �������
	Local $sObjectName = $aParameters[1] ;��� �������
	Local $iUsingMonitor = $aParameters[2] ;����� ��������
	Local $iGridX = $aParameters[3] ;������ �����. ������ ��������
	Local $iGridY = $aParameters[4] ;������ �����. ������ ��������
	Local $sViewPanelName = $aParameters[1] & " 1"

    Local $sRegionAdded = IniRead ($sRegionsConfig, $sRegionName, "isAdded", "False")
    Local $sObjectAdded = IniRead ($sRegionsObjectsConfig, $sObjectName, "isAdded", "False")
	Local $sViewPanelAdded = IniRead ($sViewPanelsConfig, $sViewPanelName, "isAdded", "False")
    Local $iTotalCountOfAddedRegionsObjects = IniRead($sAddedObjectsConfig, "AddedObjects", "totalCountOfAddedRegionsObjects", "0")
	
	_FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� ���������� ��������� ����� ����� �������")

    If $sRegionAdded = "False" Then
		_FileWriteLog($sLogFile, "������ " &  $sRegionName & " �� ��������. ������ �������� ����������")
		ActionAddRegionByScada($sRegionName)
	EndIf

	If $sObjectAdded = "False" Then
		_FileWriteLog($sLogFile, "������ " &  $sObjectName & " �� ��������. ������ �������� ����������")
		ActionAddObjectToRegionByScada($sRegionName, $sObjectName)
	EndIf

    If $sViewPanelAdded = "True" Then
		_FileWriteLog($sLogFile, "��������� " & $sViewPanelName & " ��� ���������")
	ElseIf $sViewPanelAdded = "False" Then
		InnerDoAddViewPanelByScadaScripts($aParameters)
        $sAction = "��������� " & $sViewPanelName & " ������� ���������"
	  	_FileWriteLog($sLogFile, $sAction)
	  	FncScreenCapture($sKeyScreenPath, $sAction)
	Else
		_FileWriteLog($sLogFile, "�������� ������������� �������� ��������� $sViewPanelAdded " & $sViewPanelAdded)
	EndIf

EndFunc

Func InnerDoAddViewPanelByScadaScripts($aParameters)

	Local $sRegionName = $aParameters[0] ;��� �������
	Local $sObjectName = $aParameters[1] ;��� �������
	Local $iUsingMonitor = $aParameters[2] ;����� ��������
	Local $iGridX = $aParameters[3] ;������ �����. ������ ��������
	Local $iGridY = $aParameters[4] ;������ �����. ������ ��������
	Local $sViewPanelName = $aParameters[1] & " 1"

    $sAction = "���� �� ������� ����� �������"
	If $sVersion = "2.8" Then
        FncMouseClick("left",490,15,1)
    ElseIf $sVersion = "2.10" Then
        FncMouseClick("left",580,15,1)
    EndIf
				
	$sAction = "���� �� ������ ��������� �� ��"
	FncMouseClick("left",50,50,1)

    $sAction = "������� ������ ENTER �� ������ ���� �������� ���� �������������"
	FncSendData("{ENTER}")

    $sAction = "���� �� ������ �������� ���������"
	FncMouseClick("left",50,185,1)

    $sAction = "���� �� ������ ��"
	FncMouseClick("left",17,342,1)

    $sAction = "���� �� ���� �������"
	FncMouseClick("left",80,20,1)

    $sAction = "������� ����������� ���� �������� BACKSPACE"
	FncSendData("{BACKSPACE}")
    FncSendData("{BACKSPACE}")

    $sAction = "���� ������ ������������� ��������"
	FncSendData($iUsingMonitor)

    $sAction = "���� �� ���� ����� 1"
	FncMouseClick("left",80,45,1)

    $sAction = "������� ����������� ���� �������� BACKSPACE"
	FncSendData("{BACKSPACE}")
    FncSendData("{BACKSPACE}")

    $sAction = "���� �������� " & $iGridX &  " � ����"
	FncSendData($iGridX)
	$sAction = "������� �� ������ ���� ������� ����� �������� TAB"
	FncSendData("{TAB}")
	$sAction = "���� �������� " & $iGridY &  " � ����"
	FncSendData($iGridY)

    Local $iTotalCountOfAddedRegionsObjects = IniRead($sAddedObjectsConfig, "AddedObjects", "totalCountOfAddedRegionsObjects", "0")
    Local $iCoordY = 75

    If $iTotalCountOfAddedRegionsObjects < 1 Then
        FncTestEnding(False, "������ ����������. ���-�� ����������� �������� ������� ������ 1")
    ElseIf $iTotalCountOfAddedRegionsObjects = 1 Then
        ;$sAction = "����� ������� " & $sObjectName & " �� �����"
	    ;FncMouseClick("left",42,$iCoordY,1)
    Else
        ;for $i = 2 to $iTotalCountOfAddedRegionsObjects 
        ;    $iCoordY += 18
        ;next
        ;$sAction = "����� ������� " & $sObjectName & " �� �����"
	    ;FncMouseClick("left",42,$iCoordY,1)
    EndIf

    $sAction = "������� ������ ��"
	FncMouseClick("left",355,385,1)

	Sleep(1000)

    $sAction = "���� �� ������ ������������ �����"
	FncMouseClick("left",290,800,1)

	Local $iCountOfAddedToRegionsObjectViewPanels = IniRead ($sRegionsObjectsConfig, $sObjectName , "countOfAddedToRegionsObjectViewPanels", "0")
	Local $iTotalCountOfAddedViewPanels = IniRead( $sViewPanelsConfig, "CountOfAddedViewPanels", "countOfAddedViewPanels", "$iTotalCountOfAddedViewPanels")
	$iCountOfAddedToRegionsObjectViewPanels += 1
	$iTotalCountOfAddedViewPanels += 1
	IniWrite($sRegionsObjectsConfig, $sObjectName , "countOfAddedToRegionsObjectViewPanels", $iCountOfAddedToRegionsObjectViewPanels)
	IniWrite($sViewPanelsConfig, "CountOfAddedViewPanels", "countOfAddedViewPanels", $iTotalCountOfAddedViewPanels)
	IniWrite($sViewPanelsConfig, $sViewPanelName, "isAdded", "True")

EndFunc