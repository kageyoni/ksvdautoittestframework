Func ActionAddTileServer($sTileServerName)

   	Local $sName = IniRead ( $sTileServersConfig, $sTileServerName, "name", "$sName" )
   	Local $sGuiName = IniRead ( $sTileServersConfig, $sTileServerName, "guiName", "$sGuiName" )
   	Local $sDescription = IniRead ( $sTileServersConfig, $sTileServerName, "description", "$sDescription" )
   	Local $sURI = IniRead ( $sTileServersConfig, $sTileServerName, "URI", "$sURI" )
   	Local $sMapName = IniRead ( $sTileServersConfig, $sTileServerName, "mapName", "$sMapName" )
   	Local $sUseByDefault = IniRead ( $sTileServersConfig, $sTileServerName, "useByDefault", "$sUseByDefault" )
   	Local $sSourceType = IniRead ( $sTileServersConfig, $sTileServerName, "sourceType", "$sSourceType" )
   	Local $sIsAdded = IniRead ( $sTileServersConfig, $sTileServerName, "isAdded", "$sIsAdded" )

	_FileWriteLog($sLogFile, "---------------------------------------")
	_FileWriteLog($sLogFile, "������ �������� ���������� ��������� �������: " & $sTileServerName)
	_FileWriteLog($sDebugLogFile, "---------------------------------------")
	_FileWriteLog($sDebugLogFile, "������ �������� ���������� ��������� �������: " & $sTileServerName)

   	If $sIsAdded = "True" Then
		_FileWriteLog($sLogFile, "�������� ������ " & $sName & " ��� ��� �������� �����")
   	Else

	   	FncCheckNetworkFileExists($sURI)

		$sAction = "���� �� ������� �������"
		FncMouseClick("left",285,10,1)

		;ActionClearFilterFieldInArmAdmin()

		$sAction = "���� �� ���� ������"
	  	FncMouseClick("left",320,45,1)

	  	$sAction = "���� ������� ��������� � ������ �������"
	  	FncSendData("'")

	  	$sAction = "���� �� ������� ��������� �������"
		If $sVersion = "2.8" Then
			FncMouseClick("left",350,565,1)
		ElseIf $sVersion = "2.10" Then
			FncMouseClick("left",350,545,1)
		EndIf

	  	$sAction = "���� �� ���� ���"
	  	FncMouseClick("left",140,580,2)

	  	$sAction = "���� �����"
	  	FncSendData($sName)

	  	$sAction = "���� �� ���� URI"
	  	FncMouseClick("left",140,640,1)

	  	$sAction = "���������� ���� URI"
	  	FncSendData($sURI)

	  	$sAction = "������� ������ ENTER"
	  	FncSendData("{ENTER}")

	  	$sAction = "������� � ���� �������� ����� �������� ������ DOWN"
	  	FncSendData("{DOWN}")

	  	$sAction = "���� �������� �����"
	  	FncSendData($sMapName)

	  	$sAction = "������� � ���� �� ��������� �������� ������ DOWN"
	  	FncSendData("{DOWN}")

	  	$sAction = "������� ������ SPACE"
	  	FncSendData("{SPACE}")

	  	If $sUseByDefault = "True" Then
			$sAction = "����� �������� ��"
		  	If $sVersion = "2.8" Then
				FncMouseClick("left",188,707,1)
			ElseIf $sVersion = "2.10" Then
				FncMouseClick("left",188,698,1)
			EndIf	
	  	ElseIf $sUseByDefault = "False" Then
		 	;�������� ��� ��� �������������
	  	EndIf

	  	$sAction = "������� � ���� ��� ��������� �������� ������ DOWN"
	  	FncSendData("{DOWN}")

	  	$sAction = "������� ���� �� ����"
	  	FncMouseClick("left",188,727,2)

	  	If $sSourceType = "file" Then
			$sAction = "���� �� ��������� ����"
			If $sVersion = "2.8" Then
				FncMouseClick("left",188,736,1)
			ElseIf $sVersion = "2.10" Then
				FncMouseClick("left",188,727,1)
			EndIf
	  	ElseIf $sSourceType = "server" Then
		 	;�������� ��� ��� �������������
	  	EndIf

	  	$sAction = "���� �� ������ ������� �������� ������"
	  	FncMouseClick("left",120,50,1)

	  	ActionClearFilterFieldInArmAdmin()

		ActionFindTileServerInGui($sTileServerName)

		IniWrite( $sTileServersConfig, $sTileServerName, "isAdded", "True" )
		FncAddItemsToArrayAddedElementsInKSVD("TileServer", $sTileServerName)

		$sAction = "�������� ������ " & $sTileServerName & " ������� ��������"
	  	_FileWriteLog($sLogFile, $sAction)
	  	FncScreenCapture($sKeyScreenPath, $sAction)

   EndIf
EndFunc
