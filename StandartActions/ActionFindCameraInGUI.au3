Func ActionFindCameraInGUI($sCamera)

    Local $sCameraGuiName = IniRead( $sCamerasConfig, $sCamera, "cameraGuiName", "$sCameraGuiName")
    Local $sIsAddedToRtspEchd = IniRead( $sCamerasConfig, $sCamera, "isAddedToRtspEchd", "False")
    Local $sIsAddedToArchive = IniRead( $sCamerasConfig, $sCamera, "isAddedToArchive", "False")
    Local $sIsActive = IniRead( $sCamerasConfig, $sCamera, "isActive", "False")
    Local $aCoords[2]

    Local $sFuncName = "ActionFindCameraInGUI. "

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� ������ ������ " & $sCameraGuiName & " � ������")
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "������ �������� ������ ������ " & $sCameraGuiName & " � ������")

    ActionClearFilterFieldInArmAdmin()

    $sAction = "���� � ���� ������ ����� ������"
    FncSendData($sCameraGuiName)

    If $sIsActive = "True" Then

        If $sIsAddedToRtspEchd = "True" Then
            _FileWriteLog($sDebugLogFile, $sFuncName & "$sIsAddedToRtspEchd = True")
            $sAction = "���� �� �������������� ������"
            If $sVersion = "2.8" Then
                $aCoords[0] = 400
                $aCoords[1] = 170
            ElseIf $sVersion = "2.10" Then
                $aCoords[0] = 400
                $aCoords[1] = 130
            EndIf
            FncMouseClick("left",$aCoords[0],$aCoords[1],1) 
        ElseIf $sIsAddedToRtspEchd = "False" Then
            _FileWriteLog($sDebugLogFile, $sFuncName & "$sIsAddedToRtspEchd = False")
            If $sVersion = "2.8" Then
                $aCoords[0] = 400
                $aCoords[1] = 135
            ElseIf $sVersion = "2.10" Then
                $aCoords[0] = 370
                $aCoords[1] = 110
            EndIf
            $sAction = "���� �� �������������� ������"
            FncMouseClick("left",$aCoords[0],$aCoords[1],1) 
        Else
            FncTestEnding(False, "�������� ������������� �������� ��������� $sIsAddedToRtspEchd: " & $sIsAddedToRtspEchd)
        EndIf

    ElseIf $sIsActive = "False" Then
        _FileWriteLog($sDebugLogFile, $sFuncName & "$sIsActive = False")
        If $sVersion = "2.8" Then
            $aCoords[0] = 400
            $aCoords[1] = 115
        ElseIf $sVersion = "2.10" Then
            $aCoords[0] = 370
            $aCoords[1] = 95
        EndIf
        $sAction = "���� �� �������������� ������"
            FncMouseClick("left",$aCoords[0],$aCoords[1],1) 
    Else
        FncTestEnding(False, "�������� ������������� �������� ��������� $sIsActive: " & $sIsActive)
    EndIf

    $sAction = "������� ���� �� ���� ���"
    FncMouseClick("left",160,580,2)

    Local $sClipboard = FncCopyToClipboard()
    If $sCameraGuiName = $sClipboard Then
    _FileWriteLog($sDebugLogFile, $sFuncName & "������ ������� � GUI")
    Else
        _FileWriteLog($sLogFile, "�� ������� ����� ������ � GUI")
        FncTestEnding(False, "�� ������� ����� ������ " & $sCameraGuiName & " � GUI")
    EndIf

    Return $aCoords
EndFunc