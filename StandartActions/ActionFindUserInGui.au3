Func ActionFindUserInGui($sUser)

    Local $sGuiName = IniRead ( $sUsersConfig, $sUser, "guiName", "$sGuiName" )
    Local $sIsAdded = IniRead ( $sUsersConfig, $sUser, "isAdded", "$sIsAdded" )

    _FileWriteLog($sLogFile, "---------------------------------------")
    _FileWriteLog($sLogFile, "������ �������� ������ ������������ " & $sUser )
    _FileWriteLog($sDebugLogFile, "---------------------------------------")
    _FileWriteLog($sDebugLogFile, "������ �������� ������ ������������ " & $sUser )

    If $sIsAdded = "False" Then
		_FileWriteLog($sLogFile, "����� ����������, ��������� ������������ " & $sUser & " �� �������� �� ������")
    ElseIf $sIsAdded = "True" Then
    
        ActionClearFilterFieldInArmAdmin()

        $sAction = "���� � ���� ������ ����� ������������"
        FncSendData($sGuiName)

        $sAction = "���� �� ��������������� ������������"
        If $sVersion = "2.8" Then
            FncMouseClick("left",350,115,1)
        ElseIf $sVersion = "2.10" Then
            FncMouseClick("left",350,95,1)
        EndIf
        
        $sAction = "������� ���� �� ���� ���"
        FncMouseClick("left",160,580,2)

        Local $sClipboard = FncCopyToClipboard()
        If $sGuiName = $sClipboard Then
            _FileWriteLog($sLogFile, "������������ ������ � GUI")
        Else
            FncTestEnding(False, "������������ " & $sUser & " �� ������ � GUI")
        EndIf

    Else
        FncTestEnding(False, "�������� ������������ �������� ��������� $sIsAdded: " & $sIsAdded )
    EndIf

EndFunc